/* eslint-disable @typescript-eslint/no-var-requires */
const webpack = require('webpack'),
  version = require('./package.json').version,
  LodashModuleReplacementPlugin = require('lodash-webpack-plugin'),
  path = require('path'),
  MomentLocalesPlugin = require('moment-locales-webpack-plugin');

module.exports = {
  name: 'browser',
  mode: 'production',
  entry: `${__dirname}/lib/main.js`,
  output: {
    path: `${__dirname}/dist`,
    filename: 'illumass.js',
    library: {
      root: 'IllumassSDK',
      amd: 'illumass-sdk',
      commonjs: 'illumass-sdk',
    },
    libraryTarget: 'umd',
  },
  target: 'web',
  watch: false,
  devtool: 'cheap-module-source-map',
  node: {
    crypto: true,
    Buffer: true,
  },
  module: {
    rules: [
      {
        test: /\.?js$/,
        use: {
          loader: 'babel-loader',
          options: {
            plugins: ['lodash'],
            presets: [['@babel/env', { modules: false, targets: { node: 4 } }]],
          },
        },
      },
      {
        test: /\.?js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },
    ],
  },
  resolve: {
    alias: {
      'kuzzle-sdk': path.resolve(
        __dirname,
        'node_modules',
        'kuzzle-sdk',
        'dist',
        'kuzzle.js'
      ),
      'protobufjs/minimal': path.resolve(
        __dirname,
        'node_modules',
        'protobufjs',
        'dist',
        'minimal',
        'protobuf.min.js'
      ),
    },
  },
  plugins: [
    new MomentLocalesPlugin(),
    new LodashModuleReplacementPlugin(),
    new webpack.IgnorePlugin(/^(http|min-req-promise|package|ws)$/),
    new webpack.DefinePlugin({
      SDKVERSION: JSON.stringify(version),
      BUILT: true,
    }),
    new webpack.BannerPlugin('Illumass javascript SDK version ' + version),
    new webpack.optimize.OccurrenceOrderPlugin(),
  ],
};
