#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pushd ${SCRIPT_DIR} || exit 1

docker-compose pull
docker-compose down
scripts/up.sh

jest --config jest.functional.config.js --runInBand

docker-compose down

popd || exit 1
