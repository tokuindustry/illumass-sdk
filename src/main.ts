// Copyright 2022 TOKU Systems, Inc.
import _set from 'lodash/set';
import _has from 'lodash/has';

export * from './illumass';
export { equestria, google } from './time-series';
export {
  RawPoint,
  SummaryPoint,
  TimeSeries,
  TemporalResolution,
} from './controller/signal-data';
export {
  SearchResult,
  SearchHit,
  SearchOptions,
  ReadOptions,
  SearchWithIncludeLastOptions,
  ReadWithIncludeLastOptions,
} from './controller/base-entity-controller';
export * from './common';
import { Kuzzle, WebSocket, Http, KuzzleAbstractProtocol } from 'kuzzle-sdk';
import { Illumass } from './illumass';

export function kuzzleParamsFromUrl(
  illumassUrl?: string
): [string, { port?: number; sslConnection?: boolean }, string] {
  const url = new URL(
    illumassUrl ?? process.env.ILLUMASS_API_URL ?? 'wss://api.illumass.com/v4/'
  );

  const opts = {};

  if (url.port !== '') {
    _set(opts, ['port'], parseInt(url.port));
  }

  if (['https:', 'wss:'].indexOf(url.protocol) >= 0) {
    _set(opts, ['sslConnection'], true);
    // Only default to 443 if there is no other port provided
    if (!_has(opts, 'port')) {
      _set(opts, ['port'], 443);
    }
  }

  if (url.pathname === '/') {
    return [url.hostname, opts, url.protocol];
  } else {
    return [url.hostname + url.pathname, opts, url.protocol];
  }
}

export function createIllumass(illumassUrl?: string): Illumass {
  const [host, opts, scheme] = kuzzleParamsFromUrl(illumassUrl);

  let protocol: KuzzleAbstractProtocol;

  if (['ws:', 'wss:'].indexOf(scheme) >= 0) {
    protocol = new WebSocket(host, opts);
  } else if (['http:', 'https:'].indexOf(scheme) >= 0) {
    protocol = new Http(host, opts);
  } else {
    throw new Error(`${illumassUrl} is not a valid Illumass URL`);
  }

  const kuzzle: Kuzzle = new Kuzzle(protocol);
  return new Illumass(kuzzle);
}
