import * as $protobuf from "protobufjs";
/** Namespace equestria. */
export namespace equestria {

    /** Namespace protobuf. */
    namespace protobuf {

        /** Namespace common. */
        namespace common {

            /** Namespace v1. */
            namespace v1 {

                /** Properties of a Slice. */
                interface ISlice {

                    /** Slice regular */
                    regular?: (equestria.protobuf.common.v1.IRegularSlice|null);

                    /** Slice irregular */
                    irregular?: (equestria.protobuf.common.v1.IIrregularSlice|null);
                }

                /** Represents a Slice. */
                class Slice implements ISlice {

                    /**
                     * Constructs a new Slice.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.ISlice);

                    /** Slice regular. */
                    public regular?: (equestria.protobuf.common.v1.IRegularSlice|null);

                    /** Slice irregular. */
                    public irregular?: (equestria.protobuf.common.v1.IIrregularSlice|null);

                    /** Slice kind. */
                    public kind?: ("regular"|"irregular");

                    /**
                     * Creates a new Slice instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns Slice instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.ISlice): equestria.protobuf.common.v1.Slice;

                    /**
                     * Encodes the specified Slice message. Does not implicitly {@link equestria.protobuf.common.v1.Slice.verify|verify} messages.
                     * @param message Slice message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.ISlice, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified Slice message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.Slice.verify|verify} messages.
                     * @param message Slice message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.ISlice, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a Slice message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns Slice
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.Slice;

                    /**
                     * Decodes a Slice message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns Slice
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.Slice;

                    /**
                     * Verifies a Slice message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a Slice message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns Slice
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.Slice;

                    /**
                     * Creates a plain object from a Slice message. Also converts values to other types if specified.
                     * @param message Slice
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.Slice, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this Slice to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a RawValues. */
                interface IRawValues {

                    /** RawValues values */
                    values?: (number[]|null);
                }

                /** Represents a RawValues. */
                class RawValues implements IRawValues {

                    /**
                     * Constructs a new RawValues.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.IRawValues);

                    /** RawValues values. */
                    public values: number[];

                    /**
                     * Creates a new RawValues instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns RawValues instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.IRawValues): equestria.protobuf.common.v1.RawValues;

                    /**
                     * Encodes the specified RawValues message. Does not implicitly {@link equestria.protobuf.common.v1.RawValues.verify|verify} messages.
                     * @param message RawValues message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.IRawValues, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified RawValues message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.RawValues.verify|verify} messages.
                     * @param message RawValues message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.IRawValues, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a RawValues message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns RawValues
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.RawValues;

                    /**
                     * Decodes a RawValues message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns RawValues
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.RawValues;

                    /**
                     * Verifies a RawValues message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a RawValues message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns RawValues
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.RawValues;

                    /**
                     * Creates a plain object from a RawValues message. Also converts values to other types if specified.
                     * @param message RawValues
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.RawValues, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this RawValues to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a SummaryValues. */
                interface ISummaryValues {

                    /** SummaryValues meanValues */
                    meanValues?: (number[]|null);

                    /** SummaryValues minValues */
                    minValues?: (number[]|null);

                    /** SummaryValues maxValues */
                    maxValues?: (number[]|null);

                    /** SummaryValues variances */
                    variances?: (number[]|null);

                    /** SummaryValues counts */
                    counts?: ((number|Long)[]|null);
                }

                /** Represents a SummaryValues. */
                class SummaryValues implements ISummaryValues {

                    /**
                     * Constructs a new SummaryValues.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.ISummaryValues);

                    /** SummaryValues meanValues. */
                    public meanValues: number[];

                    /** SummaryValues minValues. */
                    public minValues: number[];

                    /** SummaryValues maxValues. */
                    public maxValues: number[];

                    /** SummaryValues variances. */
                    public variances: number[];

                    /** SummaryValues counts. */
                    public counts: (number|Long)[];

                    /**
                     * Creates a new SummaryValues instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns SummaryValues instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.ISummaryValues): equestria.protobuf.common.v1.SummaryValues;

                    /**
                     * Encodes the specified SummaryValues message. Does not implicitly {@link equestria.protobuf.common.v1.SummaryValues.verify|verify} messages.
                     * @param message SummaryValues message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.ISummaryValues, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified SummaryValues message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.SummaryValues.verify|verify} messages.
                     * @param message SummaryValues message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.ISummaryValues, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a SummaryValues message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns SummaryValues
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.SummaryValues;

                    /**
                     * Decodes a SummaryValues message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns SummaryValues
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.SummaryValues;

                    /**
                     * Verifies a SummaryValues message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a SummaryValues message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns SummaryValues
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.SummaryValues;

                    /**
                     * Creates a plain object from a SummaryValues message. Also converts values to other types if specified.
                     * @param message SummaryValues
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.SummaryValues, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this SummaryValues to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a RegularSlice. */
                interface IRegularSlice {

                    /** RegularSlice start */
                    start?: (google.protobuf.ITimestamp|null);

                    /** RegularSlice period */
                    period?: (google.protobuf.IDuration|null);

                    /** RegularSlice raws */
                    raws?: (equestria.protobuf.common.v1.IRawValues|null);

                    /** RegularSlice summaries */
                    summaries?: (equestria.protobuf.common.v1.ISummaryValues|null);
                }

                /** Represents a RegularSlice. */
                class RegularSlice implements IRegularSlice {

                    /**
                     * Constructs a new RegularSlice.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.IRegularSlice);

                    /** RegularSlice start. */
                    public start?: (google.protobuf.ITimestamp|null);

                    /** RegularSlice period. */
                    public period?: (google.protobuf.IDuration|null);

                    /** RegularSlice raws. */
                    public raws?: (equestria.protobuf.common.v1.IRawValues|null);

                    /** RegularSlice summaries. */
                    public summaries?: (equestria.protobuf.common.v1.ISummaryValues|null);

                    /** RegularSlice data. */
                    public data?: ("raws"|"summaries");

                    /**
                     * Creates a new RegularSlice instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns RegularSlice instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.IRegularSlice): equestria.protobuf.common.v1.RegularSlice;

                    /**
                     * Encodes the specified RegularSlice message. Does not implicitly {@link equestria.protobuf.common.v1.RegularSlice.verify|verify} messages.
                     * @param message RegularSlice message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.IRegularSlice, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified RegularSlice message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.RegularSlice.verify|verify} messages.
                     * @param message RegularSlice message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.IRegularSlice, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a RegularSlice message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns RegularSlice
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.RegularSlice;

                    /**
                     * Decodes a RegularSlice message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns RegularSlice
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.RegularSlice;

                    /**
                     * Verifies a RegularSlice message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a RegularSlice message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns RegularSlice
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.RegularSlice;

                    /**
                     * Creates a plain object from a RegularSlice message. Also converts values to other types if specified.
                     * @param message RegularSlice
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.RegularSlice, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this RegularSlice to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of an IrregularSlice. */
                interface IIrregularSlice {

                    /** IrregularSlice start */
                    start?: (google.protobuf.ITimestamp|null);

                    /** IrregularSlice asDurations */
                    asDurations?: (equestria.protobuf.common.v1.IPeriodsAsDurations|null);

                    /** IrregularSlice asSeconds */
                    asSeconds?: (equestria.protobuf.common.v1.IPeriodsAsSeconds|null);

                    /** IrregularSlice asMilliseconds */
                    asMilliseconds?: (equestria.protobuf.common.v1.IPeriodsAsMilliseconds|null);

                    /** IrregularSlice asMicroseconds */
                    asMicroseconds?: (equestria.protobuf.common.v1.IPeriodsAsMicroseconds|null);

                    /** IrregularSlice raws */
                    raws?: (equestria.protobuf.common.v1.IRawValues|null);

                    /** IrregularSlice summaries */
                    summaries?: (equestria.protobuf.common.v1.ISummaryValues|null);
                }

                /** Represents an IrregularSlice. */
                class IrregularSlice implements IIrregularSlice {

                    /**
                     * Constructs a new IrregularSlice.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.IIrregularSlice);

                    /** IrregularSlice start. */
                    public start?: (google.protobuf.ITimestamp|null);

                    /** IrregularSlice asDurations. */
                    public asDurations?: (equestria.protobuf.common.v1.IPeriodsAsDurations|null);

                    /** IrregularSlice asSeconds. */
                    public asSeconds?: (equestria.protobuf.common.v1.IPeriodsAsSeconds|null);

                    /** IrregularSlice asMilliseconds. */
                    public asMilliseconds?: (equestria.protobuf.common.v1.IPeriodsAsMilliseconds|null);

                    /** IrregularSlice asMicroseconds. */
                    public asMicroseconds?: (equestria.protobuf.common.v1.IPeriodsAsMicroseconds|null);

                    /** IrregularSlice raws. */
                    public raws?: (equestria.protobuf.common.v1.IRawValues|null);

                    /** IrregularSlice summaries. */
                    public summaries?: (equestria.protobuf.common.v1.ISummaryValues|null);

                    /** IrregularSlice periods. */
                    public periods?: ("asDurations"|"asSeconds"|"asMilliseconds"|"asMicroseconds");

                    /** IrregularSlice data. */
                    public data?: ("raws"|"summaries");

                    /**
                     * Creates a new IrregularSlice instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns IrregularSlice instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.IIrregularSlice): equestria.protobuf.common.v1.IrregularSlice;

                    /**
                     * Encodes the specified IrregularSlice message. Does not implicitly {@link equestria.protobuf.common.v1.IrregularSlice.verify|verify} messages.
                     * @param message IrregularSlice message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.IIrregularSlice, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified IrregularSlice message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.IrregularSlice.verify|verify} messages.
                     * @param message IrregularSlice message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.IIrregularSlice, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes an IrregularSlice message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns IrregularSlice
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.IrregularSlice;

                    /**
                     * Decodes an IrregularSlice message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns IrregularSlice
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.IrregularSlice;

                    /**
                     * Verifies an IrregularSlice message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates an IrregularSlice message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns IrregularSlice
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.IrregularSlice;

                    /**
                     * Creates a plain object from an IrregularSlice message. Also converts values to other types if specified.
                     * @param message IrregularSlice
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.IrregularSlice, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this IrregularSlice to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a PeriodsAsDurations. */
                interface IPeriodsAsDurations {

                    /** PeriodsAsDurations durations */
                    durations?: (google.protobuf.IDuration[]|null);
                }

                /** Represents a PeriodsAsDurations. */
                class PeriodsAsDurations implements IPeriodsAsDurations {

                    /**
                     * Constructs a new PeriodsAsDurations.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.IPeriodsAsDurations);

                    /** PeriodsAsDurations durations. */
                    public durations: google.protobuf.IDuration[];

                    /**
                     * Creates a new PeriodsAsDurations instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns PeriodsAsDurations instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.IPeriodsAsDurations): equestria.protobuf.common.v1.PeriodsAsDurations;

                    /**
                     * Encodes the specified PeriodsAsDurations message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsDurations.verify|verify} messages.
                     * @param message PeriodsAsDurations message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.IPeriodsAsDurations, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified PeriodsAsDurations message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsDurations.verify|verify} messages.
                     * @param message PeriodsAsDurations message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.IPeriodsAsDurations, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a PeriodsAsDurations message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns PeriodsAsDurations
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.PeriodsAsDurations;

                    /**
                     * Decodes a PeriodsAsDurations message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns PeriodsAsDurations
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.PeriodsAsDurations;

                    /**
                     * Verifies a PeriodsAsDurations message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a PeriodsAsDurations message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns PeriodsAsDurations
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.PeriodsAsDurations;

                    /**
                     * Creates a plain object from a PeriodsAsDurations message. Also converts values to other types if specified.
                     * @param message PeriodsAsDurations
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.PeriodsAsDurations, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this PeriodsAsDurations to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a PeriodsAsSeconds. */
                interface IPeriodsAsSeconds {

                    /** PeriodsAsSeconds seconds */
                    seconds?: (number[]|null);
                }

                /** Represents a PeriodsAsSeconds. */
                class PeriodsAsSeconds implements IPeriodsAsSeconds {

                    /**
                     * Constructs a new PeriodsAsSeconds.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.IPeriodsAsSeconds);

                    /** PeriodsAsSeconds seconds. */
                    public seconds: number[];

                    /**
                     * Creates a new PeriodsAsSeconds instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns PeriodsAsSeconds instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.IPeriodsAsSeconds): equestria.protobuf.common.v1.PeriodsAsSeconds;

                    /**
                     * Encodes the specified PeriodsAsSeconds message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsSeconds.verify|verify} messages.
                     * @param message PeriodsAsSeconds message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.IPeriodsAsSeconds, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified PeriodsAsSeconds message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsSeconds.verify|verify} messages.
                     * @param message PeriodsAsSeconds message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.IPeriodsAsSeconds, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a PeriodsAsSeconds message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns PeriodsAsSeconds
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.PeriodsAsSeconds;

                    /**
                     * Decodes a PeriodsAsSeconds message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns PeriodsAsSeconds
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.PeriodsAsSeconds;

                    /**
                     * Verifies a PeriodsAsSeconds message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a PeriodsAsSeconds message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns PeriodsAsSeconds
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.PeriodsAsSeconds;

                    /**
                     * Creates a plain object from a PeriodsAsSeconds message. Also converts values to other types if specified.
                     * @param message PeriodsAsSeconds
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.PeriodsAsSeconds, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this PeriodsAsSeconds to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a PeriodsAsMilliseconds. */
                interface IPeriodsAsMilliseconds {

                    /** PeriodsAsMilliseconds milliseconds */
                    milliseconds?: (number[]|null);
                }

                /** Represents a PeriodsAsMilliseconds. */
                class PeriodsAsMilliseconds implements IPeriodsAsMilliseconds {

                    /**
                     * Constructs a new PeriodsAsMilliseconds.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.IPeriodsAsMilliseconds);

                    /** PeriodsAsMilliseconds milliseconds. */
                    public milliseconds: number[];

                    /**
                     * Creates a new PeriodsAsMilliseconds instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns PeriodsAsMilliseconds instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.IPeriodsAsMilliseconds): equestria.protobuf.common.v1.PeriodsAsMilliseconds;

                    /**
                     * Encodes the specified PeriodsAsMilliseconds message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMilliseconds.verify|verify} messages.
                     * @param message PeriodsAsMilliseconds message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.IPeriodsAsMilliseconds, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified PeriodsAsMilliseconds message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMilliseconds.verify|verify} messages.
                     * @param message PeriodsAsMilliseconds message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.IPeriodsAsMilliseconds, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a PeriodsAsMilliseconds message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns PeriodsAsMilliseconds
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.PeriodsAsMilliseconds;

                    /**
                     * Decodes a PeriodsAsMilliseconds message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns PeriodsAsMilliseconds
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.PeriodsAsMilliseconds;

                    /**
                     * Verifies a PeriodsAsMilliseconds message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a PeriodsAsMilliseconds message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns PeriodsAsMilliseconds
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.PeriodsAsMilliseconds;

                    /**
                     * Creates a plain object from a PeriodsAsMilliseconds message. Also converts values to other types if specified.
                     * @param message PeriodsAsMilliseconds
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.PeriodsAsMilliseconds, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this PeriodsAsMilliseconds to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a PeriodsAsMicroseconds. */
                interface IPeriodsAsMicroseconds {

                    /** PeriodsAsMicroseconds microseconds */
                    microseconds?: (number[]|null);
                }

                /** Represents a PeriodsAsMicroseconds. */
                class PeriodsAsMicroseconds implements IPeriodsAsMicroseconds {

                    /**
                     * Constructs a new PeriodsAsMicroseconds.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.IPeriodsAsMicroseconds);

                    /** PeriodsAsMicroseconds microseconds. */
                    public microseconds: number[];

                    /**
                     * Creates a new PeriodsAsMicroseconds instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns PeriodsAsMicroseconds instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.IPeriodsAsMicroseconds): equestria.protobuf.common.v1.PeriodsAsMicroseconds;

                    /**
                     * Encodes the specified PeriodsAsMicroseconds message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMicroseconds.verify|verify} messages.
                     * @param message PeriodsAsMicroseconds message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.IPeriodsAsMicroseconds, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified PeriodsAsMicroseconds message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMicroseconds.verify|verify} messages.
                     * @param message PeriodsAsMicroseconds message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.IPeriodsAsMicroseconds, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a PeriodsAsMicroseconds message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns PeriodsAsMicroseconds
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.PeriodsAsMicroseconds;

                    /**
                     * Decodes a PeriodsAsMicroseconds message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns PeriodsAsMicroseconds
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.PeriodsAsMicroseconds;

                    /**
                     * Verifies a PeriodsAsMicroseconds message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a PeriodsAsMicroseconds message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns PeriodsAsMicroseconds
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.PeriodsAsMicroseconds;

                    /**
                     * Creates a plain object from a PeriodsAsMicroseconds message. Also converts values to other types if specified.
                     * @param message PeriodsAsMicroseconds
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.PeriodsAsMicroseconds, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this PeriodsAsMicroseconds to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of a TimeSeries. */
                interface ITimeSeries {

                    /** TimeSeries slices */
                    slices?: (equestria.protobuf.common.v1.ISlice[]|null);

                    /** TimeSeries measuringUnit */
                    measuringUnit?: (equestria.protobuf.entity.v1.IEntityRef|null);
                }

                /** Represents a TimeSeries. */
                class TimeSeries implements ITimeSeries {

                    /**
                     * Constructs a new TimeSeries.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.common.v1.ITimeSeries);

                    /** TimeSeries slices. */
                    public slices: equestria.protobuf.common.v1.ISlice[];

                    /** TimeSeries measuringUnit. */
                    public measuringUnit?: (equestria.protobuf.entity.v1.IEntityRef|null);

                    /**
                     * Creates a new TimeSeries instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns TimeSeries instance
                     */
                    public static create(properties?: equestria.protobuf.common.v1.ITimeSeries): equestria.protobuf.common.v1.TimeSeries;

                    /**
                     * Encodes the specified TimeSeries message. Does not implicitly {@link equestria.protobuf.common.v1.TimeSeries.verify|verify} messages.
                     * @param message TimeSeries message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.common.v1.ITimeSeries, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified TimeSeries message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.TimeSeries.verify|verify} messages.
                     * @param message TimeSeries message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.common.v1.ITimeSeries, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a TimeSeries message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns TimeSeries
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.common.v1.TimeSeries;

                    /**
                     * Decodes a TimeSeries message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns TimeSeries
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.common.v1.TimeSeries;

                    /**
                     * Verifies a TimeSeries message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a TimeSeries message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns TimeSeries
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.common.v1.TimeSeries;

                    /**
                     * Creates a plain object from a TimeSeries message. Also converts values to other types if specified.
                     * @param message TimeSeries
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.common.v1.TimeSeries, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this TimeSeries to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }
            }
        }

        /** Namespace entity. */
        namespace entity {

            /** Namespace v1. */
            namespace v1 {

                /** EntityType enum. */
                enum EntityType {
                    USERS = 0,
                    COUNTRIES = 1,
                    MEASUREMENT_TYPES = 2,
                    MEASURING_UNITS = 3,
                    DEVICE_TYPES = 4,
                    TENANTS = 5,
                    DEVICE_MODELS = 6,
                    DEVICES = 7,
                    SIGNALS = 8,
                    ASSET_TYPES = 9,
                    ASSETS = 10,
                    HARDPOINTS = 11,
                    SOFTPOINT_TYPES = 12,
                    SOFTPOINTS = 13,
                    SENSORS = 14,
                    SENSOR_MODELS = 15,
                    SENSOR_CLASSES = 16,
                    DEVICE_PROFILES = 17,
                    DEVICE_PROFILE_TYPES = 18
                }

                /** Properties of an EntityTypeValue. */
                interface IEntityTypeValue {

                    /** EntityTypeValue value */
                    value?: (equestria.protobuf.entity.v1.EntityType|null);
                }

                /** Represents an EntityTypeValue. */
                class EntityTypeValue implements IEntityTypeValue {

                    /**
                     * Constructs a new EntityTypeValue.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.entity.v1.IEntityTypeValue);

                    /** EntityTypeValue value. */
                    public value: equestria.protobuf.entity.v1.EntityType;

                    /**
                     * Creates a new EntityTypeValue instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns EntityTypeValue instance
                     */
                    public static create(properties?: equestria.protobuf.entity.v1.IEntityTypeValue): equestria.protobuf.entity.v1.EntityTypeValue;

                    /**
                     * Encodes the specified EntityTypeValue message. Does not implicitly {@link equestria.protobuf.entity.v1.EntityTypeValue.verify|verify} messages.
                     * @param message EntityTypeValue message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.entity.v1.IEntityTypeValue, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified EntityTypeValue message, length delimited. Does not implicitly {@link equestria.protobuf.entity.v1.EntityTypeValue.verify|verify} messages.
                     * @param message EntityTypeValue message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.entity.v1.IEntityTypeValue, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes an EntityTypeValue message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns EntityTypeValue
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.entity.v1.EntityTypeValue;

                    /**
                     * Decodes an EntityTypeValue message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns EntityTypeValue
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.entity.v1.EntityTypeValue;

                    /**
                     * Verifies an EntityTypeValue message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates an EntityTypeValue message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns EntityTypeValue
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.entity.v1.EntityTypeValue;

                    /**
                     * Creates a plain object from an EntityTypeValue message. Also converts values to other types if specified.
                     * @param message EntityTypeValue
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.entity.v1.EntityTypeValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this EntityTypeValue to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** Properties of an EntityRef. */
                interface IEntityRef {

                    /** EntityRef entityType */
                    entityType?: (equestria.protobuf.entity.v1.EntityType|null);

                    /** EntityRef id */
                    id?: (string|null);

                    /** EntityRef slugs */
                    slugs?: (string[]|null);

                    /** EntityRef shortName */
                    shortName?: (string|null);
                }

                /** Represents an EntityRef. */
                class EntityRef implements IEntityRef {

                    /**
                     * Constructs a new EntityRef.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.entity.v1.IEntityRef);

                    /** EntityRef entityType. */
                    public entityType: equestria.protobuf.entity.v1.EntityType;

                    /** EntityRef id. */
                    public id: string;

                    /** EntityRef slugs. */
                    public slugs: string[];

                    /** EntityRef shortName. */
                    public shortName: string;

                    /**
                     * Creates a new EntityRef instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns EntityRef instance
                     */
                    public static create(properties?: equestria.protobuf.entity.v1.IEntityRef): equestria.protobuf.entity.v1.EntityRef;

                    /**
                     * Encodes the specified EntityRef message. Does not implicitly {@link equestria.protobuf.entity.v1.EntityRef.verify|verify} messages.
                     * @param message EntityRef message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.entity.v1.IEntityRef, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified EntityRef message, length delimited. Does not implicitly {@link equestria.protobuf.entity.v1.EntityRef.verify|verify} messages.
                     * @param message EntityRef message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.entity.v1.IEntityRef, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes an EntityRef message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns EntityRef
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.entity.v1.EntityRef;

                    /**
                     * Decodes an EntityRef message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns EntityRef
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.entity.v1.EntityRef;

                    /**
                     * Verifies an EntityRef message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates an EntityRef message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns EntityRef
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.entity.v1.EntityRef;

                    /**
                     * Creates a plain object from an EntityRef message. Also converts values to other types if specified.
                     * @param message EntityRef
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.entity.v1.EntityRef, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this EntityRef to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }

                /** SignalNumberFormatType enum. */
                enum SignalNumberFormatType {
                    SIGNAL_NUMBER_FORMAT_TYPE_NOOP = 0,
                    DEFAULT = 1,
                    PRECISION = 2,
                    FIXED = 3
                }

                /** Properties of a SignalNumberFormat. */
                interface ISignalNumberFormat {

                    /** SignalNumberFormat formatType */
                    formatType?: (equestria.protobuf.entity.v1.SignalNumberFormatType|null);

                    /** SignalNumberFormat digits */
                    digits?: (google.protobuf.IInt32Value|null);
                }

                /** Represents a SignalNumberFormat. */
                class SignalNumberFormat implements ISignalNumberFormat {

                    /**
                     * Constructs a new SignalNumberFormat.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: equestria.protobuf.entity.v1.ISignalNumberFormat);

                    /** SignalNumberFormat formatType. */
                    public formatType: equestria.protobuf.entity.v1.SignalNumberFormatType;

                    /** SignalNumberFormat digits. */
                    public digits?: (google.protobuf.IInt32Value|null);

                    /**
                     * Creates a new SignalNumberFormat instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns SignalNumberFormat instance
                     */
                    public static create(properties?: equestria.protobuf.entity.v1.ISignalNumberFormat): equestria.protobuf.entity.v1.SignalNumberFormat;

                    /**
                     * Encodes the specified SignalNumberFormat message. Does not implicitly {@link equestria.protobuf.entity.v1.SignalNumberFormat.verify|verify} messages.
                     * @param message SignalNumberFormat message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: equestria.protobuf.entity.v1.ISignalNumberFormat, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified SignalNumberFormat message, length delimited. Does not implicitly {@link equestria.protobuf.entity.v1.SignalNumberFormat.verify|verify} messages.
                     * @param message SignalNumberFormat message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: equestria.protobuf.entity.v1.ISignalNumberFormat, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a SignalNumberFormat message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns SignalNumberFormat
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): equestria.protobuf.entity.v1.SignalNumberFormat;

                    /**
                     * Decodes a SignalNumberFormat message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns SignalNumberFormat
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): equestria.protobuf.entity.v1.SignalNumberFormat;

                    /**
                     * Verifies a SignalNumberFormat message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a SignalNumberFormat message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns SignalNumberFormat
                     */
                    public static fromObject(object: { [k: string]: any }): equestria.protobuf.entity.v1.SignalNumberFormat;

                    /**
                     * Creates a plain object from a SignalNumberFormat message. Also converts values to other types if specified.
                     * @param message SignalNumberFormat
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: equestria.protobuf.entity.v1.SignalNumberFormat, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this SignalNumberFormat to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };
                }
            }
        }
    }
}

/** Namespace google. */
export namespace google {

    /** Namespace protobuf. */
    namespace protobuf {

        /** Properties of a Duration. */
        interface IDuration {

            /** Duration seconds */
            seconds?: (number|Long|null);

            /** Duration nanos */
            nanos?: (number|null);
        }

        /** Represents a Duration. */
        class Duration implements IDuration {

            /**
             * Constructs a new Duration.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IDuration);

            /** Duration seconds. */
            public seconds: (number|Long);

            /** Duration nanos. */
            public nanos: number;

            /**
             * Creates a new Duration instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Duration instance
             */
            public static create(properties?: google.protobuf.IDuration): google.protobuf.Duration;

            /**
             * Encodes the specified Duration message. Does not implicitly {@link google.protobuf.Duration.verify|verify} messages.
             * @param message Duration message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IDuration, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Duration message, length delimited. Does not implicitly {@link google.protobuf.Duration.verify|verify} messages.
             * @param message Duration message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IDuration, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Duration message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Duration
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Duration;

            /**
             * Decodes a Duration message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Duration
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Duration;

            /**
             * Verifies a Duration message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Duration message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Duration
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Duration;

            /**
             * Creates a plain object from a Duration message. Also converts values to other types if specified.
             * @param message Duration
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Duration, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Duration to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Timestamp. */
        interface ITimestamp {

            /** Timestamp seconds */
            seconds?: (number|Long|null);

            /** Timestamp nanos */
            nanos?: (number|null);
        }

        /** Represents a Timestamp. */
        class Timestamp implements ITimestamp {

            /**
             * Constructs a new Timestamp.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.ITimestamp);

            /** Timestamp seconds. */
            public seconds: (number|Long);

            /** Timestamp nanos. */
            public nanos: number;

            /**
             * Creates a new Timestamp instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Timestamp instance
             */
            public static create(properties?: google.protobuf.ITimestamp): google.protobuf.Timestamp;

            /**
             * Encodes the specified Timestamp message. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
             * @param message Timestamp message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.ITimestamp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Timestamp message, length delimited. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
             * @param message Timestamp message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.ITimestamp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Timestamp message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Timestamp
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Timestamp;

            /**
             * Decodes a Timestamp message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Timestamp
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Timestamp;

            /**
             * Verifies a Timestamp message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Timestamp message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Timestamp
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Timestamp;

            /**
             * Creates a plain object from a Timestamp message. Also converts values to other types if specified.
             * @param message Timestamp
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Timestamp, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Timestamp to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a DoubleValue. */
        interface IDoubleValue {

            /** DoubleValue value */
            value?: (number|null);
        }

        /** Represents a DoubleValue. */
        class DoubleValue implements IDoubleValue {

            /**
             * Constructs a new DoubleValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IDoubleValue);

            /** DoubleValue value. */
            public value: number;

            /**
             * Creates a new DoubleValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns DoubleValue instance
             */
            public static create(properties?: google.protobuf.IDoubleValue): google.protobuf.DoubleValue;

            /**
             * Encodes the specified DoubleValue message. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
             * @param message DoubleValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IDoubleValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified DoubleValue message, length delimited. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
             * @param message DoubleValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IDoubleValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a DoubleValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns DoubleValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.DoubleValue;

            /**
             * Decodes a DoubleValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns DoubleValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.DoubleValue;

            /**
             * Verifies a DoubleValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a DoubleValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns DoubleValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.DoubleValue;

            /**
             * Creates a plain object from a DoubleValue message. Also converts values to other types if specified.
             * @param message DoubleValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.DoubleValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this DoubleValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a FloatValue. */
        interface IFloatValue {

            /** FloatValue value */
            value?: (number|null);
        }

        /** Represents a FloatValue. */
        class FloatValue implements IFloatValue {

            /**
             * Constructs a new FloatValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IFloatValue);

            /** FloatValue value. */
            public value: number;

            /**
             * Creates a new FloatValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns FloatValue instance
             */
            public static create(properties?: google.protobuf.IFloatValue): google.protobuf.FloatValue;

            /**
             * Encodes the specified FloatValue message. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
             * @param message FloatValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IFloatValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified FloatValue message, length delimited. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
             * @param message FloatValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IFloatValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a FloatValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns FloatValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.FloatValue;

            /**
             * Decodes a FloatValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns FloatValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.FloatValue;

            /**
             * Verifies a FloatValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a FloatValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns FloatValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.FloatValue;

            /**
             * Creates a plain object from a FloatValue message. Also converts values to other types if specified.
             * @param message FloatValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.FloatValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this FloatValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an Int64Value. */
        interface IInt64Value {

            /** Int64Value value */
            value?: (number|Long|null);
        }

        /** Represents an Int64Value. */
        class Int64Value implements IInt64Value {

            /**
             * Constructs a new Int64Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IInt64Value);

            /** Int64Value value. */
            public value: (number|Long);

            /**
             * Creates a new Int64Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Int64Value instance
             */
            public static create(properties?: google.protobuf.IInt64Value): google.protobuf.Int64Value;

            /**
             * Encodes the specified Int64Value message. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
             * @param message Int64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Int64Value message, length delimited. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
             * @param message Int64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Int64Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Int64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Int64Value;

            /**
             * Decodes an Int64Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Int64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Int64Value;

            /**
             * Verifies an Int64Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Int64Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Int64Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Int64Value;

            /**
             * Creates a plain object from an Int64Value message. Also converts values to other types if specified.
             * @param message Int64Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Int64Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Int64Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a UInt64Value. */
        interface IUInt64Value {

            /** UInt64Value value */
            value?: (number|Long|null);
        }

        /** Represents a UInt64Value. */
        class UInt64Value implements IUInt64Value {

            /**
             * Constructs a new UInt64Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IUInt64Value);

            /** UInt64Value value. */
            public value: (number|Long);

            /**
             * Creates a new UInt64Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns UInt64Value instance
             */
            public static create(properties?: google.protobuf.IUInt64Value): google.protobuf.UInt64Value;

            /**
             * Encodes the specified UInt64Value message. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
             * @param message UInt64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IUInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified UInt64Value message, length delimited. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
             * @param message UInt64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IUInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a UInt64Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns UInt64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.UInt64Value;

            /**
             * Decodes a UInt64Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns UInt64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.UInt64Value;

            /**
             * Verifies a UInt64Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a UInt64Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns UInt64Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.UInt64Value;

            /**
             * Creates a plain object from a UInt64Value message. Also converts values to other types if specified.
             * @param message UInt64Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.UInt64Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this UInt64Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an Int32Value. */
        interface IInt32Value {

            /** Int32Value value */
            value?: (number|null);
        }

        /** Represents an Int32Value. */
        class Int32Value implements IInt32Value {

            /**
             * Constructs a new Int32Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IInt32Value);

            /** Int32Value value. */
            public value: number;

            /**
             * Creates a new Int32Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Int32Value instance
             */
            public static create(properties?: google.protobuf.IInt32Value): google.protobuf.Int32Value;

            /**
             * Encodes the specified Int32Value message. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
             * @param message Int32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Int32Value message, length delimited. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
             * @param message Int32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Int32Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Int32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Int32Value;

            /**
             * Decodes an Int32Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Int32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Int32Value;

            /**
             * Verifies an Int32Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Int32Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Int32Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Int32Value;

            /**
             * Creates a plain object from an Int32Value message. Also converts values to other types if specified.
             * @param message Int32Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Int32Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Int32Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a UInt32Value. */
        interface IUInt32Value {

            /** UInt32Value value */
            value?: (number|null);
        }

        /** Represents a UInt32Value. */
        class UInt32Value implements IUInt32Value {

            /**
             * Constructs a new UInt32Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IUInt32Value);

            /** UInt32Value value. */
            public value: number;

            /**
             * Creates a new UInt32Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns UInt32Value instance
             */
            public static create(properties?: google.protobuf.IUInt32Value): google.protobuf.UInt32Value;

            /**
             * Encodes the specified UInt32Value message. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
             * @param message UInt32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IUInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified UInt32Value message, length delimited. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
             * @param message UInt32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IUInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a UInt32Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns UInt32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.UInt32Value;

            /**
             * Decodes a UInt32Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns UInt32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.UInt32Value;

            /**
             * Verifies a UInt32Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a UInt32Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns UInt32Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.UInt32Value;

            /**
             * Creates a plain object from a UInt32Value message. Also converts values to other types if specified.
             * @param message UInt32Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.UInt32Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this UInt32Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a BoolValue. */
        interface IBoolValue {

            /** BoolValue value */
            value?: (boolean|null);
        }

        /** Represents a BoolValue. */
        class BoolValue implements IBoolValue {

            /**
             * Constructs a new BoolValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IBoolValue);

            /** BoolValue value. */
            public value: boolean;

            /**
             * Creates a new BoolValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BoolValue instance
             */
            public static create(properties?: google.protobuf.IBoolValue): google.protobuf.BoolValue;

            /**
             * Encodes the specified BoolValue message. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
             * @param message BoolValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IBoolValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified BoolValue message, length delimited. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
             * @param message BoolValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IBoolValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a BoolValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BoolValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.BoolValue;

            /**
             * Decodes a BoolValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BoolValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.BoolValue;

            /**
             * Verifies a BoolValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a BoolValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns BoolValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.BoolValue;

            /**
             * Creates a plain object from a BoolValue message. Also converts values to other types if specified.
             * @param message BoolValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.BoolValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this BoolValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a StringValue. */
        interface IStringValue {

            /** StringValue value */
            value?: (string|null);
        }

        /** Represents a StringValue. */
        class StringValue implements IStringValue {

            /**
             * Constructs a new StringValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IStringValue);

            /** StringValue value. */
            public value: string;

            /**
             * Creates a new StringValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns StringValue instance
             */
            public static create(properties?: google.protobuf.IStringValue): google.protobuf.StringValue;

            /**
             * Encodes the specified StringValue message. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
             * @param message StringValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IStringValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified StringValue message, length delimited. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
             * @param message StringValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IStringValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a StringValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns StringValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.StringValue;

            /**
             * Decodes a StringValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns StringValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.StringValue;

            /**
             * Verifies a StringValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a StringValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns StringValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.StringValue;

            /**
             * Creates a plain object from a StringValue message. Also converts values to other types if specified.
             * @param message StringValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.StringValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this StringValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a BytesValue. */
        interface IBytesValue {

            /** BytesValue value */
            value?: (Uint8Array|null);
        }

        /** Represents a BytesValue. */
        class BytesValue implements IBytesValue {

            /**
             * Constructs a new BytesValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IBytesValue);

            /** BytesValue value. */
            public value: Uint8Array;

            /**
             * Creates a new BytesValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BytesValue instance
             */
            public static create(properties?: google.protobuf.IBytesValue): google.protobuf.BytesValue;

            /**
             * Encodes the specified BytesValue message. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
             * @param message BytesValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IBytesValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified BytesValue message, length delimited. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
             * @param message BytesValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IBytesValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a BytesValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BytesValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.BytesValue;

            /**
             * Decodes a BytesValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BytesValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.BytesValue;

            /**
             * Verifies a BytesValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a BytesValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns BytesValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.BytesValue;

            /**
             * Creates a plain object from a BytesValue message. Also converts values to other types if specified.
             * @param message BytesValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.BytesValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this BytesValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }
}
