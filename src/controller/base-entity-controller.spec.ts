import { Http, Kuzzle } from 'kuzzle-sdk';
import _ from 'lodash';
import { Illumass, kuzzleParamsFromUrl } from '../main';

function mockIllumass(): Illumass {
  const illumassUrl = 'http://xyz:1234';
  const [host, opts] = kuzzleParamsFromUrl(illumassUrl);
  const protocol = new Http(host, opts);
  protocol.connect = (): Promise<void> => {
    return Promise.resolve();
  };
  const kuzzle: Kuzzle = new Kuzzle(protocol);
  return new Illumass(kuzzle);
}

describe('base entity controller tests', () => {
  test('search for devices and set includeLast to true', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(true);
        eventReceived = true;
      });
      await illumass.device.search(
        {},
        {
          includeLast: true,
        }
      );
    } catch (error) {
      /*
        Since there's no connection made to kuzzle the following error will be thrown:

        Error: Unable to execute request: not connected to a Kuzzle server.
        Discarded request: {"index":"rumble","collection":"countries","body":{},"action":"search",
        "controller":"document","requestId":"4437aae4-10ff-4aea-94ac-00683f2a94c3","verb":"POST",
        "volatile":{"includeLast":true,"sdkInstanceId":"d77181cd-7b0d-4124-936f-1e518523a641",
        "sdkName":"js@7.10.0"},"includeLast":true}
      */
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });

  test('search for devices and set includeLast to false', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(false);
        eventReceived = true;
      });
      await illumass.device.search(
        {},
        {
          includeLast: false,
        }
      );
    } catch (error) {
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });

  test('search for devices and includeLast not specified', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(false);
        eventReceived = true;
      });
      await illumass.device.search({}, {});
    } catch (error) {
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });

  test('read a device and set includeLast to true', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(true);
        eventReceived = true;
      });
      const dummyRef = 'devices!XYZ';
      await illumass.device.read(dummyRef, {
        includeLast: true,
      });
    } catch (error) {
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });

  test('read a device and set includeLast to false', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(false);
        eventReceived = true;
      });
      const dummyRef = 'devices!XYZ';
      await illumass.device.read(dummyRef, {
        includeLast: false,
      });
    } catch (error) {
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });

  test('read a device and includeLast not specified', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(false);
        eventReceived = true;
      });
      const dummyRef = 'devices!XYZ';
      await illumass.device.read(dummyRef, {});
    } catch (error) {
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });

  test('get a device and set includeLast to true', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(true);
        eventReceived = true;
      });
      const dummyRef = 'devices!XYZ';

      const opts = Object.assign(
        {
          volatile: {
            includeLast: true,
          },
        },
        {}
      );

      await illumass.kuzzle.document.get('rumble', 'devices', dummyRef, opts);
    } catch (error) {
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });

  test('get a device and set includeLast to false', async () => {
    let eventReceived = false;

    try {
      const illumass = mockIllumass();
      await illumass.connect();
      illumass.kuzzle.addListener('discarded', (request: object) => {
        expect(_.get(request, ['volatile', 'includeLast'])).toBe(false);
        eventReceived = true;
      });
      const dummyRef = 'devices!XYZ';

      const opts = Object.assign(
        {
          volatile: {
            includeLast: false,
          },
        },
        {}
      );

      await illumass.kuzzle.document.get('rumble', 'devices', dummyRef, opts);
    } catch (error) {
      expect(
        (error as Error)
          .toString()
          .indexOf(
            'Unable to execute request: not connected to a Kuzzle server.'
          )
      ).toBeGreaterThan(-1);
    } finally {
      expect(eventReceived).toBe(true);
    }
  });
});
