// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import { User, entityTypeInfoByEntityType, EntityType } from '../../common';
import { Illumass } from '../../illumass';

export class UserController extends BaseEntityController<User> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.USERS));
  }
}
