// Copyright 2025 TOKU Systems Inc.
import {
  BaseEntityController,
  ReadWithIncludeLastOptions,
  SearchResult,
  SearchWithIncludeLastOptions,
} from '../base-entity-controller';
import {
  Device,
  entityTypeInfoByEntityType,
  EntityType,
  EntityRef,
} from '../../common';
import { Illumass } from '../../illumass';

export class DeviceController extends BaseEntityController<Device> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.DEVICES));
  }

  async read(
    ref: string | EntityRef,
    options?: ReadWithIncludeLastOptions
  ): Promise<Device> {
    const optsWithIncludeLast = {
      ...options,
      includeLast: options?.includeLast ?? false,
    };
    return await super.read(
      ref,
      optsWithIncludeLast as ReadWithIncludeLastOptions
    );
  }

  async search(
    query: object,
    options?: SearchWithIncludeLastOptions
  ): Promise<SearchResult<Device>> {
    const optsWithIncludeLast = {
      ...options,
      includeLast: options?.includeLast ?? false,
    };
    return await super.search(
      query,
      optsWithIncludeLast as ReadWithIncludeLastOptions
    );
  }
}
