// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import {
  MeasuringUnit,
  entityTypeInfoByEntityType,
  EntityType,
} from '../../common';
import { Illumass } from '../../illumass';

export class MeasuringUnitController extends BaseEntityController<MeasuringUnit> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.MEASURING_UNITS));
  }
}
