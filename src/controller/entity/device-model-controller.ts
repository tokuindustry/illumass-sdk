// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import {
  DeviceModel,
  entityTypeInfoByEntityType,
  EntityType,
} from '../../common';
import { Illumass } from '../../illumass';

export class DeviceModelController extends BaseEntityController<DeviceModel> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.DEVICE_MODELS));
  }
}
