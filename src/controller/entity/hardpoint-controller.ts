// Copyright 2025 TOKU Systems Inc.
import {
  BaseEntityController,
  ReadWithIncludeLastOptions,
  SearchResult,
  SearchWithIncludeLastOptions,
} from '../base-entity-controller';
import {
  Hardpoint,
  entityTypeInfoByEntityType,
  EntityType,
  EntityRef,
} from '../../common';
import { Illumass } from '../../illumass';

export class HardpointController extends BaseEntityController<Hardpoint> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.HARDPOINTS));
  }

  async read(
    ref: string | EntityRef,
    options?: ReadWithIncludeLastOptions
  ): Promise<Hardpoint> {
    const optsWithIncludeLast = {
      ...options,
      includeLast: options?.includeLast ?? false,
    };
    return await super.read(
      ref,
      optsWithIncludeLast as ReadWithIncludeLastOptions
    );
  }

  async search(
    query: object,
    options?: SearchWithIncludeLastOptions
  ): Promise<SearchResult<Hardpoint>> {
    const optsWithIncludeLast = {
      ...options,
      includeLast: options?.includeLast ?? false,
    };
    return await super.search(
      query,
      optsWithIncludeLast as ReadWithIncludeLastOptions
    );
  }
}
