// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import {
  MeasurementType,
  entityTypeInfoByEntityType,
  EntityType,
} from '../../common';
import { Illumass } from '../../illumass';

export class MeasurementTypeController extends BaseEntityController<MeasurementType> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.MEASUREMENT_TYPES));
  }
}
