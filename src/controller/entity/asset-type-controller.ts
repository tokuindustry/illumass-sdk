// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import {
  AssetType,
  entityTypeInfoByEntityType,
  EntityType,
} from '../../common';
import { Illumass } from '../../illumass';

export class AssetTypeController extends BaseEntityController<AssetType> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.ASSET_TYPES));
  }
}
