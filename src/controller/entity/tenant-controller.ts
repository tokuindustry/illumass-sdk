// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import { Tenant, entityTypeInfoByEntityType, EntityType } from '../../common';
import { Illumass } from '../../illumass';

export class TenantController extends BaseEntityController<Tenant> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.TENANTS));
  }
}
