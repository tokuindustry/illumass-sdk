// Copyright 2025 TOKU Systems Inc.
import {
  BaseEntityController,
  ReadWithIncludeLastOptions,
  SearchResult,
  SearchWithIncludeLastOptions,
} from '../base-entity-controller';
import {
  Signal,
  entityTypeInfoByEntityType,
  EntityType,
  EntityRef,
} from '../../common';
import { Illumass } from '../../illumass';

export class SignalController extends BaseEntityController<Signal> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.SIGNALS));
  }

  async read(
    ref: string | EntityRef,
    options?: ReadWithIncludeLastOptions
  ): Promise<Signal> {
    const optsWithIncludeLast = {
      ...options,
      includeLast: options?.includeLast ?? false,
    };
    return await super.read(
      ref,
      optsWithIncludeLast as ReadWithIncludeLastOptions
    );
  }

  async search(
    query: object,
    options?: SearchWithIncludeLastOptions
  ): Promise<SearchResult<Signal>> {
    const optsWithIncludeLast = {
      ...options,
      includeLast: options?.includeLast ?? false,
    };
    return await super.search(
      query,
      optsWithIncludeLast as ReadWithIncludeLastOptions
    );
  }
}
