// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import {
  Softpoint,
  entityTypeInfoByEntityType,
  EntityType,
} from '../../common';
import { Illumass } from '../../illumass';

export class SoftpointController extends BaseEntityController<Softpoint> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.SOFTPOINTS));
  }
}
