// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import { Asset, entityTypeInfoByEntityType, EntityType } from '../../common';
import { Illumass } from '../../illumass';

export class AssetController extends BaseEntityController<Asset> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.ASSETS));
  }
}
