// Copyright 2025 TOKU Systems Inc.
import { BaseEntityController } from '../base-entity-controller';
import { Country, entityTypeInfoByEntityType, EntityType } from '../../common';
import { Illumass } from '../../illumass';

export class CountryController extends BaseEntityController<Country> {
  constructor(readonly illumass: Illumass) {
    super(illumass, entityTypeInfoByEntityType(EntityType.COUNTRIES));
  }
}
