// Copyright 2022 TOKU Systems Inc.
import moment from 'moment';
import _get from 'lodash/get';
import _map from 'lodash/map';
import { User } from '../common';
import { Kuzzle } from 'kuzzle-sdk';
import { BaseIllumass } from '../base-illumass';
import { Illumass } from '../illumass';

export interface ApiKey {
  id: string;
  description: string;
  fingerprint: string;
  userId: string;
  expiresAt: number;
  ttl: number;
  token?: string;
}

export interface ApiKeySearchResults {
  hits: ApiKey[];
  total: number;
}

export interface AuthUser extends User {
  profileIds: string[];
}

export class AuthController {
  constructor(readonly kuzzle: Kuzzle, readonly illumass: BaseIllumass) {}

  getJwt(): string | undefined {
    return this.kuzzle.jwt;
  }

  async setJwt(jwt: string): Promise<boolean> {
    const result = await this.kuzzle.auth.checkToken(jwt);
    if (_get(result, ['valid'])) {
      this.kuzzle.jwt = jwt;
      return true;
    } else {
      return false;
    }
  }

  /**
   * parse expiresIn parameter
   * @param expiresIn expiresIn expressed as ISO 8601 duration or a number
   */
  private parseExpiresIn(expiresIn?: string | number): number {
    if (typeof expiresIn === 'string') {
      return moment.duration(expiresIn).asMilliseconds();
    }

    if (typeof expiresIn === 'number') {
      return expiresIn;
    }

    // Default to 1 hour
    return 3600000;
  }

  /**
   * Login with
   * @param email User email
   * @param password password
   * @param expiresIn duration expressed as an ISO 8601 duration or milliseconds
   */
  async login(
    email: string,
    password: string,
    expiresIn?: string | number
  ): Promise<string> {
    return await this.kuzzle.auth.login(
      'illumass',
      { email, password },
      this.parseExpiresIn(expiresIn)
    );
  }

  async logout(): Promise<void> {
    await this.kuzzle.auth.logout();
  }

  async checkToken(
    jwt?: string
  ): Promise<{ valid: boolean; state?: string; expiresAt: string }> {
    const result = await this.kuzzle.auth.checkToken(jwt);
    return {
      valid: _get(result, ['valid'], false),
      state: _get(result, ['state']),
      expiresAt: moment(
        _get(result, ['expiresAt'], _get(result, ['expires_at'])) // docs say "expires_at", but I've observed "expiresAt"
      )
        .utc()
        .toISOString(),
    };
  }

  /**
   * Exchange for a new token
   * @param options expiresIn is duration in ISO 8601 format
   */
  async refreshToken(options?: {
    expiresIn: string;
    queuable?: boolean;
  }): Promise<{ userId: string; expiresAt: string; jwt: string; ttl: number }> {
    const doc = await this.kuzzle.auth.refreshToken({
      expiresIn: this.parseExpiresIn(options?.expiresIn),
      queuable: options?.queuable,
    });

    return {
      userId: _get(doc, ['_id']),
      expiresAt: moment(_get(doc, ['expiresAt'], _get(doc, ['expires_at'])))
        .utc()
        .toISOString(),
      jwt: _get(doc, ['jwt']),
      ttl: _get(doc, ['ttl']),
    };
  }

  async createApiKey(
    description: string,
    options?: { expiresIn?: string | number; id?: string; refresh?: boolean }
  ): Promise<ApiKey> {
    const doc = await this.kuzzle.auth.createApiKey(description, {
      expiresIn: this.parseExpiresIn(options?.expiresIn),
      _id: options?.id,
      refresh: options?.refresh ? 'wait_for' : undefined,
    });

    return {
      id: _get(doc, ['_id']),
      userId: _get(doc, ['_source', 'userId']),
      description: _get(doc, ['_source', 'description']),
      fingerprint: _get(doc, ['_source', 'fingerprint']),
      expiresAt: _get(doc, ['_source', 'expiresAt']),
      ttl: _get(doc, ['_source', 'ttl']),
      token: _get(doc, ['_source', 'token']),
    };
  }

  async deleteApiKey(
    id: string,
    options?: { refresh?: boolean }
  ): Promise<null> {
    return await this.kuzzle.auth.deleteApiKey(id, {
      refresh: options?.refresh ? 'wait_for' : undefined,
    });
  }

  async searchApiKeys(
    query?: object,
    options?: { from?: number; size?: number }
  ): Promise<ApiKeySearchResults> {
    const searchResults = await this.kuzzle.auth.searchApiKeys(query, options);
    return {
      total: _get(searchResults, ['total'], 0),
      hits: _map(_get(searchResults, ['hits'], []), (hit) => {
        return {
          id: _get(hit, ['_id']),
          userId: _get(hit, ['_source', 'userId']),
          description: _get(hit, ['_source', 'description']),
          fingerprint: _get(hit, ['_source', 'fingerprint']),
          expiresAt: _get(hit, ['_source', 'expiresAt']),
          ttl: _get(hit, ['_source', 'ttl']),
        };
      }),
    };
  }

  async getCurrentUser(): Promise<AuthUser> {
    const kuser = await this.kuzzle.auth.getCurrentUser();
    const { ...fields } = await (this.illumass as Illumass).user.read(
      'user!' + kuser._id
    );
    return { ...fields, profileIds: kuser.profileIds };
  }
}
