// Copyright 2022 TOKU Systems Inc.
import moment from 'moment';
import { equestria, google } from '../time-series';
import Long from 'long';
import _map from 'lodash/map';
import _set from 'lodash/set';
import _get from 'lodash/get';

import protobuf = equestria.protobuf.common.v1;
import { EntityRef } from '../common';

export enum TemporalResolution {
  Hour,
  Minute,
  Second,
}

export interface ReportOptions {
  refresh?: boolean;
}

export interface SignalOptions {
  measuringUnit?: string; // One of 'source', 'tenant', or a measuring unit ref. Default is 'source'
}

export interface RawPoint {
  t: number;
  y: number;
}

export interface SummaryPoint {
  t: number;
  mean: number;
  min: number;
  max: number;
  variance: number;
  count: number;
}

// Seperate object for TimeSeries
// The reason for doing this instead of just working with array of points
// is we may want to
export interface TimeSeries<T> {
  points: T[];
  measuringUnit: EntityRef;
}

export function timestampToMoment(
  timestamp: google.protobuf.ITimestamp
): moment.Moment {
  if (typeof timestamp.seconds === 'number') {
    return moment(timestamp.seconds * 1000 + (timestamp.nanos ?? 0) / 1000000);
  } else if (Long.isLong(timestamp.seconds)) {
    return moment(
      (timestamp.seconds as Long).toNumber() * 1000 +
        (timestamp.nanos ?? 0) / 1000000
    );
  } else {
    throw new Error('Timestamp seconds type is invalid.');
  }
}

export function momentToTimestamp(
  m: moment.Moment
): google.protobuf.ITimestamp {
  const s = Math.floor(m.valueOf() / 1000);
  const ns = (m.valueOf() - s * 1000) * 1000000;
  return google.protobuf.Timestamp.create({
    seconds: s,
    nanos: ns,
  });
}

export function durationToMoment(
  duration: google.protobuf.IDuration
): moment.Duration {
  if (typeof duration.seconds === 'number') {
    return moment.duration(
      duration.seconds * 1000 + (duration.nanos ?? 0) / 1000000
    );
  } else if (Long.isLong(duration.seconds)) {
    return moment.duration(
      (duration.seconds as Long).toNumber() * 1000 +
        (duration.nanos ?? 0) / 1000000
    );
  } else {
    throw new Error('Duration seconds type is invalid.');
  }
}

export function momentToDuration(
  d: moment.Duration
): google.protobuf.IDuration {
  const ms = d.asMilliseconds();
  const s = Math.floor(ms / 1000);
  const ns = (ms - s * 1000) * 1000000;
  return google.protobuf.Duration.create({
    seconds: s,
    nanos: ns,
  });
}

function getIrregularPeriods(irregular: protobuf.IIrregularSlice): number[] {
  if (irregular.asDurations) {
    return _map(irregular.asDurations.durations, (duration) =>
      durationToMoment(duration).asMilliseconds()
    );
  } else if (irregular.asSeconds) {
    return _map(irregular.asSeconds.seconds, (s) => s * 1000);
  } else if (irregular.asMilliseconds) {
    return irregular.asMilliseconds.milliseconds ?? [];
  } else if (irregular.asMicroseconds) {
    return _map(irregular.asMicroseconds.microseconds, (us) => us / 1000);
  } else {
    throw new Error('Periods are missing from irregular slice.');
  }
}

function toTimeSeries<T, U>({
  timeSeries,
  transform,
  extractRegularData,
  extractIrregularData,
  dataLength,
}: {
  timeSeries: protobuf.ITimeSeries;
  transform: (t: number, i: number, data: U) => T;
  extractRegularData: (regular: protobuf.IRegularSlice) => U;
  extractIrregularData: (irregular: protobuf.IIrregularSlice) => U;
  dataLength(u: U): number;
}): TimeSeries<T> {
  const points: T[] = [];

  (timeSeries.slices ?? []).forEach((slice) => {
    if (slice.regular) {
      const start: moment.Moment = timestampToMoment(slice.regular.start ?? {});
      const period: moment.Duration = durationToMoment(
        slice.regular.period ?? {}
      );
      const data: U = extractRegularData(slice.regular);
      for (let i = 0; i < dataLength(data); i++) {
        points.push(
          transform(start.valueOf() + i * period.asMilliseconds(), i, data)
        );
      }
    } else if (slice.irregular) {
      const start: moment.Moment = timestampToMoment(
        slice.irregular.start ?? {}
      );
      const periods: number[] = getIrregularPeriods(slice.irregular ?? {});
      const data: U = extractIrregularData(slice.irregular);
      const length = dataLength(data);

      let time = start.valueOf();
      if (length > 0) {
        points.push(transform(time, 0, data));
      }

      for (let i = 1; i < length; i++) {
        time += periods[i - 1];
        points.push(transform(time, i, data));
      }
    } else {
      throw new Error('Time series slice must be regular or irregular.');
    }
  });

  return {
    points,
    measuringUnit: {
      entityType: 'measuring-unit',
      id: timeSeries.measuringUnit?.id ?? undefined,
      slug: timeSeries.measuringUnit?.slugs?.join('.'),
      shortName: timeSeries.measuringUnit?.shortName ?? undefined,
    },
  };
}

export function toProtobufTimeSeries(
  timeSeries: Partial<TimeSeries<RawPoint>>
): protobuf.TimeSeries {
  if (timeSeries.points !== undefined) {
    if (timeSeries.points.length < 1) {
      return protobuf.TimeSeries.create({});
    }

    const start = moment(timeSeries.points[0].t);
    const periods: number[] = [];
    for (let i = 1; i < timeSeries.points.length; i++) {
      periods.push(timeSeries.points[i].t - timeSeries.points[i - 1].t);
    }

    const pb = protobuf.TimeSeries.create({
      slices: [
        protobuf.Slice.create({
          irregular: protobuf.IrregularSlice.create({
            start: momentToTimestamp(start),
            asMilliseconds: protobuf.PeriodsAsMilliseconds.create({
              milliseconds: periods,
            }),
            raws: protobuf.RawValues.create({
              values: _map(timeSeries.points, (p) => p.y),
            }),
          }),
        }),
      ],
    });

    return pb;
  } else {
    throw new Error('Time serries is missing data points.');
  }
}

export function toSummaryPointTimeSeries(
  timeSeries: protobuf.TimeSeries
): TimeSeries<SummaryPoint> {
  return toTimeSeries<SummaryPoint, protobuf.ISummaryValues>({
    timeSeries,
    transform: (t: number, i: number, data: protobuf.ISummaryValues) => ({
      t,
      mean: data.meanValues ? data.meanValues[i] : 0,
      min: data.minValues ? data.minValues[i] : 0,
      max: data.maxValues ? data.maxValues[i] : 0,
      variance: data.variances ? data.variances[i] : 0,
      count: data.counts
        ? Long.isLong(data.counts[i])
          ? (data.counts[i] as Long).toNumber()
          : (data.counts[i] as number)
        : 0,
    }),
    extractRegularData: (regular: protobuf.IRegularSlice) =>
      regular.summaries ?? {},
    extractIrregularData: (irregular: protobuf.IIrregularSlice) =>
      irregular.summaries ?? {},
    dataLength: (data: protobuf.ISummaryValues) => data.counts?.length ?? 0,
  });
}

export function toRawPointTimeSeries(
  timeSeries: protobuf.TimeSeries
): TimeSeries<RawPoint> {
  return toTimeSeries<RawPoint, protobuf.IRawValues>({
    timeSeries,
    transform: (t: number, i: number, data: protobuf.IRawValues) => ({
      t,
      y: data.values ? data.values[i] : 0,
    }),
    extractRegularData: (regular: protobuf.IRegularSlice) => regular.raws ?? {},
    extractIrregularData: (irregular: protobuf.IIrregularSlice) =>
      irregular.raws ?? {},
    dataLength: (data: protobuf.IRawValues) => data.values?.length ?? 0,
  });
}

export class SignalDataController {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(readonly kuzzle: any) {}

  private nameOfResolution(r: TemporalResolution): string {
    switch (r) {
      case TemporalResolution.Hour:
        return 'hour';
      case TemporalResolution.Minute:
        return 'minute';
      case TemporalResolution.Second:
        return 'second';
    }
  }

  private setupEnd(
    request: object,
    end: moment.Moment | moment.Duration
  ): object {
    if (moment.isMoment(end)) {
      return _set(
        request,
        ['end'],
        (end as moment.Moment).clone().utc().toISOString()
      );
    } else if (moment.isDuration(end)) {
      return _set(
        request,
        ['duration'],
        (end as moment.Duration).toISOString()
      );
    } else {
      throw new Error(`Unrecognized end parameter. ${end}`);
    }
  }

  private applyOptions(request: object, options?: object): object {
    if (options !== undefined) {
      const fluttershyId = _get(options, ['fluttershyId']);
      if (fluttershyId !== undefined) {
        _set(request, ['fluttershyId'], fluttershyId);
      }

      const measuringUnit = _get(
        options,
        ['measuringUnit'],
        _get(options, ['measuring-unit'])
      );
      if (measuringUnit !== undefined) {
        _set(request, ['measuringUnit'], measuringUnit);
      }
    }

    return request;
  }

  async raw(
    ref: string,
    start: moment.Moment,
    end: moment.Moment,
    resolution: TemporalResolution,
    options?: SignalOptions
  ): Promise<TimeSeries<RawPoint>> {
    const request = {
      controller: 'rumble/signal',
      action: 'raw',
      ref,
      start: start.clone().utc().toISOString(),
      resolution: this.nameOfResolution(resolution),
    };

    const response = await this.kuzzle.query(
      this.setupEnd(this.applyOptions(request, options), end)
    );

    const b64 = _get(response, ['result']);
    if (b64 === undefined) {
      return { points: [], measuringUnit: { entityType: 'measuring-unit' } };
    } else {
      const buf = Buffer.from(b64, 'base64');
      const timeSeries = protobuf.TimeSeries.decode(buf);
      return toRawPointTimeSeries(timeSeries);
    }
  }

  async summary(
    ref: string,
    start: moment.Moment,
    end: moment.Moment,
    resolution: TemporalResolution,
    options?: SignalOptions
  ): Promise<TimeSeries<SummaryPoint>> {
    const request = {
      controller: 'rumble/signal',
      action: 'summary',
      ref,
      start: start.clone().utc().toISOString(),
      resolution: this.nameOfResolution(resolution),
    };

    const response = await this.kuzzle.query(
      this.setupEnd(this.applyOptions(request, options), end)
    );

    const b64 = _get(response, ['result']);
    if (b64 === undefined) {
      return {
        points: [],
        measuringUnit: {
          entityType: 'measuring-unit',
        },
      };
    } else {
      const buf = Buffer.from(b64, 'base64');
      const timeSeries = protobuf.TimeSeries.decode(buf);
      return toSummaryPointTimeSeries(timeSeries);
    }
  }

  async report(
    ref: string,
    timeSeries: TimeSeries<RawPoint>,
    options?: ReportOptions
  ): Promise<void> {
    const pb = toProtobufTimeSeries(timeSeries);

    if (pb.slices.length == 0) {
      return;
    }

    const buf = Buffer.from(protobuf.TimeSeries.encode(pb).finish());
    const request = {
      controller: 'rumble/signal',
      action: 'report',
      contentType: 'application/x-illumass-time-series',
      ref,
      body: buf,
    };

    if (options?.refresh) {
      _set(request, ['waitForWrite'], 'signal');
    }

    await this.kuzzle.query(request);
  }
}
