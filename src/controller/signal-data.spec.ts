// Copyright 2020 TOKU Systems Inc.

import {
  toRawPointTimeSeries,
  toSummaryPointTimeSeries,
  toProtobufTimeSeries,
  RawPoint,
  momentToTimestamp,
  momentToDuration,
  timestampToMoment,
  TimeSeries,
} from './signal-data';
import { equestria } from '../time-series';
import moment from 'moment';
import Long from 'long';
import protobuf = equestria.protobuf.common.v1;

describe('Signal data', () => {
  test('decode raw point time series', () => {
    const start = moment().utc().startOf('day').subtract(1, 'day');

    const pb = protobuf.TimeSeries.create({
      slices: [
        protobuf.Slice.create({
          regular: protobuf.RegularSlice.create({
            start: momentToTimestamp(start),
            period: momentToDuration(moment.duration('PT5S')),
            raws: protobuf.RawValues.create({
              values: Array(3600 / 5)
                .fill(0)
                .map((_, i) => i * 5),
            }),
          }),
        }),
        protobuf.Slice.create({
          irregular: protobuf.IrregularSlice.create({
            start: momentToTimestamp(start.clone().add(1, 'hour')),
            asSeconds: protobuf.PeriodsAsSeconds.create({
              seconds: Array(720)
                .fill(0)
                .map((_, i) => (i % 5) + 1),
            }),
            raws: protobuf.RawValues.create({
              values: Array(720)
                .fill(0)
                .map((_, i) => i),
            }),
          }),
        }),
      ],
    });

    const timeSeries = toRawPointTimeSeries(pb);

    expect(timeSeries.points.length).toEqual(1440);

    const regular = timeSeries.points.slice(0, 720);
    regular.forEach((p, i) => {
      expect(p.t).toEqual(start.valueOf() + i * 5000);
      expect(p.y).toEqual(i * 5);
    });

    const irregular = timeSeries.points.slice(720, 1440);
    const currentTime = start.clone().add(1, 'hour');
    irregular.forEach((p, i) => {
      expect(p.t).toEqual(currentTime.valueOf());
      expect(p.y).toEqual(i);
      currentTime.add((i % 5) + 1, 's');
    });
  });

  test('decode summary point time series', () => {
    const start = moment().utc().startOf('day').subtract(1, 'day');

    const pb = protobuf.TimeSeries.create({
      slices: [
        protobuf.Slice.create({
          regular: protobuf.RegularSlice.create({
            start: momentToTimestamp(start),
            period: momentToDuration(moment.duration('PT5S')),
            summaries: protobuf.SummaryValues.create({
              meanValues: Array(3600 / 5)
                .fill(0)
                .map((_, i) => i * 5),
              minValues: Array(3600 / 5)
                .fill(0)
                .map((_, i) => i * 5 - 10),
              maxValues: Array(3600 / 5)
                .fill(0)
                .map((_, i) => i * 5 + 10),
              variances: Array(3600 / 5)
                .fill(0)
                .map((_, i) => i % 5),
              counts: Array(3600 / 5)
                .fill(0)
                .map((_, i) => (i % 2 == 0 ? 100 : Long.fromNumber(100))),
            }),
          }),
        }),
        protobuf.Slice.create({
          irregular: protobuf.IrregularSlice.create({
            start: momentToTimestamp(start.clone().add(1, 'hour')),
            asSeconds: protobuf.PeriodsAsSeconds.create({
              seconds: Array(720)
                .fill(0)
                .map((_, i) => (i % 5) + 1),
            }),
            summaries: protobuf.SummaryValues.create({
              meanValues: Array(720)
                .fill(0)
                .map((_, i) => i),
              minValues: Array(720)
                .fill(0)
                .map((_, i) => i - 10),
              maxValues: Array(720)
                .fill(0)
                .map((_, i) => i + 10),
              variances: Array(720)
                .fill(0)
                .map((_, i) => i % 5),
              counts: Array(720)
                .fill(0)
                .map((_, i) => (i % 2 == 0 ? 100 : Long.fromNumber(100))),
            }),
          }),
        }),
      ],
    });

    const timeSeries = toSummaryPointTimeSeries(pb);

    expect(timeSeries.points.length).toEqual(1440);

    const regular = timeSeries.points.slice(0, 720);
    regular.forEach((p, i) => {
      expect(p.t).toEqual(start.valueOf() + i * 5000);
      expect(p.mean).toEqual(i * 5);
      expect(p.min).toEqual(i * 5 - 10);
      expect(p.max).toEqual(i * 5 + 10);
      expect(p.variance).toEqual(i % 5);
      expect(p.count).toEqual(100);
    });

    const irregular = timeSeries.points.slice(720, 1440);
    const currentTime = start.clone().add(1, 'hour');
    irregular.forEach((p, i) => {
      expect(p.t).toEqual(currentTime.valueOf());
      expect(p.mean).toEqual(i);
      expect(p.min).toEqual(i - 10);
      expect(p.max).toEqual(i + 10);
      expect(p.variance).toEqual(i % 5);
      expect(p.count).toEqual(100);
      currentTime.add((i % 5) + 1, 's');
    });
  });

  test('encode time series', () => {
    const start = moment().utc().startOf('day').subtract(1, 'day');

    const timeSeries: TimeSeries<RawPoint> = {
      points: Array(3600)
        .fill(0)
        .map((_, i) => {
          return {
            t: start.valueOf() + i * 1000,
            y: i,
          };
        }),
      measuringUnit: {
        entityType: 'measuringUnit',
      },
    };

    const pb = toProtobufTimeSeries(timeSeries);
    expect(pb.slices.length).toEqual(1);
    expect(pb.slices[0].irregular).toBeDefined();
    expect(pb.slices[0].regular).toBeNull();
    expect(
      pb.slices[0].irregular?.asMilliseconds?.milliseconds?.length
    ).toEqual(3599);
    expect(pb.slices[0].irregular?.raws?.values?.length).toEqual(3600);
    if (pb.slices[0].irregular?.start) {
      expect(
        timestampToMoment(pb.slices[0].irregular?.start).valueOf()
      ).toEqual(start.valueOf());
    } else {
      expect(pb.slices[0].irregular?.start).toBeDefined();
    }
    for (let i = 0; i < 3599; i++) {
      expect(
        (pb.slices[0].irregular?.asMilliseconds?.milliseconds ?? [])[i]
      ).toEqual(1000);
    }
  });
});
