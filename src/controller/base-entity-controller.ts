// Copyright 2020 TOKU Systems, Inc.
import {
  EntityTypeInfo,
  EntityRef,
  parseEntityRef,
  formatEntityRef,
} from '../common';
import _get from 'lodash/get';
import _map from 'lodash/map';
import _set from 'lodash/set';
import { Illumass } from '../illumass';
import {
  SearchResult as KuzzleSearchResult,
  KHit,
  JSONObject,
} from 'kuzzle-sdk';

// Avoid using 'scroll'. It can exhaust memory resources.
export interface SearchOptions {
  from?: number;
  size?: number;
  scroll?: string;
  [key: string]: string | number | boolean | undefined | null;
}

export interface ReadOptions {
  [key: string]: string | number | boolean | undefined | null;
}

export interface SearchWithIncludeLastOptions extends SearchOptions {
  includeLast?: boolean;
}

export interface ReadWithIncludeLastOptions extends ReadOptions {
  includeLast?: boolean;
}

export interface SearchHit<T> {
  _id: string;
  _score: number;
  _source: T;
}

export interface SearchResult<T> {
  hits: SearchHit<T>[];
  total: number;
  fetched: number;
  next: () => Promise<SearchResult<T> | null>;
}

class BaseSearchResult<T> implements SearchResult<T> {
  readonly hits: SearchHit<T>[];
  readonly total: number;
  readonly fetched: number;

  constructor(
    readonly searchResults: KuzzleSearchResult<KHit<Record<string, unknown>>>,
    readonly info: EntityTypeInfo
  ) {
    this.total = searchResults.total;
    this.fetched = searchResults.fetched;
    this.hits = _map(searchResults.hits, (hit) => {
      const source = hit._source ?? {};
      _set(source, ['entityType'], info.hyphenatedSingular);
      return {
        _id: hit._id ?? '',
        _score: hit._score,
        _source: source as T,
      };
    });
  }

  async next(): Promise<SearchResult<T> | null> {
    const sr = await this.searchResults.next();
    if (sr) {
      return new BaseSearchResult<T>(sr, this.info);
    } else {
      return null;
    }
  }
}

export class BaseEntityController<T extends JSONObject> {
  private readonly index: string = 'rumble';

  get volatile(): object {
    const tenantFilterId = this.illumass.tenantIdFilter;
    if (tenantFilterId) {
      return {
        rumble: {
          tenantId: tenantFilterId,
        },
      };
    } else {
      return {};
    }
  }

  constructor(
    readonly illumass: Illumass,
    readonly entityTypeInfo: EntityTypeInfo
  ) {}

  async create(entity: T): Promise<T> {
    const response = await this.illumass.kuzzle.query({
      controller: 'rumble/entity',
      action: 'create',
      body: entity,
      volatile: this.volatile,
    });

    return _get(response, ['result'], {}) as T;
  }

  async update(ref: string | EntityRef, change: T): Promise<T> {
    const request = {
      controller: 'rumble/entity',
      action: 'update',
      body: change,
      volatile: this.volatile,
    };

    if (typeof ref === 'string') {
      _set(request, ['ref'], ref);
    } else {
      _set(request, ['ref'], formatEntityRef(ref));
    }

    const response = await this.illumass.kuzzle.query(request);

    return _get(response, ['result'], {}) as T;
  }

  async archive(ref: string | EntityRef): Promise<T> {
    const request = {
      controller: 'rumble/entity',
      action: 'archive',
      volatile: this.volatile,
    };

    if (typeof ref === 'string') {
      _set(request, ['ref'], ref);
    } else {
      _set(request, ['ref'], formatEntityRef(ref));
    }

    const response = await this.illumass.kuzzle.query(request);

    return _get(response, ['result'], {}) as T;
  }

  async unarchive(ref: string | EntityRef): Promise<T> {
    const request = {
      controller: 'rumble/entity',
      action: 'unarchive',
      volatile: this.volatile,
    };

    if (typeof ref === 'string') {
      _set(request, ['ref'], ref);
    } else {
      _set(request, ['ref'], formatEntityRef(ref));
    }

    const response = await this.illumass.kuzzle.query(request);

    return _get(response, ['result'], {}) as T;
  }

  async read(ref: string | EntityRef, options?: ReadOptions): Promise<T> {
    const normalizedRef = ((): EntityRef => {
      if (typeof ref === 'string') {
        return parseEntityRef(ref);
      } else {
        return ref;
      }
    })();

    if (
      this.entityTypeInfo.singular !== normalizedRef.entityType &&
      this.entityTypeInfo.plural !== normalizedRef.entityType &&
      this.entityTypeInfo.hyphenatedSingular !== normalizedRef.entityType &&
      this.entityTypeInfo.hyphenatedSingular
    ) {
      throw new Error(
        `Cannot fetch ${normalizedRef.entityType} with ${this.entityTypeInfo.singular} controller.`
      );
    }

    const opts = Object.assign(
      {
        volatile: {
          ...this.volatile,
          includeLast: options?.includeLast,
        },
      },
      options
    );

    if (normalizedRef.id) {
      const response = await this.illumass.kuzzle.query(
        {
          controller: 'rumble/entity',
          action: 'read',
          ref,
        },
        opts
      );

      const r = _get(response, ['result']);
      if (r !== undefined) {
        _set(r, ['entityType'], this.entityTypeInfo.hyphenatedSingular);
      }
      return r as T;
    } else if (normalizedRef.slug) {
      const q = _set(
        {},
        ['query', 'term', 'slug', 'value'],
        normalizedRef.slug
      );

      const response = await this.illumass.kuzzle.document.search(
        this.index,
        this.entityTypeInfo.hyphenatedPlural,
        q,
        opts
      );

      const totalHits = _get(response, ['total'], 0);
      if (totalHits !== 1) {
        throw new Error(`There should be 1 hit, but found ${totalHits}.`);
      }

      const r = _get(response, ['hits', '0', '_source']);
      _set(r, ['entityType'], this.entityTypeInfo.hyphenatedSingular);
      return r as T;
    } else {
      throw new Error(
        `Entity reference is missing id and slug. ${JSON.stringify(ref)}`
      );
    }
  }

  async search(
    query: object,
    options?: SearchOptions
  ): Promise<SearchResult<T>> {
    const sr: KuzzleSearchResult<KHit<Record<string, unknown>>> =
      await this.illumass.kuzzle.document.search(
        this.index,
        this.entityTypeInfo.hyphenatedPlural,
        query,
        Object.assign(
          {
            volatile: {
              ...this.volatile,
              includeLast: options?.includeLast,
            },
          },
          options
        )
      );

    return new BaseSearchResult<T>(sr, this.entityTypeInfo);
  }
}
