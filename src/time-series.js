/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
(function(global, factory) { /* global define, require, module */

    /* AMD */ if (typeof define === 'function' && define.amd)
        define(["protobufjs/minimal"], factory);

    /* CommonJS */ else if (typeof require === 'function' && typeof module === 'object' && module && module.exports)
        module.exports = factory(require("protobufjs/minimal"));

})(this, function($protobuf) {
    "use strict";

    // Common aliases
    var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;
    
    // Exported root namespace
    var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
    
    $root.equestria = (function() {
    
        /**
         * Namespace equestria.
         * @exports equestria
         * @namespace
         */
        var equestria = {};
    
        equestria.protobuf = (function() {
    
            /**
             * Namespace protobuf.
             * @memberof equestria
             * @namespace
             */
            var protobuf = {};
    
            protobuf.common = (function() {
    
                /**
                 * Namespace common.
                 * @memberof equestria.protobuf
                 * @namespace
                 */
                var common = {};
    
                common.v1 = (function() {
    
                    /**
                     * Namespace v1.
                     * @memberof equestria.protobuf.common
                     * @namespace
                     */
                    var v1 = {};
    
                    v1.Slice = (function() {
    
                        /**
                         * Properties of a Slice.
                         * @memberof equestria.protobuf.common.v1
                         * @interface ISlice
                         * @property {equestria.protobuf.common.v1.IRegularSlice|null} [regular] Slice regular
                         * @property {equestria.protobuf.common.v1.IIrregularSlice|null} [irregular] Slice irregular
                         */
    
                        /**
                         * Constructs a new Slice.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a Slice.
                         * @implements ISlice
                         * @constructor
                         * @param {equestria.protobuf.common.v1.ISlice=} [properties] Properties to set
                         */
                        function Slice(properties) {
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * Slice regular.
                         * @member {equestria.protobuf.common.v1.IRegularSlice|null|undefined} regular
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @instance
                         */
                        Slice.prototype.regular = null;
    
                        /**
                         * Slice irregular.
                         * @member {equestria.protobuf.common.v1.IIrregularSlice|null|undefined} irregular
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @instance
                         */
                        Slice.prototype.irregular = null;
    
                        // OneOf field names bound to virtual getters and setters
                        var $oneOfFields;
    
                        /**
                         * Slice kind.
                         * @member {"regular"|"irregular"|undefined} kind
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @instance
                         */
                        Object.defineProperty(Slice.prototype, "kind", {
                            get: $util.oneOfGetter($oneOfFields = ["regular", "irregular"]),
                            set: $util.oneOfSetter($oneOfFields)
                        });
    
                        /**
                         * Creates a new Slice instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {equestria.protobuf.common.v1.ISlice=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.Slice} Slice instance
                         */
                        Slice.create = function create(properties) {
                            return new Slice(properties);
                        };
    
                        /**
                         * Encodes the specified Slice message. Does not implicitly {@link equestria.protobuf.common.v1.Slice.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {equestria.protobuf.common.v1.ISlice} message Slice message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        Slice.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.regular != null && Object.hasOwnProperty.call(message, "regular"))
                                $root.equestria.protobuf.common.v1.RegularSlice.encode(message.regular, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                            if (message.irregular != null && Object.hasOwnProperty.call(message, "irregular"))
                                $root.equestria.protobuf.common.v1.IrregularSlice.encode(message.irregular, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
                            return writer;
                        };
    
                        /**
                         * Encodes the specified Slice message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.Slice.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {equestria.protobuf.common.v1.ISlice} message Slice message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        Slice.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a Slice message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.Slice} Slice
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        Slice.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.Slice();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    message.regular = $root.equestria.protobuf.common.v1.RegularSlice.decode(reader, reader.uint32());
                                    break;
                                case 2:
                                    message.irregular = $root.equestria.protobuf.common.v1.IrregularSlice.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a Slice message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.Slice} Slice
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        Slice.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a Slice message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        Slice.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            var properties = {};
                            if (message.regular != null && message.hasOwnProperty("regular")) {
                                properties.kind = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.RegularSlice.verify(message.regular);
                                    if (error)
                                        return "regular." + error;
                                }
                            }
                            if (message.irregular != null && message.hasOwnProperty("irregular")) {
                                if (properties.kind === 1)
                                    return "kind: multiple values";
                                properties.kind = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.IrregularSlice.verify(message.irregular);
                                    if (error)
                                        return "irregular." + error;
                                }
                            }
                            return null;
                        };
    
                        /**
                         * Creates a Slice message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.Slice} Slice
                         */
                        Slice.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.Slice)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.Slice();
                            if (object.regular != null) {
                                if (typeof object.regular !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.Slice.regular: object expected");
                                message.regular = $root.equestria.protobuf.common.v1.RegularSlice.fromObject(object.regular);
                            }
                            if (object.irregular != null) {
                                if (typeof object.irregular !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.Slice.irregular: object expected");
                                message.irregular = $root.equestria.protobuf.common.v1.IrregularSlice.fromObject(object.irregular);
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a Slice message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @static
                         * @param {equestria.protobuf.common.v1.Slice} message Slice
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        Slice.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (message.regular != null && message.hasOwnProperty("regular")) {
                                object.regular = $root.equestria.protobuf.common.v1.RegularSlice.toObject(message.regular, options);
                                if (options.oneofs)
                                    object.kind = "regular";
                            }
                            if (message.irregular != null && message.hasOwnProperty("irregular")) {
                                object.irregular = $root.equestria.protobuf.common.v1.IrregularSlice.toObject(message.irregular, options);
                                if (options.oneofs)
                                    object.kind = "irregular";
                            }
                            return object;
                        };
    
                        /**
                         * Converts this Slice to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.Slice
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        Slice.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return Slice;
                    })();
    
                    v1.RawValues = (function() {
    
                        /**
                         * Properties of a RawValues.
                         * @memberof equestria.protobuf.common.v1
                         * @interface IRawValues
                         * @property {Array.<number>|null} [values] RawValues values
                         */
    
                        /**
                         * Constructs a new RawValues.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a RawValues.
                         * @implements IRawValues
                         * @constructor
                         * @param {equestria.protobuf.common.v1.IRawValues=} [properties] Properties to set
                         */
                        function RawValues(properties) {
                            this.values = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * RawValues values.
                         * @member {Array.<number>} values
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @instance
                         */
                        RawValues.prototype.values = $util.emptyArray;
    
                        /**
                         * Creates a new RawValues instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {equestria.protobuf.common.v1.IRawValues=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.RawValues} RawValues instance
                         */
                        RawValues.create = function create(properties) {
                            return new RawValues(properties);
                        };
    
                        /**
                         * Encodes the specified RawValues message. Does not implicitly {@link equestria.protobuf.common.v1.RawValues.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {equestria.protobuf.common.v1.IRawValues} message RawValues message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        RawValues.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.values != null && message.values.length) {
                                writer.uint32(/* id 1, wireType 2 =*/10).fork();
                                for (var i = 0; i < message.values.length; ++i)
                                    writer.float(message.values[i]);
                                writer.ldelim();
                            }
                            return writer;
                        };
    
                        /**
                         * Encodes the specified RawValues message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.RawValues.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {equestria.protobuf.common.v1.IRawValues} message RawValues message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        RawValues.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a RawValues message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.RawValues} RawValues
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        RawValues.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.RawValues();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    if (!(message.values && message.values.length))
                                        message.values = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.values.push(reader.float());
                                    } else
                                        message.values.push(reader.float());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a RawValues message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.RawValues} RawValues
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        RawValues.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a RawValues message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        RawValues.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.values != null && message.hasOwnProperty("values")) {
                                if (!Array.isArray(message.values))
                                    return "values: array expected";
                                for (var i = 0; i < message.values.length; ++i)
                                    if (typeof message.values[i] !== "number")
                                        return "values: number[] expected";
                            }
                            return null;
                        };
    
                        /**
                         * Creates a RawValues message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.RawValues} RawValues
                         */
                        RawValues.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.RawValues)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.RawValues();
                            if (object.values) {
                                if (!Array.isArray(object.values))
                                    throw TypeError(".equestria.protobuf.common.v1.RawValues.values: array expected");
                                message.values = [];
                                for (var i = 0; i < object.values.length; ++i)
                                    message.values[i] = Number(object.values[i]);
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a RawValues message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @static
                         * @param {equestria.protobuf.common.v1.RawValues} message RawValues
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        RawValues.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults)
                                object.values = [];
                            if (message.values && message.values.length) {
                                object.values = [];
                                for (var j = 0; j < message.values.length; ++j)
                                    object.values[j] = options.json && !isFinite(message.values[j]) ? String(message.values[j]) : message.values[j];
                            }
                            return object;
                        };
    
                        /**
                         * Converts this RawValues to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.RawValues
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        RawValues.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return RawValues;
                    })();
    
                    v1.SummaryValues = (function() {
    
                        /**
                         * Properties of a SummaryValues.
                         * @memberof equestria.protobuf.common.v1
                         * @interface ISummaryValues
                         * @property {Array.<number>|null} [meanValues] SummaryValues meanValues
                         * @property {Array.<number>|null} [minValues] SummaryValues minValues
                         * @property {Array.<number>|null} [maxValues] SummaryValues maxValues
                         * @property {Array.<number>|null} [variances] SummaryValues variances
                         * @property {Array.<number|Long>|null} [counts] SummaryValues counts
                         */
    
                        /**
                         * Constructs a new SummaryValues.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a SummaryValues.
                         * @implements ISummaryValues
                         * @constructor
                         * @param {equestria.protobuf.common.v1.ISummaryValues=} [properties] Properties to set
                         */
                        function SummaryValues(properties) {
                            this.meanValues = [];
                            this.minValues = [];
                            this.maxValues = [];
                            this.variances = [];
                            this.counts = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * SummaryValues meanValues.
                         * @member {Array.<number>} meanValues
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @instance
                         */
                        SummaryValues.prototype.meanValues = $util.emptyArray;
    
                        /**
                         * SummaryValues minValues.
                         * @member {Array.<number>} minValues
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @instance
                         */
                        SummaryValues.prototype.minValues = $util.emptyArray;
    
                        /**
                         * SummaryValues maxValues.
                         * @member {Array.<number>} maxValues
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @instance
                         */
                        SummaryValues.prototype.maxValues = $util.emptyArray;
    
                        /**
                         * SummaryValues variances.
                         * @member {Array.<number>} variances
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @instance
                         */
                        SummaryValues.prototype.variances = $util.emptyArray;
    
                        /**
                         * SummaryValues counts.
                         * @member {Array.<number|Long>} counts
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @instance
                         */
                        SummaryValues.prototype.counts = $util.emptyArray;
    
                        /**
                         * Creates a new SummaryValues instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {equestria.protobuf.common.v1.ISummaryValues=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.SummaryValues} SummaryValues instance
                         */
                        SummaryValues.create = function create(properties) {
                            return new SummaryValues(properties);
                        };
    
                        /**
                         * Encodes the specified SummaryValues message. Does not implicitly {@link equestria.protobuf.common.v1.SummaryValues.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {equestria.protobuf.common.v1.ISummaryValues} message SummaryValues message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        SummaryValues.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.meanValues != null && message.meanValues.length) {
                                writer.uint32(/* id 1, wireType 2 =*/10).fork();
                                for (var i = 0; i < message.meanValues.length; ++i)
                                    writer.float(message.meanValues[i]);
                                writer.ldelim();
                            }
                            if (message.minValues != null && message.minValues.length) {
                                writer.uint32(/* id 2, wireType 2 =*/18).fork();
                                for (var i = 0; i < message.minValues.length; ++i)
                                    writer.float(message.minValues[i]);
                                writer.ldelim();
                            }
                            if (message.maxValues != null && message.maxValues.length) {
                                writer.uint32(/* id 3, wireType 2 =*/26).fork();
                                for (var i = 0; i < message.maxValues.length; ++i)
                                    writer.float(message.maxValues[i]);
                                writer.ldelim();
                            }
                            if (message.variances != null && message.variances.length) {
                                writer.uint32(/* id 4, wireType 2 =*/34).fork();
                                for (var i = 0; i < message.variances.length; ++i)
                                    writer.float(message.variances[i]);
                                writer.ldelim();
                            }
                            if (message.counts != null && message.counts.length) {
                                writer.uint32(/* id 5, wireType 2 =*/42).fork();
                                for (var i = 0; i < message.counts.length; ++i)
                                    writer.fixed64(message.counts[i]);
                                writer.ldelim();
                            }
                            return writer;
                        };
    
                        /**
                         * Encodes the specified SummaryValues message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.SummaryValues.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {equestria.protobuf.common.v1.ISummaryValues} message SummaryValues message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        SummaryValues.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a SummaryValues message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.SummaryValues} SummaryValues
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        SummaryValues.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.SummaryValues();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    if (!(message.meanValues && message.meanValues.length))
                                        message.meanValues = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.meanValues.push(reader.float());
                                    } else
                                        message.meanValues.push(reader.float());
                                    break;
                                case 2:
                                    if (!(message.minValues && message.minValues.length))
                                        message.minValues = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.minValues.push(reader.float());
                                    } else
                                        message.minValues.push(reader.float());
                                    break;
                                case 3:
                                    if (!(message.maxValues && message.maxValues.length))
                                        message.maxValues = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.maxValues.push(reader.float());
                                    } else
                                        message.maxValues.push(reader.float());
                                    break;
                                case 4:
                                    if (!(message.variances && message.variances.length))
                                        message.variances = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.variances.push(reader.float());
                                    } else
                                        message.variances.push(reader.float());
                                    break;
                                case 5:
                                    if (!(message.counts && message.counts.length))
                                        message.counts = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.counts.push(reader.fixed64());
                                    } else
                                        message.counts.push(reader.fixed64());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a SummaryValues message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.SummaryValues} SummaryValues
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        SummaryValues.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a SummaryValues message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        SummaryValues.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.meanValues != null && message.hasOwnProperty("meanValues")) {
                                if (!Array.isArray(message.meanValues))
                                    return "meanValues: array expected";
                                for (var i = 0; i < message.meanValues.length; ++i)
                                    if (typeof message.meanValues[i] !== "number")
                                        return "meanValues: number[] expected";
                            }
                            if (message.minValues != null && message.hasOwnProperty("minValues")) {
                                if (!Array.isArray(message.minValues))
                                    return "minValues: array expected";
                                for (var i = 0; i < message.minValues.length; ++i)
                                    if (typeof message.minValues[i] !== "number")
                                        return "minValues: number[] expected";
                            }
                            if (message.maxValues != null && message.hasOwnProperty("maxValues")) {
                                if (!Array.isArray(message.maxValues))
                                    return "maxValues: array expected";
                                for (var i = 0; i < message.maxValues.length; ++i)
                                    if (typeof message.maxValues[i] !== "number")
                                        return "maxValues: number[] expected";
                            }
                            if (message.variances != null && message.hasOwnProperty("variances")) {
                                if (!Array.isArray(message.variances))
                                    return "variances: array expected";
                                for (var i = 0; i < message.variances.length; ++i)
                                    if (typeof message.variances[i] !== "number")
                                        return "variances: number[] expected";
                            }
                            if (message.counts != null && message.hasOwnProperty("counts")) {
                                if (!Array.isArray(message.counts))
                                    return "counts: array expected";
                                for (var i = 0; i < message.counts.length; ++i)
                                    if (!$util.isInteger(message.counts[i]) && !(message.counts[i] && $util.isInteger(message.counts[i].low) && $util.isInteger(message.counts[i].high)))
                                        return "counts: integer|Long[] expected";
                            }
                            return null;
                        };
    
                        /**
                         * Creates a SummaryValues message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.SummaryValues} SummaryValues
                         */
                        SummaryValues.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.SummaryValues)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.SummaryValues();
                            if (object.meanValues) {
                                if (!Array.isArray(object.meanValues))
                                    throw TypeError(".equestria.protobuf.common.v1.SummaryValues.meanValues: array expected");
                                message.meanValues = [];
                                for (var i = 0; i < object.meanValues.length; ++i)
                                    message.meanValues[i] = Number(object.meanValues[i]);
                            }
                            if (object.minValues) {
                                if (!Array.isArray(object.minValues))
                                    throw TypeError(".equestria.protobuf.common.v1.SummaryValues.minValues: array expected");
                                message.minValues = [];
                                for (var i = 0; i < object.minValues.length; ++i)
                                    message.minValues[i] = Number(object.minValues[i]);
                            }
                            if (object.maxValues) {
                                if (!Array.isArray(object.maxValues))
                                    throw TypeError(".equestria.protobuf.common.v1.SummaryValues.maxValues: array expected");
                                message.maxValues = [];
                                for (var i = 0; i < object.maxValues.length; ++i)
                                    message.maxValues[i] = Number(object.maxValues[i]);
                            }
                            if (object.variances) {
                                if (!Array.isArray(object.variances))
                                    throw TypeError(".equestria.protobuf.common.v1.SummaryValues.variances: array expected");
                                message.variances = [];
                                for (var i = 0; i < object.variances.length; ++i)
                                    message.variances[i] = Number(object.variances[i]);
                            }
                            if (object.counts) {
                                if (!Array.isArray(object.counts))
                                    throw TypeError(".equestria.protobuf.common.v1.SummaryValues.counts: array expected");
                                message.counts = [];
                                for (var i = 0; i < object.counts.length; ++i)
                                    if ($util.Long)
                                        (message.counts[i] = $util.Long.fromValue(object.counts[i])).unsigned = false;
                                    else if (typeof object.counts[i] === "string")
                                        message.counts[i] = parseInt(object.counts[i], 10);
                                    else if (typeof object.counts[i] === "number")
                                        message.counts[i] = object.counts[i];
                                    else if (typeof object.counts[i] === "object")
                                        message.counts[i] = new $util.LongBits(object.counts[i].low >>> 0, object.counts[i].high >>> 0).toNumber();
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a SummaryValues message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @static
                         * @param {equestria.protobuf.common.v1.SummaryValues} message SummaryValues
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        SummaryValues.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults) {
                                object.meanValues = [];
                                object.minValues = [];
                                object.maxValues = [];
                                object.variances = [];
                                object.counts = [];
                            }
                            if (message.meanValues && message.meanValues.length) {
                                object.meanValues = [];
                                for (var j = 0; j < message.meanValues.length; ++j)
                                    object.meanValues[j] = options.json && !isFinite(message.meanValues[j]) ? String(message.meanValues[j]) : message.meanValues[j];
                            }
                            if (message.minValues && message.minValues.length) {
                                object.minValues = [];
                                for (var j = 0; j < message.minValues.length; ++j)
                                    object.minValues[j] = options.json && !isFinite(message.minValues[j]) ? String(message.minValues[j]) : message.minValues[j];
                            }
                            if (message.maxValues && message.maxValues.length) {
                                object.maxValues = [];
                                for (var j = 0; j < message.maxValues.length; ++j)
                                    object.maxValues[j] = options.json && !isFinite(message.maxValues[j]) ? String(message.maxValues[j]) : message.maxValues[j];
                            }
                            if (message.variances && message.variances.length) {
                                object.variances = [];
                                for (var j = 0; j < message.variances.length; ++j)
                                    object.variances[j] = options.json && !isFinite(message.variances[j]) ? String(message.variances[j]) : message.variances[j];
                            }
                            if (message.counts && message.counts.length) {
                                object.counts = [];
                                for (var j = 0; j < message.counts.length; ++j)
                                    if (typeof message.counts[j] === "number")
                                        object.counts[j] = options.longs === String ? String(message.counts[j]) : message.counts[j];
                                    else
                                        object.counts[j] = options.longs === String ? $util.Long.prototype.toString.call(message.counts[j]) : options.longs === Number ? new $util.LongBits(message.counts[j].low >>> 0, message.counts[j].high >>> 0).toNumber() : message.counts[j];
                            }
                            return object;
                        };
    
                        /**
                         * Converts this SummaryValues to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.SummaryValues
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        SummaryValues.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return SummaryValues;
                    })();
    
                    v1.RegularSlice = (function() {
    
                        /**
                         * Properties of a RegularSlice.
                         * @memberof equestria.protobuf.common.v1
                         * @interface IRegularSlice
                         * @property {google.protobuf.ITimestamp|null} [start] RegularSlice start
                         * @property {google.protobuf.IDuration|null} [period] RegularSlice period
                         * @property {equestria.protobuf.common.v1.IRawValues|null} [raws] RegularSlice raws
                         * @property {equestria.protobuf.common.v1.ISummaryValues|null} [summaries] RegularSlice summaries
                         */
    
                        /**
                         * Constructs a new RegularSlice.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a RegularSlice.
                         * @implements IRegularSlice
                         * @constructor
                         * @param {equestria.protobuf.common.v1.IRegularSlice=} [properties] Properties to set
                         */
                        function RegularSlice(properties) {
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * RegularSlice start.
                         * @member {google.protobuf.ITimestamp|null|undefined} start
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @instance
                         */
                        RegularSlice.prototype.start = null;
    
                        /**
                         * RegularSlice period.
                         * @member {google.protobuf.IDuration|null|undefined} period
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @instance
                         */
                        RegularSlice.prototype.period = null;
    
                        /**
                         * RegularSlice raws.
                         * @member {equestria.protobuf.common.v1.IRawValues|null|undefined} raws
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @instance
                         */
                        RegularSlice.prototype.raws = null;
    
                        /**
                         * RegularSlice summaries.
                         * @member {equestria.protobuf.common.v1.ISummaryValues|null|undefined} summaries
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @instance
                         */
                        RegularSlice.prototype.summaries = null;
    
                        // OneOf field names bound to virtual getters and setters
                        var $oneOfFields;
    
                        /**
                         * RegularSlice data.
                         * @member {"raws"|"summaries"|undefined} data
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @instance
                         */
                        Object.defineProperty(RegularSlice.prototype, "data", {
                            get: $util.oneOfGetter($oneOfFields = ["raws", "summaries"]),
                            set: $util.oneOfSetter($oneOfFields)
                        });
    
                        /**
                         * Creates a new RegularSlice instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.IRegularSlice=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.RegularSlice} RegularSlice instance
                         */
                        RegularSlice.create = function create(properties) {
                            return new RegularSlice(properties);
                        };
    
                        /**
                         * Encodes the specified RegularSlice message. Does not implicitly {@link equestria.protobuf.common.v1.RegularSlice.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.IRegularSlice} message RegularSlice message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        RegularSlice.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.start != null && Object.hasOwnProperty.call(message, "start"))
                                $root.google.protobuf.Timestamp.encode(message.start, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                            if (message.period != null && Object.hasOwnProperty.call(message, "period"))
                                $root.google.protobuf.Duration.encode(message.period, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
                            if (message.raws != null && Object.hasOwnProperty.call(message, "raws"))
                                $root.equestria.protobuf.common.v1.RawValues.encode(message.raws, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
                            if (message.summaries != null && Object.hasOwnProperty.call(message, "summaries"))
                                $root.equestria.protobuf.common.v1.SummaryValues.encode(message.summaries, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
                            return writer;
                        };
    
                        /**
                         * Encodes the specified RegularSlice message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.RegularSlice.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.IRegularSlice} message RegularSlice message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        RegularSlice.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a RegularSlice message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.RegularSlice} RegularSlice
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        RegularSlice.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.RegularSlice();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    message.start = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
                                    break;
                                case 2:
                                    message.period = $root.google.protobuf.Duration.decode(reader, reader.uint32());
                                    break;
                                case 3:
                                    message.raws = $root.equestria.protobuf.common.v1.RawValues.decode(reader, reader.uint32());
                                    break;
                                case 4:
                                    message.summaries = $root.equestria.protobuf.common.v1.SummaryValues.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a RegularSlice message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.RegularSlice} RegularSlice
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        RegularSlice.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a RegularSlice message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        RegularSlice.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            var properties = {};
                            if (message.start != null && message.hasOwnProperty("start")) {
                                var error = $root.google.protobuf.Timestamp.verify(message.start);
                                if (error)
                                    return "start." + error;
                            }
                            if (message.period != null && message.hasOwnProperty("period")) {
                                var error = $root.google.protobuf.Duration.verify(message.period);
                                if (error)
                                    return "period." + error;
                            }
                            if (message.raws != null && message.hasOwnProperty("raws")) {
                                properties.data = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.RawValues.verify(message.raws);
                                    if (error)
                                        return "raws." + error;
                                }
                            }
                            if (message.summaries != null && message.hasOwnProperty("summaries")) {
                                if (properties.data === 1)
                                    return "data: multiple values";
                                properties.data = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.SummaryValues.verify(message.summaries);
                                    if (error)
                                        return "summaries." + error;
                                }
                            }
                            return null;
                        };
    
                        /**
                         * Creates a RegularSlice message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.RegularSlice} RegularSlice
                         */
                        RegularSlice.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.RegularSlice)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.RegularSlice();
                            if (object.start != null) {
                                if (typeof object.start !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.RegularSlice.start: object expected");
                                message.start = $root.google.protobuf.Timestamp.fromObject(object.start);
                            }
                            if (object.period != null) {
                                if (typeof object.period !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.RegularSlice.period: object expected");
                                message.period = $root.google.protobuf.Duration.fromObject(object.period);
                            }
                            if (object.raws != null) {
                                if (typeof object.raws !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.RegularSlice.raws: object expected");
                                message.raws = $root.equestria.protobuf.common.v1.RawValues.fromObject(object.raws);
                            }
                            if (object.summaries != null) {
                                if (typeof object.summaries !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.RegularSlice.summaries: object expected");
                                message.summaries = $root.equestria.protobuf.common.v1.SummaryValues.fromObject(object.summaries);
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a RegularSlice message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.RegularSlice} message RegularSlice
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        RegularSlice.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.defaults) {
                                object.start = null;
                                object.period = null;
                            }
                            if (message.start != null && message.hasOwnProperty("start"))
                                object.start = $root.google.protobuf.Timestamp.toObject(message.start, options);
                            if (message.period != null && message.hasOwnProperty("period"))
                                object.period = $root.google.protobuf.Duration.toObject(message.period, options);
                            if (message.raws != null && message.hasOwnProperty("raws")) {
                                object.raws = $root.equestria.protobuf.common.v1.RawValues.toObject(message.raws, options);
                                if (options.oneofs)
                                    object.data = "raws";
                            }
                            if (message.summaries != null && message.hasOwnProperty("summaries")) {
                                object.summaries = $root.equestria.protobuf.common.v1.SummaryValues.toObject(message.summaries, options);
                                if (options.oneofs)
                                    object.data = "summaries";
                            }
                            return object;
                        };
    
                        /**
                         * Converts this RegularSlice to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.RegularSlice
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        RegularSlice.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return RegularSlice;
                    })();
    
                    v1.IrregularSlice = (function() {
    
                        /**
                         * Properties of an IrregularSlice.
                         * @memberof equestria.protobuf.common.v1
                         * @interface IIrregularSlice
                         * @property {google.protobuf.ITimestamp|null} [start] IrregularSlice start
                         * @property {equestria.protobuf.common.v1.IPeriodsAsDurations|null} [asDurations] IrregularSlice asDurations
                         * @property {equestria.protobuf.common.v1.IPeriodsAsSeconds|null} [asSeconds] IrregularSlice asSeconds
                         * @property {equestria.protobuf.common.v1.IPeriodsAsMilliseconds|null} [asMilliseconds] IrregularSlice asMilliseconds
                         * @property {equestria.protobuf.common.v1.IPeriodsAsMicroseconds|null} [asMicroseconds] IrregularSlice asMicroseconds
                         * @property {equestria.protobuf.common.v1.IRawValues|null} [raws] IrregularSlice raws
                         * @property {equestria.protobuf.common.v1.ISummaryValues|null} [summaries] IrregularSlice summaries
                         */
    
                        /**
                         * Constructs a new IrregularSlice.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents an IrregularSlice.
                         * @implements IIrregularSlice
                         * @constructor
                         * @param {equestria.protobuf.common.v1.IIrregularSlice=} [properties] Properties to set
                         */
                        function IrregularSlice(properties) {
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * IrregularSlice start.
                         * @member {google.protobuf.ITimestamp|null|undefined} start
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        IrregularSlice.prototype.start = null;
    
                        /**
                         * IrregularSlice asDurations.
                         * @member {equestria.protobuf.common.v1.IPeriodsAsDurations|null|undefined} asDurations
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        IrregularSlice.prototype.asDurations = null;
    
                        /**
                         * IrregularSlice asSeconds.
                         * @member {equestria.protobuf.common.v1.IPeriodsAsSeconds|null|undefined} asSeconds
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        IrregularSlice.prototype.asSeconds = null;
    
                        /**
                         * IrregularSlice asMilliseconds.
                         * @member {equestria.protobuf.common.v1.IPeriodsAsMilliseconds|null|undefined} asMilliseconds
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        IrregularSlice.prototype.asMilliseconds = null;
    
                        /**
                         * IrregularSlice asMicroseconds.
                         * @member {equestria.protobuf.common.v1.IPeriodsAsMicroseconds|null|undefined} asMicroseconds
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        IrregularSlice.prototype.asMicroseconds = null;
    
                        /**
                         * IrregularSlice raws.
                         * @member {equestria.protobuf.common.v1.IRawValues|null|undefined} raws
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        IrregularSlice.prototype.raws = null;
    
                        /**
                         * IrregularSlice summaries.
                         * @member {equestria.protobuf.common.v1.ISummaryValues|null|undefined} summaries
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        IrregularSlice.prototype.summaries = null;
    
                        // OneOf field names bound to virtual getters and setters
                        var $oneOfFields;
    
                        /**
                         * IrregularSlice periods.
                         * @member {"asDurations"|"asSeconds"|"asMilliseconds"|"asMicroseconds"|undefined} periods
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        Object.defineProperty(IrregularSlice.prototype, "periods", {
                            get: $util.oneOfGetter($oneOfFields = ["asDurations", "asSeconds", "asMilliseconds", "asMicroseconds"]),
                            set: $util.oneOfSetter($oneOfFields)
                        });
    
                        /**
                         * IrregularSlice data.
                         * @member {"raws"|"summaries"|undefined} data
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         */
                        Object.defineProperty(IrregularSlice.prototype, "data", {
                            get: $util.oneOfGetter($oneOfFields = ["raws", "summaries"]),
                            set: $util.oneOfSetter($oneOfFields)
                        });
    
                        /**
                         * Creates a new IrregularSlice instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.IIrregularSlice=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.IrregularSlice} IrregularSlice instance
                         */
                        IrregularSlice.create = function create(properties) {
                            return new IrregularSlice(properties);
                        };
    
                        /**
                         * Encodes the specified IrregularSlice message. Does not implicitly {@link equestria.protobuf.common.v1.IrregularSlice.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.IIrregularSlice} message IrregularSlice message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        IrregularSlice.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.start != null && Object.hasOwnProperty.call(message, "start"))
                                $root.google.protobuf.Timestamp.encode(message.start, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                            if (message.asDurations != null && Object.hasOwnProperty.call(message, "asDurations"))
                                $root.equestria.protobuf.common.v1.PeriodsAsDurations.encode(message.asDurations, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
                            if (message.asSeconds != null && Object.hasOwnProperty.call(message, "asSeconds"))
                                $root.equestria.protobuf.common.v1.PeriodsAsSeconds.encode(message.asSeconds, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
                            if (message.asMilliseconds != null && Object.hasOwnProperty.call(message, "asMilliseconds"))
                                $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds.encode(message.asMilliseconds, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
                            if (message.asMicroseconds != null && Object.hasOwnProperty.call(message, "asMicroseconds"))
                                $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds.encode(message.asMicroseconds, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
                            if (message.raws != null && Object.hasOwnProperty.call(message, "raws"))
                                $root.equestria.protobuf.common.v1.RawValues.encode(message.raws, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
                            if (message.summaries != null && Object.hasOwnProperty.call(message, "summaries"))
                                $root.equestria.protobuf.common.v1.SummaryValues.encode(message.summaries, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
                            return writer;
                        };
    
                        /**
                         * Encodes the specified IrregularSlice message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.IrregularSlice.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.IIrregularSlice} message IrregularSlice message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        IrregularSlice.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes an IrregularSlice message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.IrregularSlice} IrregularSlice
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        IrregularSlice.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.IrregularSlice();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    message.start = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
                                    break;
                                case 2:
                                    message.asDurations = $root.equestria.protobuf.common.v1.PeriodsAsDurations.decode(reader, reader.uint32());
                                    break;
                                case 3:
                                    message.asSeconds = $root.equestria.protobuf.common.v1.PeriodsAsSeconds.decode(reader, reader.uint32());
                                    break;
                                case 4:
                                    message.asMilliseconds = $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds.decode(reader, reader.uint32());
                                    break;
                                case 5:
                                    message.asMicroseconds = $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds.decode(reader, reader.uint32());
                                    break;
                                case 6:
                                    message.raws = $root.equestria.protobuf.common.v1.RawValues.decode(reader, reader.uint32());
                                    break;
                                case 7:
                                    message.summaries = $root.equestria.protobuf.common.v1.SummaryValues.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes an IrregularSlice message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.IrregularSlice} IrregularSlice
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        IrregularSlice.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies an IrregularSlice message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        IrregularSlice.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            var properties = {};
                            if (message.start != null && message.hasOwnProperty("start")) {
                                var error = $root.google.protobuf.Timestamp.verify(message.start);
                                if (error)
                                    return "start." + error;
                            }
                            if (message.asDurations != null && message.hasOwnProperty("asDurations")) {
                                properties.periods = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.PeriodsAsDurations.verify(message.asDurations);
                                    if (error)
                                        return "asDurations." + error;
                                }
                            }
                            if (message.asSeconds != null && message.hasOwnProperty("asSeconds")) {
                                if (properties.periods === 1)
                                    return "periods: multiple values";
                                properties.periods = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.PeriodsAsSeconds.verify(message.asSeconds);
                                    if (error)
                                        return "asSeconds." + error;
                                }
                            }
                            if (message.asMilliseconds != null && message.hasOwnProperty("asMilliseconds")) {
                                if (properties.periods === 1)
                                    return "periods: multiple values";
                                properties.periods = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds.verify(message.asMilliseconds);
                                    if (error)
                                        return "asMilliseconds." + error;
                                }
                            }
                            if (message.asMicroseconds != null && message.hasOwnProperty("asMicroseconds")) {
                                if (properties.periods === 1)
                                    return "periods: multiple values";
                                properties.periods = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds.verify(message.asMicroseconds);
                                    if (error)
                                        return "asMicroseconds." + error;
                                }
                            }
                            if (message.raws != null && message.hasOwnProperty("raws")) {
                                properties.data = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.RawValues.verify(message.raws);
                                    if (error)
                                        return "raws." + error;
                                }
                            }
                            if (message.summaries != null && message.hasOwnProperty("summaries")) {
                                if (properties.data === 1)
                                    return "data: multiple values";
                                properties.data = 1;
                                {
                                    var error = $root.equestria.protobuf.common.v1.SummaryValues.verify(message.summaries);
                                    if (error)
                                        return "summaries." + error;
                                }
                            }
                            return null;
                        };
    
                        /**
                         * Creates an IrregularSlice message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.IrregularSlice} IrregularSlice
                         */
                        IrregularSlice.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.IrregularSlice)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.IrregularSlice();
                            if (object.start != null) {
                                if (typeof object.start !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.IrregularSlice.start: object expected");
                                message.start = $root.google.protobuf.Timestamp.fromObject(object.start);
                            }
                            if (object.asDurations != null) {
                                if (typeof object.asDurations !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.IrregularSlice.asDurations: object expected");
                                message.asDurations = $root.equestria.protobuf.common.v1.PeriodsAsDurations.fromObject(object.asDurations);
                            }
                            if (object.asSeconds != null) {
                                if (typeof object.asSeconds !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.IrregularSlice.asSeconds: object expected");
                                message.asSeconds = $root.equestria.protobuf.common.v1.PeriodsAsSeconds.fromObject(object.asSeconds);
                            }
                            if (object.asMilliseconds != null) {
                                if (typeof object.asMilliseconds !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.IrregularSlice.asMilliseconds: object expected");
                                message.asMilliseconds = $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds.fromObject(object.asMilliseconds);
                            }
                            if (object.asMicroseconds != null) {
                                if (typeof object.asMicroseconds !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.IrregularSlice.asMicroseconds: object expected");
                                message.asMicroseconds = $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds.fromObject(object.asMicroseconds);
                            }
                            if (object.raws != null) {
                                if (typeof object.raws !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.IrregularSlice.raws: object expected");
                                message.raws = $root.equestria.protobuf.common.v1.RawValues.fromObject(object.raws);
                            }
                            if (object.summaries != null) {
                                if (typeof object.summaries !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.IrregularSlice.summaries: object expected");
                                message.summaries = $root.equestria.protobuf.common.v1.SummaryValues.fromObject(object.summaries);
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from an IrregularSlice message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @static
                         * @param {equestria.protobuf.common.v1.IrregularSlice} message IrregularSlice
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        IrregularSlice.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.defaults)
                                object.start = null;
                            if (message.start != null && message.hasOwnProperty("start"))
                                object.start = $root.google.protobuf.Timestamp.toObject(message.start, options);
                            if (message.asDurations != null && message.hasOwnProperty("asDurations")) {
                                object.asDurations = $root.equestria.protobuf.common.v1.PeriodsAsDurations.toObject(message.asDurations, options);
                                if (options.oneofs)
                                    object.periods = "asDurations";
                            }
                            if (message.asSeconds != null && message.hasOwnProperty("asSeconds")) {
                                object.asSeconds = $root.equestria.protobuf.common.v1.PeriodsAsSeconds.toObject(message.asSeconds, options);
                                if (options.oneofs)
                                    object.periods = "asSeconds";
                            }
                            if (message.asMilliseconds != null && message.hasOwnProperty("asMilliseconds")) {
                                object.asMilliseconds = $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds.toObject(message.asMilliseconds, options);
                                if (options.oneofs)
                                    object.periods = "asMilliseconds";
                            }
                            if (message.asMicroseconds != null && message.hasOwnProperty("asMicroseconds")) {
                                object.asMicroseconds = $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds.toObject(message.asMicroseconds, options);
                                if (options.oneofs)
                                    object.periods = "asMicroseconds";
                            }
                            if (message.raws != null && message.hasOwnProperty("raws")) {
                                object.raws = $root.equestria.protobuf.common.v1.RawValues.toObject(message.raws, options);
                                if (options.oneofs)
                                    object.data = "raws";
                            }
                            if (message.summaries != null && message.hasOwnProperty("summaries")) {
                                object.summaries = $root.equestria.protobuf.common.v1.SummaryValues.toObject(message.summaries, options);
                                if (options.oneofs)
                                    object.data = "summaries";
                            }
                            return object;
                        };
    
                        /**
                         * Converts this IrregularSlice to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.IrregularSlice
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        IrregularSlice.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return IrregularSlice;
                    })();
    
                    v1.PeriodsAsDurations = (function() {
    
                        /**
                         * Properties of a PeriodsAsDurations.
                         * @memberof equestria.protobuf.common.v1
                         * @interface IPeriodsAsDurations
                         * @property {Array.<google.protobuf.IDuration>|null} [durations] PeriodsAsDurations durations
                         */
    
                        /**
                         * Constructs a new PeriodsAsDurations.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a PeriodsAsDurations.
                         * @implements IPeriodsAsDurations
                         * @constructor
                         * @param {equestria.protobuf.common.v1.IPeriodsAsDurations=} [properties] Properties to set
                         */
                        function PeriodsAsDurations(properties) {
                            this.durations = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * PeriodsAsDurations durations.
                         * @member {Array.<google.protobuf.IDuration>} durations
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @instance
                         */
                        PeriodsAsDurations.prototype.durations = $util.emptyArray;
    
                        /**
                         * Creates a new PeriodsAsDurations instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsDurations=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.PeriodsAsDurations} PeriodsAsDurations instance
                         */
                        PeriodsAsDurations.create = function create(properties) {
                            return new PeriodsAsDurations(properties);
                        };
    
                        /**
                         * Encodes the specified PeriodsAsDurations message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsDurations.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsDurations} message PeriodsAsDurations message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsDurations.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.durations != null && message.durations.length)
                                for (var i = 0; i < message.durations.length; ++i)
                                    $root.google.protobuf.Duration.encode(message.durations[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                            return writer;
                        };
    
                        /**
                         * Encodes the specified PeriodsAsDurations message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsDurations.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsDurations} message PeriodsAsDurations message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsDurations.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a PeriodsAsDurations message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.PeriodsAsDurations} PeriodsAsDurations
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsDurations.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.PeriodsAsDurations();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    if (!(message.durations && message.durations.length))
                                        message.durations = [];
                                    message.durations.push($root.google.protobuf.Duration.decode(reader, reader.uint32()));
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a PeriodsAsDurations message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.PeriodsAsDurations} PeriodsAsDurations
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsDurations.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a PeriodsAsDurations message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        PeriodsAsDurations.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.durations != null && message.hasOwnProperty("durations")) {
                                if (!Array.isArray(message.durations))
                                    return "durations: array expected";
                                for (var i = 0; i < message.durations.length; ++i) {
                                    var error = $root.google.protobuf.Duration.verify(message.durations[i]);
                                    if (error)
                                        return "durations." + error;
                                }
                            }
                            return null;
                        };
    
                        /**
                         * Creates a PeriodsAsDurations message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.PeriodsAsDurations} PeriodsAsDurations
                         */
                        PeriodsAsDurations.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.PeriodsAsDurations)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.PeriodsAsDurations();
                            if (object.durations) {
                                if (!Array.isArray(object.durations))
                                    throw TypeError(".equestria.protobuf.common.v1.PeriodsAsDurations.durations: array expected");
                                message.durations = [];
                                for (var i = 0; i < object.durations.length; ++i) {
                                    if (typeof object.durations[i] !== "object")
                                        throw TypeError(".equestria.protobuf.common.v1.PeriodsAsDurations.durations: object expected");
                                    message.durations[i] = $root.google.protobuf.Duration.fromObject(object.durations[i]);
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a PeriodsAsDurations message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @static
                         * @param {equestria.protobuf.common.v1.PeriodsAsDurations} message PeriodsAsDurations
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        PeriodsAsDurations.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults)
                                object.durations = [];
                            if (message.durations && message.durations.length) {
                                object.durations = [];
                                for (var j = 0; j < message.durations.length; ++j)
                                    object.durations[j] = $root.google.protobuf.Duration.toObject(message.durations[j], options);
                            }
                            return object;
                        };
    
                        /**
                         * Converts this PeriodsAsDurations to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.PeriodsAsDurations
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        PeriodsAsDurations.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return PeriodsAsDurations;
                    })();
    
                    v1.PeriodsAsSeconds = (function() {
    
                        /**
                         * Properties of a PeriodsAsSeconds.
                         * @memberof equestria.protobuf.common.v1
                         * @interface IPeriodsAsSeconds
                         * @property {Array.<number>|null} [seconds] PeriodsAsSeconds seconds
                         */
    
                        /**
                         * Constructs a new PeriodsAsSeconds.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a PeriodsAsSeconds.
                         * @implements IPeriodsAsSeconds
                         * @constructor
                         * @param {equestria.protobuf.common.v1.IPeriodsAsSeconds=} [properties] Properties to set
                         */
                        function PeriodsAsSeconds(properties) {
                            this.seconds = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * PeriodsAsSeconds seconds.
                         * @member {Array.<number>} seconds
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @instance
                         */
                        PeriodsAsSeconds.prototype.seconds = $util.emptyArray;
    
                        /**
                         * Creates a new PeriodsAsSeconds instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsSeconds=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.PeriodsAsSeconds} PeriodsAsSeconds instance
                         */
                        PeriodsAsSeconds.create = function create(properties) {
                            return new PeriodsAsSeconds(properties);
                        };
    
                        /**
                         * Encodes the specified PeriodsAsSeconds message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsSeconds.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsSeconds} message PeriodsAsSeconds message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsSeconds.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.seconds != null && message.seconds.length) {
                                writer.uint32(/* id 1, wireType 2 =*/10).fork();
                                for (var i = 0; i < message.seconds.length; ++i)
                                    writer.int32(message.seconds[i]);
                                writer.ldelim();
                            }
                            return writer;
                        };
    
                        /**
                         * Encodes the specified PeriodsAsSeconds message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsSeconds.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsSeconds} message PeriodsAsSeconds message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsSeconds.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a PeriodsAsSeconds message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.PeriodsAsSeconds} PeriodsAsSeconds
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsSeconds.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.PeriodsAsSeconds();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    if (!(message.seconds && message.seconds.length))
                                        message.seconds = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.seconds.push(reader.int32());
                                    } else
                                        message.seconds.push(reader.int32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a PeriodsAsSeconds message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.PeriodsAsSeconds} PeriodsAsSeconds
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsSeconds.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a PeriodsAsSeconds message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        PeriodsAsSeconds.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.seconds != null && message.hasOwnProperty("seconds")) {
                                if (!Array.isArray(message.seconds))
                                    return "seconds: array expected";
                                for (var i = 0; i < message.seconds.length; ++i)
                                    if (!$util.isInteger(message.seconds[i]))
                                        return "seconds: integer[] expected";
                            }
                            return null;
                        };
    
                        /**
                         * Creates a PeriodsAsSeconds message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.PeriodsAsSeconds} PeriodsAsSeconds
                         */
                        PeriodsAsSeconds.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.PeriodsAsSeconds)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.PeriodsAsSeconds();
                            if (object.seconds) {
                                if (!Array.isArray(object.seconds))
                                    throw TypeError(".equestria.protobuf.common.v1.PeriodsAsSeconds.seconds: array expected");
                                message.seconds = [];
                                for (var i = 0; i < object.seconds.length; ++i)
                                    message.seconds[i] = object.seconds[i] | 0;
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a PeriodsAsSeconds message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @static
                         * @param {equestria.protobuf.common.v1.PeriodsAsSeconds} message PeriodsAsSeconds
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        PeriodsAsSeconds.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults)
                                object.seconds = [];
                            if (message.seconds && message.seconds.length) {
                                object.seconds = [];
                                for (var j = 0; j < message.seconds.length; ++j)
                                    object.seconds[j] = message.seconds[j];
                            }
                            return object;
                        };
    
                        /**
                         * Converts this PeriodsAsSeconds to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.PeriodsAsSeconds
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        PeriodsAsSeconds.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return PeriodsAsSeconds;
                    })();
    
                    v1.PeriodsAsMilliseconds = (function() {
    
                        /**
                         * Properties of a PeriodsAsMilliseconds.
                         * @memberof equestria.protobuf.common.v1
                         * @interface IPeriodsAsMilliseconds
                         * @property {Array.<number>|null} [milliseconds] PeriodsAsMilliseconds milliseconds
                         */
    
                        /**
                         * Constructs a new PeriodsAsMilliseconds.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a PeriodsAsMilliseconds.
                         * @implements IPeriodsAsMilliseconds
                         * @constructor
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMilliseconds=} [properties] Properties to set
                         */
                        function PeriodsAsMilliseconds(properties) {
                            this.milliseconds = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * PeriodsAsMilliseconds milliseconds.
                         * @member {Array.<number>} milliseconds
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @instance
                         */
                        PeriodsAsMilliseconds.prototype.milliseconds = $util.emptyArray;
    
                        /**
                         * Creates a new PeriodsAsMilliseconds instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMilliseconds=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMilliseconds} PeriodsAsMilliseconds instance
                         */
                        PeriodsAsMilliseconds.create = function create(properties) {
                            return new PeriodsAsMilliseconds(properties);
                        };
    
                        /**
                         * Encodes the specified PeriodsAsMilliseconds message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMilliseconds.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMilliseconds} message PeriodsAsMilliseconds message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsMilliseconds.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.milliseconds != null && message.milliseconds.length) {
                                writer.uint32(/* id 1, wireType 2 =*/10).fork();
                                for (var i = 0; i < message.milliseconds.length; ++i)
                                    writer.int32(message.milliseconds[i]);
                                writer.ldelim();
                            }
                            return writer;
                        };
    
                        /**
                         * Encodes the specified PeriodsAsMilliseconds message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMilliseconds.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMilliseconds} message PeriodsAsMilliseconds message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsMilliseconds.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a PeriodsAsMilliseconds message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMilliseconds} PeriodsAsMilliseconds
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsMilliseconds.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    if (!(message.milliseconds && message.milliseconds.length))
                                        message.milliseconds = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.milliseconds.push(reader.int32());
                                    } else
                                        message.milliseconds.push(reader.int32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a PeriodsAsMilliseconds message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMilliseconds} PeriodsAsMilliseconds
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsMilliseconds.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a PeriodsAsMilliseconds message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        PeriodsAsMilliseconds.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.milliseconds != null && message.hasOwnProperty("milliseconds")) {
                                if (!Array.isArray(message.milliseconds))
                                    return "milliseconds: array expected";
                                for (var i = 0; i < message.milliseconds.length; ++i)
                                    if (!$util.isInteger(message.milliseconds[i]))
                                        return "milliseconds: integer[] expected";
                            }
                            return null;
                        };
    
                        /**
                         * Creates a PeriodsAsMilliseconds message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMilliseconds} PeriodsAsMilliseconds
                         */
                        PeriodsAsMilliseconds.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.PeriodsAsMilliseconds();
                            if (object.milliseconds) {
                                if (!Array.isArray(object.milliseconds))
                                    throw TypeError(".equestria.protobuf.common.v1.PeriodsAsMilliseconds.milliseconds: array expected");
                                message.milliseconds = [];
                                for (var i = 0; i < object.milliseconds.length; ++i)
                                    message.milliseconds[i] = object.milliseconds[i] | 0;
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a PeriodsAsMilliseconds message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.PeriodsAsMilliseconds} message PeriodsAsMilliseconds
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        PeriodsAsMilliseconds.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults)
                                object.milliseconds = [];
                            if (message.milliseconds && message.milliseconds.length) {
                                object.milliseconds = [];
                                for (var j = 0; j < message.milliseconds.length; ++j)
                                    object.milliseconds[j] = message.milliseconds[j];
                            }
                            return object;
                        };
    
                        /**
                         * Converts this PeriodsAsMilliseconds to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMilliseconds
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        PeriodsAsMilliseconds.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return PeriodsAsMilliseconds;
                    })();
    
                    v1.PeriodsAsMicroseconds = (function() {
    
                        /**
                         * Properties of a PeriodsAsMicroseconds.
                         * @memberof equestria.protobuf.common.v1
                         * @interface IPeriodsAsMicroseconds
                         * @property {Array.<number>|null} [microseconds] PeriodsAsMicroseconds microseconds
                         */
    
                        /**
                         * Constructs a new PeriodsAsMicroseconds.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a PeriodsAsMicroseconds.
                         * @implements IPeriodsAsMicroseconds
                         * @constructor
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMicroseconds=} [properties] Properties to set
                         */
                        function PeriodsAsMicroseconds(properties) {
                            this.microseconds = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * PeriodsAsMicroseconds microseconds.
                         * @member {Array.<number>} microseconds
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @instance
                         */
                        PeriodsAsMicroseconds.prototype.microseconds = $util.emptyArray;
    
                        /**
                         * Creates a new PeriodsAsMicroseconds instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMicroseconds=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMicroseconds} PeriodsAsMicroseconds instance
                         */
                        PeriodsAsMicroseconds.create = function create(properties) {
                            return new PeriodsAsMicroseconds(properties);
                        };
    
                        /**
                         * Encodes the specified PeriodsAsMicroseconds message. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMicroseconds.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMicroseconds} message PeriodsAsMicroseconds message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsMicroseconds.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.microseconds != null && message.microseconds.length) {
                                writer.uint32(/* id 1, wireType 2 =*/10).fork();
                                for (var i = 0; i < message.microseconds.length; ++i)
                                    writer.int32(message.microseconds[i]);
                                writer.ldelim();
                            }
                            return writer;
                        };
    
                        /**
                         * Encodes the specified PeriodsAsMicroseconds message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.PeriodsAsMicroseconds.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.IPeriodsAsMicroseconds} message PeriodsAsMicroseconds message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        PeriodsAsMicroseconds.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a PeriodsAsMicroseconds message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMicroseconds} PeriodsAsMicroseconds
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsMicroseconds.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    if (!(message.microseconds && message.microseconds.length))
                                        message.microseconds = [];
                                    if ((tag & 7) === 2) {
                                        var end2 = reader.uint32() + reader.pos;
                                        while (reader.pos < end2)
                                            message.microseconds.push(reader.int32());
                                    } else
                                        message.microseconds.push(reader.int32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a PeriodsAsMicroseconds message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMicroseconds} PeriodsAsMicroseconds
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        PeriodsAsMicroseconds.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a PeriodsAsMicroseconds message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        PeriodsAsMicroseconds.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.microseconds != null && message.hasOwnProperty("microseconds")) {
                                if (!Array.isArray(message.microseconds))
                                    return "microseconds: array expected";
                                for (var i = 0; i < message.microseconds.length; ++i)
                                    if (!$util.isInteger(message.microseconds[i]))
                                        return "microseconds: integer[] expected";
                            }
                            return null;
                        };
    
                        /**
                         * Creates a PeriodsAsMicroseconds message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.PeriodsAsMicroseconds} PeriodsAsMicroseconds
                         */
                        PeriodsAsMicroseconds.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.PeriodsAsMicroseconds();
                            if (object.microseconds) {
                                if (!Array.isArray(object.microseconds))
                                    throw TypeError(".equestria.protobuf.common.v1.PeriodsAsMicroseconds.microseconds: array expected");
                                message.microseconds = [];
                                for (var i = 0; i < object.microseconds.length; ++i)
                                    message.microseconds[i] = object.microseconds[i] | 0;
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a PeriodsAsMicroseconds message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @static
                         * @param {equestria.protobuf.common.v1.PeriodsAsMicroseconds} message PeriodsAsMicroseconds
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        PeriodsAsMicroseconds.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults)
                                object.microseconds = [];
                            if (message.microseconds && message.microseconds.length) {
                                object.microseconds = [];
                                for (var j = 0; j < message.microseconds.length; ++j)
                                    object.microseconds[j] = message.microseconds[j];
                            }
                            return object;
                        };
    
                        /**
                         * Converts this PeriodsAsMicroseconds to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.PeriodsAsMicroseconds
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        PeriodsAsMicroseconds.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return PeriodsAsMicroseconds;
                    })();
    
                    v1.TimeSeries = (function() {
    
                        /**
                         * Properties of a TimeSeries.
                         * @memberof equestria.protobuf.common.v1
                         * @interface ITimeSeries
                         * @property {Array.<equestria.protobuf.common.v1.ISlice>|null} [slices] TimeSeries slices
                         * @property {equestria.protobuf.entity.v1.IEntityRef|null} [measuringUnit] TimeSeries measuringUnit
                         */
    
                        /**
                         * Constructs a new TimeSeries.
                         * @memberof equestria.protobuf.common.v1
                         * @classdesc Represents a TimeSeries.
                         * @implements ITimeSeries
                         * @constructor
                         * @param {equestria.protobuf.common.v1.ITimeSeries=} [properties] Properties to set
                         */
                        function TimeSeries(properties) {
                            this.slices = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * TimeSeries slices.
                         * @member {Array.<equestria.protobuf.common.v1.ISlice>} slices
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @instance
                         */
                        TimeSeries.prototype.slices = $util.emptyArray;
    
                        /**
                         * TimeSeries measuringUnit.
                         * @member {equestria.protobuf.entity.v1.IEntityRef|null|undefined} measuringUnit
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @instance
                         */
                        TimeSeries.prototype.measuringUnit = null;
    
                        /**
                         * Creates a new TimeSeries instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {equestria.protobuf.common.v1.ITimeSeries=} [properties] Properties to set
                         * @returns {equestria.protobuf.common.v1.TimeSeries} TimeSeries instance
                         */
                        TimeSeries.create = function create(properties) {
                            return new TimeSeries(properties);
                        };
    
                        /**
                         * Encodes the specified TimeSeries message. Does not implicitly {@link equestria.protobuf.common.v1.TimeSeries.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {equestria.protobuf.common.v1.ITimeSeries} message TimeSeries message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        TimeSeries.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.slices != null && message.slices.length)
                                for (var i = 0; i < message.slices.length; ++i)
                                    $root.equestria.protobuf.common.v1.Slice.encode(message.slices[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                            if (message.measuringUnit != null && Object.hasOwnProperty.call(message, "measuringUnit"))
                                $root.equestria.protobuf.entity.v1.EntityRef.encode(message.measuringUnit, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
                            return writer;
                        };
    
                        /**
                         * Encodes the specified TimeSeries message, length delimited. Does not implicitly {@link equestria.protobuf.common.v1.TimeSeries.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {equestria.protobuf.common.v1.ITimeSeries} message TimeSeries message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        TimeSeries.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a TimeSeries message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.common.v1.TimeSeries} TimeSeries
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        TimeSeries.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.common.v1.TimeSeries();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    if (!(message.slices && message.slices.length))
                                        message.slices = [];
                                    message.slices.push($root.equestria.protobuf.common.v1.Slice.decode(reader, reader.uint32()));
                                    break;
                                case 2:
                                    message.measuringUnit = $root.equestria.protobuf.entity.v1.EntityRef.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a TimeSeries message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.common.v1.TimeSeries} TimeSeries
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        TimeSeries.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a TimeSeries message.
                         * @function verify
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        TimeSeries.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.slices != null && message.hasOwnProperty("slices")) {
                                if (!Array.isArray(message.slices))
                                    return "slices: array expected";
                                for (var i = 0; i < message.slices.length; ++i) {
                                    var error = $root.equestria.protobuf.common.v1.Slice.verify(message.slices[i]);
                                    if (error)
                                        return "slices." + error;
                                }
                            }
                            if (message.measuringUnit != null && message.hasOwnProperty("measuringUnit")) {
                                var error = $root.equestria.protobuf.entity.v1.EntityRef.verify(message.measuringUnit);
                                if (error)
                                    return "measuringUnit." + error;
                            }
                            return null;
                        };
    
                        /**
                         * Creates a TimeSeries message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.common.v1.TimeSeries} TimeSeries
                         */
                        TimeSeries.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.common.v1.TimeSeries)
                                return object;
                            var message = new $root.equestria.protobuf.common.v1.TimeSeries();
                            if (object.slices) {
                                if (!Array.isArray(object.slices))
                                    throw TypeError(".equestria.protobuf.common.v1.TimeSeries.slices: array expected");
                                message.slices = [];
                                for (var i = 0; i < object.slices.length; ++i) {
                                    if (typeof object.slices[i] !== "object")
                                        throw TypeError(".equestria.protobuf.common.v1.TimeSeries.slices: object expected");
                                    message.slices[i] = $root.equestria.protobuf.common.v1.Slice.fromObject(object.slices[i]);
                                }
                            }
                            if (object.measuringUnit != null) {
                                if (typeof object.measuringUnit !== "object")
                                    throw TypeError(".equestria.protobuf.common.v1.TimeSeries.measuringUnit: object expected");
                                message.measuringUnit = $root.equestria.protobuf.entity.v1.EntityRef.fromObject(object.measuringUnit);
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a TimeSeries message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @static
                         * @param {equestria.protobuf.common.v1.TimeSeries} message TimeSeries
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        TimeSeries.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults)
                                object.slices = [];
                            if (options.defaults)
                                object.measuringUnit = null;
                            if (message.slices && message.slices.length) {
                                object.slices = [];
                                for (var j = 0; j < message.slices.length; ++j)
                                    object.slices[j] = $root.equestria.protobuf.common.v1.Slice.toObject(message.slices[j], options);
                            }
                            if (message.measuringUnit != null && message.hasOwnProperty("measuringUnit"))
                                object.measuringUnit = $root.equestria.protobuf.entity.v1.EntityRef.toObject(message.measuringUnit, options);
                            return object;
                        };
    
                        /**
                         * Converts this TimeSeries to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.common.v1.TimeSeries
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        TimeSeries.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return TimeSeries;
                    })();
    
                    return v1;
                })();
    
                return common;
            })();
    
            protobuf.entity = (function() {
    
                /**
                 * Namespace entity.
                 * @memberof equestria.protobuf
                 * @namespace
                 */
                var entity = {};
    
                entity.v1 = (function() {
    
                    /**
                     * Namespace v1.
                     * @memberof equestria.protobuf.entity
                     * @namespace
                     */
                    var v1 = {};
    
                    /**
                     * EntityType enum.
                     * @name equestria.protobuf.entity.v1.EntityType
                     * @enum {number}
                     * @property {number} USERS=0 USERS value
                     * @property {number} COUNTRIES=1 COUNTRIES value
                     * @property {number} MEASUREMENT_TYPES=2 MEASUREMENT_TYPES value
                     * @property {number} MEASURING_UNITS=3 MEASURING_UNITS value
                     * @property {number} DEVICE_TYPES=4 DEVICE_TYPES value
                     * @property {number} TENANTS=5 TENANTS value
                     * @property {number} DEVICE_MODELS=6 DEVICE_MODELS value
                     * @property {number} DEVICES=7 DEVICES value
                     * @property {number} SIGNALS=8 SIGNALS value
                     * @property {number} ASSET_TYPES=9 ASSET_TYPES value
                     * @property {number} ASSETS=10 ASSETS value
                     * @property {number} HARDPOINTS=11 HARDPOINTS value
                     * @property {number} SOFTPOINT_TYPES=12 SOFTPOINT_TYPES value
                     * @property {number} SOFTPOINTS=13 SOFTPOINTS value
                     * @property {number} SENSORS=14 SENSORS value
                     * @property {number} SENSOR_MODELS=15 SENSOR_MODELS value
                     * @property {number} SENSOR_CLASSES=16 SENSOR_CLASSES value
                     * @property {number} DEVICE_PROFILES=17 DEVICE_PROFILES value
                     * @property {number} DEVICE_PROFILE_TYPES=18 DEVICE_PROFILE_TYPES value
                     */
                    v1.EntityType = (function() {
                        var valuesById = {}, values = Object.create(valuesById);
                        values[valuesById[0] = "USERS"] = 0;
                        values[valuesById[1] = "COUNTRIES"] = 1;
                        values[valuesById[2] = "MEASUREMENT_TYPES"] = 2;
                        values[valuesById[3] = "MEASURING_UNITS"] = 3;
                        values[valuesById[4] = "DEVICE_TYPES"] = 4;
                        values[valuesById[5] = "TENANTS"] = 5;
                        values[valuesById[6] = "DEVICE_MODELS"] = 6;
                        values[valuesById[7] = "DEVICES"] = 7;
                        values[valuesById[8] = "SIGNALS"] = 8;
                        values[valuesById[9] = "ASSET_TYPES"] = 9;
                        values[valuesById[10] = "ASSETS"] = 10;
                        values[valuesById[11] = "HARDPOINTS"] = 11;
                        values[valuesById[12] = "SOFTPOINT_TYPES"] = 12;
                        values[valuesById[13] = "SOFTPOINTS"] = 13;
                        values[valuesById[14] = "SENSORS"] = 14;
                        values[valuesById[15] = "SENSOR_MODELS"] = 15;
                        values[valuesById[16] = "SENSOR_CLASSES"] = 16;
                        values[valuesById[17] = "DEVICE_PROFILES"] = 17;
                        values[valuesById[18] = "DEVICE_PROFILE_TYPES"] = 18;
                        return values;
                    })();
    
                    v1.EntityTypeValue = (function() {
    
                        /**
                         * Properties of an EntityTypeValue.
                         * @memberof equestria.protobuf.entity.v1
                         * @interface IEntityTypeValue
                         * @property {equestria.protobuf.entity.v1.EntityType|null} [value] EntityTypeValue value
                         */
    
                        /**
                         * Constructs a new EntityTypeValue.
                         * @memberof equestria.protobuf.entity.v1
                         * @classdesc Represents an EntityTypeValue.
                         * @implements IEntityTypeValue
                         * @constructor
                         * @param {equestria.protobuf.entity.v1.IEntityTypeValue=} [properties] Properties to set
                         */
                        function EntityTypeValue(properties) {
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * EntityTypeValue value.
                         * @member {equestria.protobuf.entity.v1.EntityType} value
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @instance
                         */
                        EntityTypeValue.prototype.value = 0;
    
                        /**
                         * Creates a new EntityTypeValue instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {equestria.protobuf.entity.v1.IEntityTypeValue=} [properties] Properties to set
                         * @returns {equestria.protobuf.entity.v1.EntityTypeValue} EntityTypeValue instance
                         */
                        EntityTypeValue.create = function create(properties) {
                            return new EntityTypeValue(properties);
                        };
    
                        /**
                         * Encodes the specified EntityTypeValue message. Does not implicitly {@link equestria.protobuf.entity.v1.EntityTypeValue.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {equestria.protobuf.entity.v1.IEntityTypeValue} message EntityTypeValue message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        EntityTypeValue.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.value);
                            return writer;
                        };
    
                        /**
                         * Encodes the specified EntityTypeValue message, length delimited. Does not implicitly {@link equestria.protobuf.entity.v1.EntityTypeValue.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {equestria.protobuf.entity.v1.IEntityTypeValue} message EntityTypeValue message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        EntityTypeValue.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes an EntityTypeValue message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.entity.v1.EntityTypeValue} EntityTypeValue
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        EntityTypeValue.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.entity.v1.EntityTypeValue();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    message.value = reader.int32();
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes an EntityTypeValue message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.entity.v1.EntityTypeValue} EntityTypeValue
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        EntityTypeValue.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies an EntityTypeValue message.
                         * @function verify
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        EntityTypeValue.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.value != null && message.hasOwnProperty("value"))
                                switch (message.value) {
                                default:
                                    return "value: enum value expected";
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                    break;
                                }
                            return null;
                        };
    
                        /**
                         * Creates an EntityTypeValue message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.entity.v1.EntityTypeValue} EntityTypeValue
                         */
                        EntityTypeValue.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.entity.v1.EntityTypeValue)
                                return object;
                            var message = new $root.equestria.protobuf.entity.v1.EntityTypeValue();
                            switch (object.value) {
                            case "USERS":
                            case 0:
                                message.value = 0;
                                break;
                            case "COUNTRIES":
                            case 1:
                                message.value = 1;
                                break;
                            case "MEASUREMENT_TYPES":
                            case 2:
                                message.value = 2;
                                break;
                            case "MEASURING_UNITS":
                            case 3:
                                message.value = 3;
                                break;
                            case "DEVICE_TYPES":
                            case 4:
                                message.value = 4;
                                break;
                            case "TENANTS":
                            case 5:
                                message.value = 5;
                                break;
                            case "DEVICE_MODELS":
                            case 6:
                                message.value = 6;
                                break;
                            case "DEVICES":
                            case 7:
                                message.value = 7;
                                break;
                            case "SIGNALS":
                            case 8:
                                message.value = 8;
                                break;
                            case "ASSET_TYPES":
                            case 9:
                                message.value = 9;
                                break;
                            case "ASSETS":
                            case 10:
                                message.value = 10;
                                break;
                            case "HARDPOINTS":
                            case 11:
                                message.value = 11;
                                break;
                            case "SOFTPOINT_TYPES":
                            case 12:
                                message.value = 12;
                                break;
                            case "SOFTPOINTS":
                            case 13:
                                message.value = 13;
                                break;
                            case "SENSORS":
                            case 14:
                                message.value = 14;
                                break;
                            case "SENSOR_MODELS":
                            case 15:
                                message.value = 15;
                                break;
                            case "SENSOR_CLASSES":
                            case 16:
                                message.value = 16;
                                break;
                            case "DEVICE_PROFILES":
                            case 17:
                                message.value = 17;
                                break;
                            case "DEVICE_PROFILE_TYPES":
                            case 18:
                                message.value = 18;
                                break;
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from an EntityTypeValue message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @static
                         * @param {equestria.protobuf.entity.v1.EntityTypeValue} message EntityTypeValue
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        EntityTypeValue.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.defaults)
                                object.value = options.enums === String ? "USERS" : 0;
                            if (message.value != null && message.hasOwnProperty("value"))
                                object.value = options.enums === String ? $root.equestria.protobuf.entity.v1.EntityType[message.value] : message.value;
                            return object;
                        };
    
                        /**
                         * Converts this EntityTypeValue to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.entity.v1.EntityTypeValue
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        EntityTypeValue.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return EntityTypeValue;
                    })();
    
                    v1.EntityRef = (function() {
    
                        /**
                         * Properties of an EntityRef.
                         * @memberof equestria.protobuf.entity.v1
                         * @interface IEntityRef
                         * @property {equestria.protobuf.entity.v1.EntityType|null} [entityType] EntityRef entityType
                         * @property {string|null} [id] EntityRef id
                         * @property {Array.<string>|null} [slugs] EntityRef slugs
                         * @property {string|null} [shortName] EntityRef shortName
                         */
    
                        /**
                         * Constructs a new EntityRef.
                         * @memberof equestria.protobuf.entity.v1
                         * @classdesc Represents an EntityRef.
                         * @implements IEntityRef
                         * @constructor
                         * @param {equestria.protobuf.entity.v1.IEntityRef=} [properties] Properties to set
                         */
                        function EntityRef(properties) {
                            this.slugs = [];
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * EntityRef entityType.
                         * @member {equestria.protobuf.entity.v1.EntityType} entityType
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @instance
                         */
                        EntityRef.prototype.entityType = 0;
    
                        /**
                         * EntityRef id.
                         * @member {string} id
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @instance
                         */
                        EntityRef.prototype.id = "";
    
                        /**
                         * EntityRef slugs.
                         * @member {Array.<string>} slugs
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @instance
                         */
                        EntityRef.prototype.slugs = $util.emptyArray;
    
                        /**
                         * EntityRef shortName.
                         * @member {string} shortName
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @instance
                         */
                        EntityRef.prototype.shortName = "";
    
                        /**
                         * Creates a new EntityRef instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {equestria.protobuf.entity.v1.IEntityRef=} [properties] Properties to set
                         * @returns {equestria.protobuf.entity.v1.EntityRef} EntityRef instance
                         */
                        EntityRef.create = function create(properties) {
                            return new EntityRef(properties);
                        };
    
                        /**
                         * Encodes the specified EntityRef message. Does not implicitly {@link equestria.protobuf.entity.v1.EntityRef.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {equestria.protobuf.entity.v1.IEntityRef} message EntityRef message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        EntityRef.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.entityType != null && Object.hasOwnProperty.call(message, "entityType"))
                                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.entityType);
                            if (message.id != null && Object.hasOwnProperty.call(message, "id"))
                                writer.uint32(/* id 2, wireType 2 =*/18).string(message.id);
                            if (message.slugs != null && message.slugs.length)
                                for (var i = 0; i < message.slugs.length; ++i)
                                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.slugs[i]);
                            if (message.shortName != null && Object.hasOwnProperty.call(message, "shortName"))
                                writer.uint32(/* id 4, wireType 2 =*/34).string(message.shortName);
                            return writer;
                        };
    
                        /**
                         * Encodes the specified EntityRef message, length delimited. Does not implicitly {@link equestria.protobuf.entity.v1.EntityRef.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {equestria.protobuf.entity.v1.IEntityRef} message EntityRef message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        EntityRef.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes an EntityRef message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.entity.v1.EntityRef} EntityRef
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        EntityRef.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.entity.v1.EntityRef();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    message.entityType = reader.int32();
                                    break;
                                case 2:
                                    message.id = reader.string();
                                    break;
                                case 3:
                                    if (!(message.slugs && message.slugs.length))
                                        message.slugs = [];
                                    message.slugs.push(reader.string());
                                    break;
                                case 4:
                                    message.shortName = reader.string();
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes an EntityRef message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.entity.v1.EntityRef} EntityRef
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        EntityRef.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies an EntityRef message.
                         * @function verify
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        EntityRef.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.entityType != null && message.hasOwnProperty("entityType"))
                                switch (message.entityType) {
                                default:
                                    return "entityType: enum value expected";
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                    break;
                                }
                            if (message.id != null && message.hasOwnProperty("id"))
                                if (!$util.isString(message.id))
                                    return "id: string expected";
                            if (message.slugs != null && message.hasOwnProperty("slugs")) {
                                if (!Array.isArray(message.slugs))
                                    return "slugs: array expected";
                                for (var i = 0; i < message.slugs.length; ++i)
                                    if (!$util.isString(message.slugs[i]))
                                        return "slugs: string[] expected";
                            }
                            if (message.shortName != null && message.hasOwnProperty("shortName"))
                                if (!$util.isString(message.shortName))
                                    return "shortName: string expected";
                            return null;
                        };
    
                        /**
                         * Creates an EntityRef message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.entity.v1.EntityRef} EntityRef
                         */
                        EntityRef.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.entity.v1.EntityRef)
                                return object;
                            var message = new $root.equestria.protobuf.entity.v1.EntityRef();
                            switch (object.entityType) {
                            case "USERS":
                            case 0:
                                message.entityType = 0;
                                break;
                            case "COUNTRIES":
                            case 1:
                                message.entityType = 1;
                                break;
                            case "MEASUREMENT_TYPES":
                            case 2:
                                message.entityType = 2;
                                break;
                            case "MEASURING_UNITS":
                            case 3:
                                message.entityType = 3;
                                break;
                            case "DEVICE_TYPES":
                            case 4:
                                message.entityType = 4;
                                break;
                            case "TENANTS":
                            case 5:
                                message.entityType = 5;
                                break;
                            case "DEVICE_MODELS":
                            case 6:
                                message.entityType = 6;
                                break;
                            case "DEVICES":
                            case 7:
                                message.entityType = 7;
                                break;
                            case "SIGNALS":
                            case 8:
                                message.entityType = 8;
                                break;
                            case "ASSET_TYPES":
                            case 9:
                                message.entityType = 9;
                                break;
                            case "ASSETS":
                            case 10:
                                message.entityType = 10;
                                break;
                            case "HARDPOINTS":
                            case 11:
                                message.entityType = 11;
                                break;
                            case "SOFTPOINT_TYPES":
                            case 12:
                                message.entityType = 12;
                                break;
                            case "SOFTPOINTS":
                            case 13:
                                message.entityType = 13;
                                break;
                            case "SENSORS":
                            case 14:
                                message.entityType = 14;
                                break;
                            case "SENSOR_MODELS":
                            case 15:
                                message.entityType = 15;
                                break;
                            case "SENSOR_CLASSES":
                            case 16:
                                message.entityType = 16;
                                break;
                            case "DEVICE_PROFILES":
                            case 17:
                                message.entityType = 17;
                                break;
                            case "DEVICE_PROFILE_TYPES":
                            case 18:
                                message.entityType = 18;
                                break;
                            }
                            if (object.id != null)
                                message.id = String(object.id);
                            if (object.slugs) {
                                if (!Array.isArray(object.slugs))
                                    throw TypeError(".equestria.protobuf.entity.v1.EntityRef.slugs: array expected");
                                message.slugs = [];
                                for (var i = 0; i < object.slugs.length; ++i)
                                    message.slugs[i] = String(object.slugs[i]);
                            }
                            if (object.shortName != null)
                                message.shortName = String(object.shortName);
                            return message;
                        };
    
                        /**
                         * Creates a plain object from an EntityRef message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @static
                         * @param {equestria.protobuf.entity.v1.EntityRef} message EntityRef
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        EntityRef.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.arrays || options.defaults)
                                object.slugs = [];
                            if (options.defaults) {
                                object.entityType = options.enums === String ? "USERS" : 0;
                                object.id = "";
                                object.shortName = "";
                            }
                            if (message.entityType != null && message.hasOwnProperty("entityType"))
                                object.entityType = options.enums === String ? $root.equestria.protobuf.entity.v1.EntityType[message.entityType] : message.entityType;
                            if (message.id != null && message.hasOwnProperty("id"))
                                object.id = message.id;
                            if (message.slugs && message.slugs.length) {
                                object.slugs = [];
                                for (var j = 0; j < message.slugs.length; ++j)
                                    object.slugs[j] = message.slugs[j];
                            }
                            if (message.shortName != null && message.hasOwnProperty("shortName"))
                                object.shortName = message.shortName;
                            return object;
                        };
    
                        /**
                         * Converts this EntityRef to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.entity.v1.EntityRef
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        EntityRef.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return EntityRef;
                    })();
    
                    /**
                     * SignalNumberFormatType enum.
                     * @name equestria.protobuf.entity.v1.SignalNumberFormatType
                     * @enum {number}
                     * @property {number} SIGNAL_NUMBER_FORMAT_TYPE_NOOP=0 SIGNAL_NUMBER_FORMAT_TYPE_NOOP value
                     * @property {number} DEFAULT=1 DEFAULT value
                     * @property {number} PRECISION=2 PRECISION value
                     * @property {number} FIXED=3 FIXED value
                     */
                    v1.SignalNumberFormatType = (function() {
                        var valuesById = {}, values = Object.create(valuesById);
                        values[valuesById[0] = "SIGNAL_NUMBER_FORMAT_TYPE_NOOP"] = 0;
                        values[valuesById[1] = "DEFAULT"] = 1;
                        values[valuesById[2] = "PRECISION"] = 2;
                        values[valuesById[3] = "FIXED"] = 3;
                        return values;
                    })();
    
                    v1.SignalNumberFormat = (function() {
    
                        /**
                         * Properties of a SignalNumberFormat.
                         * @memberof equestria.protobuf.entity.v1
                         * @interface ISignalNumberFormat
                         * @property {equestria.protobuf.entity.v1.SignalNumberFormatType|null} [formatType] SignalNumberFormat formatType
                         * @property {google.protobuf.IInt32Value|null} [digits] SignalNumberFormat digits
                         */
    
                        /**
                         * Constructs a new SignalNumberFormat.
                         * @memberof equestria.protobuf.entity.v1
                         * @classdesc Represents a SignalNumberFormat.
                         * @implements ISignalNumberFormat
                         * @constructor
                         * @param {equestria.protobuf.entity.v1.ISignalNumberFormat=} [properties] Properties to set
                         */
                        function SignalNumberFormat(properties) {
                            if (properties)
                                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                    if (properties[keys[i]] != null)
                                        this[keys[i]] = properties[keys[i]];
                        }
    
                        /**
                         * SignalNumberFormat formatType.
                         * @member {equestria.protobuf.entity.v1.SignalNumberFormatType} formatType
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @instance
                         */
                        SignalNumberFormat.prototype.formatType = 0;
    
                        /**
                         * SignalNumberFormat digits.
                         * @member {google.protobuf.IInt32Value|null|undefined} digits
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @instance
                         */
                        SignalNumberFormat.prototype.digits = null;
    
                        /**
                         * Creates a new SignalNumberFormat instance using the specified properties.
                         * @function create
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {equestria.protobuf.entity.v1.ISignalNumberFormat=} [properties] Properties to set
                         * @returns {equestria.protobuf.entity.v1.SignalNumberFormat} SignalNumberFormat instance
                         */
                        SignalNumberFormat.create = function create(properties) {
                            return new SignalNumberFormat(properties);
                        };
    
                        /**
                         * Encodes the specified SignalNumberFormat message. Does not implicitly {@link equestria.protobuf.entity.v1.SignalNumberFormat.verify|verify} messages.
                         * @function encode
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {equestria.protobuf.entity.v1.ISignalNumberFormat} message SignalNumberFormat message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        SignalNumberFormat.encode = function encode(message, writer) {
                            if (!writer)
                                writer = $Writer.create();
                            if (message.formatType != null && Object.hasOwnProperty.call(message, "formatType"))
                                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.formatType);
                            if (message.digits != null && Object.hasOwnProperty.call(message, "digits"))
                                $root.google.protobuf.Int32Value.encode(message.digits, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
                            return writer;
                        };
    
                        /**
                         * Encodes the specified SignalNumberFormat message, length delimited. Does not implicitly {@link equestria.protobuf.entity.v1.SignalNumberFormat.verify|verify} messages.
                         * @function encodeDelimited
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {equestria.protobuf.entity.v1.ISignalNumberFormat} message SignalNumberFormat message or plain object to encode
                         * @param {$protobuf.Writer} [writer] Writer to encode to
                         * @returns {$protobuf.Writer} Writer
                         */
                        SignalNumberFormat.encodeDelimited = function encodeDelimited(message, writer) {
                            return this.encode(message, writer).ldelim();
                        };
    
                        /**
                         * Decodes a SignalNumberFormat message from the specified reader or buffer.
                         * @function decode
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @param {number} [length] Message length if known beforehand
                         * @returns {equestria.protobuf.entity.v1.SignalNumberFormat} SignalNumberFormat
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        SignalNumberFormat.decode = function decode(reader, length) {
                            if (!(reader instanceof $Reader))
                                reader = $Reader.create(reader);
                            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.equestria.protobuf.entity.v1.SignalNumberFormat();
                            while (reader.pos < end) {
                                var tag = reader.uint32();
                                switch (tag >>> 3) {
                                case 1:
                                    message.formatType = reader.int32();
                                    break;
                                case 2:
                                    message.digits = $root.google.protobuf.Int32Value.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag & 7);
                                    break;
                                }
                            }
                            return message;
                        };
    
                        /**
                         * Decodes a SignalNumberFormat message from the specified reader or buffer, length delimited.
                         * @function decodeDelimited
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                         * @returns {equestria.protobuf.entity.v1.SignalNumberFormat} SignalNumberFormat
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        SignalNumberFormat.decodeDelimited = function decodeDelimited(reader) {
                            if (!(reader instanceof $Reader))
                                reader = new $Reader(reader);
                            return this.decode(reader, reader.uint32());
                        };
    
                        /**
                         * Verifies a SignalNumberFormat message.
                         * @function verify
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {Object.<string,*>} message Plain object to verify
                         * @returns {string|null} `null` if valid, otherwise the reason why it is not
                         */
                        SignalNumberFormat.verify = function verify(message) {
                            if (typeof message !== "object" || message === null)
                                return "object expected";
                            if (message.formatType != null && message.hasOwnProperty("formatType"))
                                switch (message.formatType) {
                                default:
                                    return "formatType: enum value expected";
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                    break;
                                }
                            if (message.digits != null && message.hasOwnProperty("digits")) {
                                var error = $root.google.protobuf.Int32Value.verify(message.digits);
                                if (error)
                                    return "digits." + error;
                            }
                            return null;
                        };
    
                        /**
                         * Creates a SignalNumberFormat message from a plain object. Also converts values to their respective internal types.
                         * @function fromObject
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {Object.<string,*>} object Plain object
                         * @returns {equestria.protobuf.entity.v1.SignalNumberFormat} SignalNumberFormat
                         */
                        SignalNumberFormat.fromObject = function fromObject(object) {
                            if (object instanceof $root.equestria.protobuf.entity.v1.SignalNumberFormat)
                                return object;
                            var message = new $root.equestria.protobuf.entity.v1.SignalNumberFormat();
                            switch (object.formatType) {
                            case "SIGNAL_NUMBER_FORMAT_TYPE_NOOP":
                            case 0:
                                message.formatType = 0;
                                break;
                            case "DEFAULT":
                            case 1:
                                message.formatType = 1;
                                break;
                            case "PRECISION":
                            case 2:
                                message.formatType = 2;
                                break;
                            case "FIXED":
                            case 3:
                                message.formatType = 3;
                                break;
                            }
                            if (object.digits != null) {
                                if (typeof object.digits !== "object")
                                    throw TypeError(".equestria.protobuf.entity.v1.SignalNumberFormat.digits: object expected");
                                message.digits = $root.google.protobuf.Int32Value.fromObject(object.digits);
                            }
                            return message;
                        };
    
                        /**
                         * Creates a plain object from a SignalNumberFormat message. Also converts values to other types if specified.
                         * @function toObject
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @static
                         * @param {equestria.protobuf.entity.v1.SignalNumberFormat} message SignalNumberFormat
                         * @param {$protobuf.IConversionOptions} [options] Conversion options
                         * @returns {Object.<string,*>} Plain object
                         */
                        SignalNumberFormat.toObject = function toObject(message, options) {
                            if (!options)
                                options = {};
                            var object = {};
                            if (options.defaults) {
                                object.formatType = options.enums === String ? "SIGNAL_NUMBER_FORMAT_TYPE_NOOP" : 0;
                                object.digits = null;
                            }
                            if (message.formatType != null && message.hasOwnProperty("formatType"))
                                object.formatType = options.enums === String ? $root.equestria.protobuf.entity.v1.SignalNumberFormatType[message.formatType] : message.formatType;
                            if (message.digits != null && message.hasOwnProperty("digits"))
                                object.digits = $root.google.protobuf.Int32Value.toObject(message.digits, options);
                            return object;
                        };
    
                        /**
                         * Converts this SignalNumberFormat to JSON.
                         * @function toJSON
                         * @memberof equestria.protobuf.entity.v1.SignalNumberFormat
                         * @instance
                         * @returns {Object.<string,*>} JSON object
                         */
                        SignalNumberFormat.prototype.toJSON = function toJSON() {
                            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                        };
    
                        return SignalNumberFormat;
                    })();
    
                    return v1;
                })();
    
                return entity;
            })();
    
            return protobuf;
        })();
    
        return equestria;
    })();
    
    $root.google = (function() {
    
        /**
         * Namespace google.
         * @exports google
         * @namespace
         */
        var google = {};
    
        google.protobuf = (function() {
    
            /**
             * Namespace protobuf.
             * @memberof google
             * @namespace
             */
            var protobuf = {};
    
            protobuf.Duration = (function() {
    
                /**
                 * Properties of a Duration.
                 * @memberof google.protobuf
                 * @interface IDuration
                 * @property {number|Long|null} [seconds] Duration seconds
                 * @property {number|null} [nanos] Duration nanos
                 */
    
                /**
                 * Constructs a new Duration.
                 * @memberof google.protobuf
                 * @classdesc Represents a Duration.
                 * @implements IDuration
                 * @constructor
                 * @param {google.protobuf.IDuration=} [properties] Properties to set
                 */
                function Duration(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * Duration seconds.
                 * @member {number|Long} seconds
                 * @memberof google.protobuf.Duration
                 * @instance
                 */
                Duration.prototype.seconds = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    
                /**
                 * Duration nanos.
                 * @member {number} nanos
                 * @memberof google.protobuf.Duration
                 * @instance
                 */
                Duration.prototype.nanos = 0;
    
                /**
                 * Creates a new Duration instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {google.protobuf.IDuration=} [properties] Properties to set
                 * @returns {google.protobuf.Duration} Duration instance
                 */
                Duration.create = function create(properties) {
                    return new Duration(properties);
                };
    
                /**
                 * Encodes the specified Duration message. Does not implicitly {@link google.protobuf.Duration.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {google.protobuf.IDuration} message Duration message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Duration.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.seconds != null && Object.hasOwnProperty.call(message, "seconds"))
                        writer.uint32(/* id 1, wireType 0 =*/8).int64(message.seconds);
                    if (message.nanos != null && Object.hasOwnProperty.call(message, "nanos"))
                        writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nanos);
                    return writer;
                };
    
                /**
                 * Encodes the specified Duration message, length delimited. Does not implicitly {@link google.protobuf.Duration.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {google.protobuf.IDuration} message Duration message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Duration.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a Duration message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.Duration} Duration
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Duration.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Duration();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.seconds = reader.int64();
                            break;
                        case 2:
                            message.nanos = reader.int32();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a Duration message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.Duration} Duration
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Duration.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a Duration message.
                 * @function verify
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                Duration.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.seconds != null && message.hasOwnProperty("seconds"))
                        if (!$util.isInteger(message.seconds) && !(message.seconds && $util.isInteger(message.seconds.low) && $util.isInteger(message.seconds.high)))
                            return "seconds: integer|Long expected";
                    if (message.nanos != null && message.hasOwnProperty("nanos"))
                        if (!$util.isInteger(message.nanos))
                            return "nanos: integer expected";
                    return null;
                };
    
                /**
                 * Creates a Duration message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.Duration} Duration
                 */
                Duration.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.Duration)
                        return object;
                    var message = new $root.google.protobuf.Duration();
                    if (object.seconds != null)
                        if ($util.Long)
                            (message.seconds = $util.Long.fromValue(object.seconds)).unsigned = false;
                        else if (typeof object.seconds === "string")
                            message.seconds = parseInt(object.seconds, 10);
                        else if (typeof object.seconds === "number")
                            message.seconds = object.seconds;
                        else if (typeof object.seconds === "object")
                            message.seconds = new $util.LongBits(object.seconds.low >>> 0, object.seconds.high >>> 0).toNumber();
                    if (object.nanos != null)
                        message.nanos = object.nanos | 0;
                    return message;
                };
    
                /**
                 * Creates a plain object from a Duration message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.Duration
                 * @static
                 * @param {google.protobuf.Duration} message Duration
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                Duration.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults) {
                        if ($util.Long) {
                            var long = new $util.Long(0, 0, false);
                            object.seconds = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                        } else
                            object.seconds = options.longs === String ? "0" : 0;
                        object.nanos = 0;
                    }
                    if (message.seconds != null && message.hasOwnProperty("seconds"))
                        if (typeof message.seconds === "number")
                            object.seconds = options.longs === String ? String(message.seconds) : message.seconds;
                        else
                            object.seconds = options.longs === String ? $util.Long.prototype.toString.call(message.seconds) : options.longs === Number ? new $util.LongBits(message.seconds.low >>> 0, message.seconds.high >>> 0).toNumber() : message.seconds;
                    if (message.nanos != null && message.hasOwnProperty("nanos"))
                        object.nanos = message.nanos;
                    return object;
                };
    
                /**
                 * Converts this Duration to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.Duration
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                Duration.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return Duration;
            })();
    
            protobuf.Timestamp = (function() {
    
                /**
                 * Properties of a Timestamp.
                 * @memberof google.protobuf
                 * @interface ITimestamp
                 * @property {number|Long|null} [seconds] Timestamp seconds
                 * @property {number|null} [nanos] Timestamp nanos
                 */
    
                /**
                 * Constructs a new Timestamp.
                 * @memberof google.protobuf
                 * @classdesc Represents a Timestamp.
                 * @implements ITimestamp
                 * @constructor
                 * @param {google.protobuf.ITimestamp=} [properties] Properties to set
                 */
                function Timestamp(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * Timestamp seconds.
                 * @member {number|Long} seconds
                 * @memberof google.protobuf.Timestamp
                 * @instance
                 */
                Timestamp.prototype.seconds = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    
                /**
                 * Timestamp nanos.
                 * @member {number} nanos
                 * @memberof google.protobuf.Timestamp
                 * @instance
                 */
                Timestamp.prototype.nanos = 0;
    
                /**
                 * Creates a new Timestamp instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {google.protobuf.ITimestamp=} [properties] Properties to set
                 * @returns {google.protobuf.Timestamp} Timestamp instance
                 */
                Timestamp.create = function create(properties) {
                    return new Timestamp(properties);
                };
    
                /**
                 * Encodes the specified Timestamp message. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {google.protobuf.ITimestamp} message Timestamp message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Timestamp.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.seconds != null && Object.hasOwnProperty.call(message, "seconds"))
                        writer.uint32(/* id 1, wireType 0 =*/8).int64(message.seconds);
                    if (message.nanos != null && Object.hasOwnProperty.call(message, "nanos"))
                        writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nanos);
                    return writer;
                };
    
                /**
                 * Encodes the specified Timestamp message, length delimited. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {google.protobuf.ITimestamp} message Timestamp message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Timestamp.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a Timestamp message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.Timestamp} Timestamp
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Timestamp.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Timestamp();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.seconds = reader.int64();
                            break;
                        case 2:
                            message.nanos = reader.int32();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a Timestamp message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.Timestamp} Timestamp
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Timestamp.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a Timestamp message.
                 * @function verify
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                Timestamp.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.seconds != null && message.hasOwnProperty("seconds"))
                        if (!$util.isInteger(message.seconds) && !(message.seconds && $util.isInteger(message.seconds.low) && $util.isInteger(message.seconds.high)))
                            return "seconds: integer|Long expected";
                    if (message.nanos != null && message.hasOwnProperty("nanos"))
                        if (!$util.isInteger(message.nanos))
                            return "nanos: integer expected";
                    return null;
                };
    
                /**
                 * Creates a Timestamp message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.Timestamp} Timestamp
                 */
                Timestamp.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.Timestamp)
                        return object;
                    var message = new $root.google.protobuf.Timestamp();
                    if (object.seconds != null)
                        if ($util.Long)
                            (message.seconds = $util.Long.fromValue(object.seconds)).unsigned = false;
                        else if (typeof object.seconds === "string")
                            message.seconds = parseInt(object.seconds, 10);
                        else if (typeof object.seconds === "number")
                            message.seconds = object.seconds;
                        else if (typeof object.seconds === "object")
                            message.seconds = new $util.LongBits(object.seconds.low >>> 0, object.seconds.high >>> 0).toNumber();
                    if (object.nanos != null)
                        message.nanos = object.nanos | 0;
                    return message;
                };
    
                /**
                 * Creates a plain object from a Timestamp message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.Timestamp
                 * @static
                 * @param {google.protobuf.Timestamp} message Timestamp
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                Timestamp.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults) {
                        if ($util.Long) {
                            var long = new $util.Long(0, 0, false);
                            object.seconds = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                        } else
                            object.seconds = options.longs === String ? "0" : 0;
                        object.nanos = 0;
                    }
                    if (message.seconds != null && message.hasOwnProperty("seconds"))
                        if (typeof message.seconds === "number")
                            object.seconds = options.longs === String ? String(message.seconds) : message.seconds;
                        else
                            object.seconds = options.longs === String ? $util.Long.prototype.toString.call(message.seconds) : options.longs === Number ? new $util.LongBits(message.seconds.low >>> 0, message.seconds.high >>> 0).toNumber() : message.seconds;
                    if (message.nanos != null && message.hasOwnProperty("nanos"))
                        object.nanos = message.nanos;
                    return object;
                };
    
                /**
                 * Converts this Timestamp to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.Timestamp
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                Timestamp.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return Timestamp;
            })();
    
            protobuf.DoubleValue = (function() {
    
                /**
                 * Properties of a DoubleValue.
                 * @memberof google.protobuf
                 * @interface IDoubleValue
                 * @property {number|null} [value] DoubleValue value
                 */
    
                /**
                 * Constructs a new DoubleValue.
                 * @memberof google.protobuf
                 * @classdesc Represents a DoubleValue.
                 * @implements IDoubleValue
                 * @constructor
                 * @param {google.protobuf.IDoubleValue=} [properties] Properties to set
                 */
                function DoubleValue(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * DoubleValue value.
                 * @member {number} value
                 * @memberof google.protobuf.DoubleValue
                 * @instance
                 */
                DoubleValue.prototype.value = 0;
    
                /**
                 * Creates a new DoubleValue instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {google.protobuf.IDoubleValue=} [properties] Properties to set
                 * @returns {google.protobuf.DoubleValue} DoubleValue instance
                 */
                DoubleValue.create = function create(properties) {
                    return new DoubleValue(properties);
                };
    
                /**
                 * Encodes the specified DoubleValue message. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {google.protobuf.IDoubleValue} message DoubleValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                DoubleValue.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 1 =*/9).double(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified DoubleValue message, length delimited. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {google.protobuf.IDoubleValue} message DoubleValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                DoubleValue.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a DoubleValue message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.DoubleValue} DoubleValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                DoubleValue.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.DoubleValue();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.double();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a DoubleValue message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.DoubleValue} DoubleValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                DoubleValue.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a DoubleValue message.
                 * @function verify
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                DoubleValue.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (typeof message.value !== "number")
                            return "value: number expected";
                    return null;
                };
    
                /**
                 * Creates a DoubleValue message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.DoubleValue} DoubleValue
                 */
                DoubleValue.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.DoubleValue)
                        return object;
                    var message = new $root.google.protobuf.DoubleValue();
                    if (object.value != null)
                        message.value = Number(object.value);
                    return message;
                };
    
                /**
                 * Creates a plain object from a DoubleValue message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.DoubleValue
                 * @static
                 * @param {google.protobuf.DoubleValue} message DoubleValue
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                DoubleValue.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        object.value = 0;
                    if (message.value != null && message.hasOwnProperty("value"))
                        object.value = options.json && !isFinite(message.value) ? String(message.value) : message.value;
                    return object;
                };
    
                /**
                 * Converts this DoubleValue to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.DoubleValue
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                DoubleValue.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return DoubleValue;
            })();
    
            protobuf.FloatValue = (function() {
    
                /**
                 * Properties of a FloatValue.
                 * @memberof google.protobuf
                 * @interface IFloatValue
                 * @property {number|null} [value] FloatValue value
                 */
    
                /**
                 * Constructs a new FloatValue.
                 * @memberof google.protobuf
                 * @classdesc Represents a FloatValue.
                 * @implements IFloatValue
                 * @constructor
                 * @param {google.protobuf.IFloatValue=} [properties] Properties to set
                 */
                function FloatValue(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * FloatValue value.
                 * @member {number} value
                 * @memberof google.protobuf.FloatValue
                 * @instance
                 */
                FloatValue.prototype.value = 0;
    
                /**
                 * Creates a new FloatValue instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {google.protobuf.IFloatValue=} [properties] Properties to set
                 * @returns {google.protobuf.FloatValue} FloatValue instance
                 */
                FloatValue.create = function create(properties) {
                    return new FloatValue(properties);
                };
    
                /**
                 * Encodes the specified FloatValue message. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {google.protobuf.IFloatValue} message FloatValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                FloatValue.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 5 =*/13).float(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified FloatValue message, length delimited. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {google.protobuf.IFloatValue} message FloatValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                FloatValue.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a FloatValue message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.FloatValue} FloatValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                FloatValue.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.FloatValue();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.float();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a FloatValue message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.FloatValue} FloatValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                FloatValue.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a FloatValue message.
                 * @function verify
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                FloatValue.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (typeof message.value !== "number")
                            return "value: number expected";
                    return null;
                };
    
                /**
                 * Creates a FloatValue message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.FloatValue} FloatValue
                 */
                FloatValue.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.FloatValue)
                        return object;
                    var message = new $root.google.protobuf.FloatValue();
                    if (object.value != null)
                        message.value = Number(object.value);
                    return message;
                };
    
                /**
                 * Creates a plain object from a FloatValue message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.FloatValue
                 * @static
                 * @param {google.protobuf.FloatValue} message FloatValue
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                FloatValue.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        object.value = 0;
                    if (message.value != null && message.hasOwnProperty("value"))
                        object.value = options.json && !isFinite(message.value) ? String(message.value) : message.value;
                    return object;
                };
    
                /**
                 * Converts this FloatValue to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.FloatValue
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                FloatValue.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return FloatValue;
            })();
    
            protobuf.Int64Value = (function() {
    
                /**
                 * Properties of an Int64Value.
                 * @memberof google.protobuf
                 * @interface IInt64Value
                 * @property {number|Long|null} [value] Int64Value value
                 */
    
                /**
                 * Constructs a new Int64Value.
                 * @memberof google.protobuf
                 * @classdesc Represents an Int64Value.
                 * @implements IInt64Value
                 * @constructor
                 * @param {google.protobuf.IInt64Value=} [properties] Properties to set
                 */
                function Int64Value(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * Int64Value value.
                 * @member {number|Long} value
                 * @memberof google.protobuf.Int64Value
                 * @instance
                 */
                Int64Value.prototype.value = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    
                /**
                 * Creates a new Int64Value instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {google.protobuf.IInt64Value=} [properties] Properties to set
                 * @returns {google.protobuf.Int64Value} Int64Value instance
                 */
                Int64Value.create = function create(properties) {
                    return new Int64Value(properties);
                };
    
                /**
                 * Encodes the specified Int64Value message. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {google.protobuf.IInt64Value} message Int64Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Int64Value.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 0 =*/8).int64(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified Int64Value message, length delimited. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {google.protobuf.IInt64Value} message Int64Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Int64Value.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes an Int64Value message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.Int64Value} Int64Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Int64Value.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Int64Value();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.int64();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes an Int64Value message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.Int64Value} Int64Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Int64Value.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies an Int64Value message.
                 * @function verify
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                Int64Value.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (!$util.isInteger(message.value) && !(message.value && $util.isInteger(message.value.low) && $util.isInteger(message.value.high)))
                            return "value: integer|Long expected";
                    return null;
                };
    
                /**
                 * Creates an Int64Value message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.Int64Value} Int64Value
                 */
                Int64Value.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.Int64Value)
                        return object;
                    var message = new $root.google.protobuf.Int64Value();
                    if (object.value != null)
                        if ($util.Long)
                            (message.value = $util.Long.fromValue(object.value)).unsigned = false;
                        else if (typeof object.value === "string")
                            message.value = parseInt(object.value, 10);
                        else if (typeof object.value === "number")
                            message.value = object.value;
                        else if (typeof object.value === "object")
                            message.value = new $util.LongBits(object.value.low >>> 0, object.value.high >>> 0).toNumber();
                    return message;
                };
    
                /**
                 * Creates a plain object from an Int64Value message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.Int64Value
                 * @static
                 * @param {google.protobuf.Int64Value} message Int64Value
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                Int64Value.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        if ($util.Long) {
                            var long = new $util.Long(0, 0, false);
                            object.value = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                        } else
                            object.value = options.longs === String ? "0" : 0;
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (typeof message.value === "number")
                            object.value = options.longs === String ? String(message.value) : message.value;
                        else
                            object.value = options.longs === String ? $util.Long.prototype.toString.call(message.value) : options.longs === Number ? new $util.LongBits(message.value.low >>> 0, message.value.high >>> 0).toNumber() : message.value;
                    return object;
                };
    
                /**
                 * Converts this Int64Value to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.Int64Value
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                Int64Value.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return Int64Value;
            })();
    
            protobuf.UInt64Value = (function() {
    
                /**
                 * Properties of a UInt64Value.
                 * @memberof google.protobuf
                 * @interface IUInt64Value
                 * @property {number|Long|null} [value] UInt64Value value
                 */
    
                /**
                 * Constructs a new UInt64Value.
                 * @memberof google.protobuf
                 * @classdesc Represents a UInt64Value.
                 * @implements IUInt64Value
                 * @constructor
                 * @param {google.protobuf.IUInt64Value=} [properties] Properties to set
                 */
                function UInt64Value(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * UInt64Value value.
                 * @member {number|Long} value
                 * @memberof google.protobuf.UInt64Value
                 * @instance
                 */
                UInt64Value.prototype.value = $util.Long ? $util.Long.fromBits(0,0,true) : 0;
    
                /**
                 * Creates a new UInt64Value instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {google.protobuf.IUInt64Value=} [properties] Properties to set
                 * @returns {google.protobuf.UInt64Value} UInt64Value instance
                 */
                UInt64Value.create = function create(properties) {
                    return new UInt64Value(properties);
                };
    
                /**
                 * Encodes the specified UInt64Value message. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {google.protobuf.IUInt64Value} message UInt64Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                UInt64Value.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified UInt64Value message, length delimited. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {google.protobuf.IUInt64Value} message UInt64Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                UInt64Value.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a UInt64Value message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.UInt64Value} UInt64Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                UInt64Value.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.UInt64Value();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.uint64();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a UInt64Value message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.UInt64Value} UInt64Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                UInt64Value.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a UInt64Value message.
                 * @function verify
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                UInt64Value.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (!$util.isInteger(message.value) && !(message.value && $util.isInteger(message.value.low) && $util.isInteger(message.value.high)))
                            return "value: integer|Long expected";
                    return null;
                };
    
                /**
                 * Creates a UInt64Value message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.UInt64Value} UInt64Value
                 */
                UInt64Value.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.UInt64Value)
                        return object;
                    var message = new $root.google.protobuf.UInt64Value();
                    if (object.value != null)
                        if ($util.Long)
                            (message.value = $util.Long.fromValue(object.value)).unsigned = true;
                        else if (typeof object.value === "string")
                            message.value = parseInt(object.value, 10);
                        else if (typeof object.value === "number")
                            message.value = object.value;
                        else if (typeof object.value === "object")
                            message.value = new $util.LongBits(object.value.low >>> 0, object.value.high >>> 0).toNumber(true);
                    return message;
                };
    
                /**
                 * Creates a plain object from a UInt64Value message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.UInt64Value
                 * @static
                 * @param {google.protobuf.UInt64Value} message UInt64Value
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                UInt64Value.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        if ($util.Long) {
                            var long = new $util.Long(0, 0, true);
                            object.value = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                        } else
                            object.value = options.longs === String ? "0" : 0;
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (typeof message.value === "number")
                            object.value = options.longs === String ? String(message.value) : message.value;
                        else
                            object.value = options.longs === String ? $util.Long.prototype.toString.call(message.value) : options.longs === Number ? new $util.LongBits(message.value.low >>> 0, message.value.high >>> 0).toNumber(true) : message.value;
                    return object;
                };
    
                /**
                 * Converts this UInt64Value to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.UInt64Value
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                UInt64Value.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return UInt64Value;
            })();
    
            protobuf.Int32Value = (function() {
    
                /**
                 * Properties of an Int32Value.
                 * @memberof google.protobuf
                 * @interface IInt32Value
                 * @property {number|null} [value] Int32Value value
                 */
    
                /**
                 * Constructs a new Int32Value.
                 * @memberof google.protobuf
                 * @classdesc Represents an Int32Value.
                 * @implements IInt32Value
                 * @constructor
                 * @param {google.protobuf.IInt32Value=} [properties] Properties to set
                 */
                function Int32Value(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * Int32Value value.
                 * @member {number} value
                 * @memberof google.protobuf.Int32Value
                 * @instance
                 */
                Int32Value.prototype.value = 0;
    
                /**
                 * Creates a new Int32Value instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {google.protobuf.IInt32Value=} [properties] Properties to set
                 * @returns {google.protobuf.Int32Value} Int32Value instance
                 */
                Int32Value.create = function create(properties) {
                    return new Int32Value(properties);
                };
    
                /**
                 * Encodes the specified Int32Value message. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {google.protobuf.IInt32Value} message Int32Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Int32Value.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 0 =*/8).int32(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified Int32Value message, length delimited. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {google.protobuf.IInt32Value} message Int32Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Int32Value.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes an Int32Value message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.Int32Value} Int32Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Int32Value.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Int32Value();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.int32();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes an Int32Value message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.Int32Value} Int32Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Int32Value.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies an Int32Value message.
                 * @function verify
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                Int32Value.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (!$util.isInteger(message.value))
                            return "value: integer expected";
                    return null;
                };
    
                /**
                 * Creates an Int32Value message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.Int32Value} Int32Value
                 */
                Int32Value.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.Int32Value)
                        return object;
                    var message = new $root.google.protobuf.Int32Value();
                    if (object.value != null)
                        message.value = object.value | 0;
                    return message;
                };
    
                /**
                 * Creates a plain object from an Int32Value message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.Int32Value
                 * @static
                 * @param {google.protobuf.Int32Value} message Int32Value
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                Int32Value.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        object.value = 0;
                    if (message.value != null && message.hasOwnProperty("value"))
                        object.value = message.value;
                    return object;
                };
    
                /**
                 * Converts this Int32Value to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.Int32Value
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                Int32Value.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return Int32Value;
            })();
    
            protobuf.UInt32Value = (function() {
    
                /**
                 * Properties of a UInt32Value.
                 * @memberof google.protobuf
                 * @interface IUInt32Value
                 * @property {number|null} [value] UInt32Value value
                 */
    
                /**
                 * Constructs a new UInt32Value.
                 * @memberof google.protobuf
                 * @classdesc Represents a UInt32Value.
                 * @implements IUInt32Value
                 * @constructor
                 * @param {google.protobuf.IUInt32Value=} [properties] Properties to set
                 */
                function UInt32Value(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * UInt32Value value.
                 * @member {number} value
                 * @memberof google.protobuf.UInt32Value
                 * @instance
                 */
                UInt32Value.prototype.value = 0;
    
                /**
                 * Creates a new UInt32Value instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {google.protobuf.IUInt32Value=} [properties] Properties to set
                 * @returns {google.protobuf.UInt32Value} UInt32Value instance
                 */
                UInt32Value.create = function create(properties) {
                    return new UInt32Value(properties);
                };
    
                /**
                 * Encodes the specified UInt32Value message. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {google.protobuf.IUInt32Value} message UInt32Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                UInt32Value.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified UInt32Value message, length delimited. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {google.protobuf.IUInt32Value} message UInt32Value message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                UInt32Value.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a UInt32Value message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.UInt32Value} UInt32Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                UInt32Value.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.UInt32Value();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.uint32();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a UInt32Value message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.UInt32Value} UInt32Value
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                UInt32Value.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a UInt32Value message.
                 * @function verify
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                UInt32Value.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (!$util.isInteger(message.value))
                            return "value: integer expected";
                    return null;
                };
    
                /**
                 * Creates a UInt32Value message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.UInt32Value} UInt32Value
                 */
                UInt32Value.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.UInt32Value)
                        return object;
                    var message = new $root.google.protobuf.UInt32Value();
                    if (object.value != null)
                        message.value = object.value >>> 0;
                    return message;
                };
    
                /**
                 * Creates a plain object from a UInt32Value message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.UInt32Value
                 * @static
                 * @param {google.protobuf.UInt32Value} message UInt32Value
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                UInt32Value.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        object.value = 0;
                    if (message.value != null && message.hasOwnProperty("value"))
                        object.value = message.value;
                    return object;
                };
    
                /**
                 * Converts this UInt32Value to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.UInt32Value
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                UInt32Value.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return UInt32Value;
            })();
    
            protobuf.BoolValue = (function() {
    
                /**
                 * Properties of a BoolValue.
                 * @memberof google.protobuf
                 * @interface IBoolValue
                 * @property {boolean|null} [value] BoolValue value
                 */
    
                /**
                 * Constructs a new BoolValue.
                 * @memberof google.protobuf
                 * @classdesc Represents a BoolValue.
                 * @implements IBoolValue
                 * @constructor
                 * @param {google.protobuf.IBoolValue=} [properties] Properties to set
                 */
                function BoolValue(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * BoolValue value.
                 * @member {boolean} value
                 * @memberof google.protobuf.BoolValue
                 * @instance
                 */
                BoolValue.prototype.value = false;
    
                /**
                 * Creates a new BoolValue instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {google.protobuf.IBoolValue=} [properties] Properties to set
                 * @returns {google.protobuf.BoolValue} BoolValue instance
                 */
                BoolValue.create = function create(properties) {
                    return new BoolValue(properties);
                };
    
                /**
                 * Encodes the specified BoolValue message. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {google.protobuf.IBoolValue} message BoolValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                BoolValue.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 0 =*/8).bool(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified BoolValue message, length delimited. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {google.protobuf.IBoolValue} message BoolValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                BoolValue.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a BoolValue message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.BoolValue} BoolValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                BoolValue.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.BoolValue();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.bool();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a BoolValue message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.BoolValue} BoolValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                BoolValue.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a BoolValue message.
                 * @function verify
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                BoolValue.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (typeof message.value !== "boolean")
                            return "value: boolean expected";
                    return null;
                };
    
                /**
                 * Creates a BoolValue message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.BoolValue} BoolValue
                 */
                BoolValue.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.BoolValue)
                        return object;
                    var message = new $root.google.protobuf.BoolValue();
                    if (object.value != null)
                        message.value = Boolean(object.value);
                    return message;
                };
    
                /**
                 * Creates a plain object from a BoolValue message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.BoolValue
                 * @static
                 * @param {google.protobuf.BoolValue} message BoolValue
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                BoolValue.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        object.value = false;
                    if (message.value != null && message.hasOwnProperty("value"))
                        object.value = message.value;
                    return object;
                };
    
                /**
                 * Converts this BoolValue to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.BoolValue
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                BoolValue.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return BoolValue;
            })();
    
            protobuf.StringValue = (function() {
    
                /**
                 * Properties of a StringValue.
                 * @memberof google.protobuf
                 * @interface IStringValue
                 * @property {string|null} [value] StringValue value
                 */
    
                /**
                 * Constructs a new StringValue.
                 * @memberof google.protobuf
                 * @classdesc Represents a StringValue.
                 * @implements IStringValue
                 * @constructor
                 * @param {google.protobuf.IStringValue=} [properties] Properties to set
                 */
                function StringValue(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * StringValue value.
                 * @member {string} value
                 * @memberof google.protobuf.StringValue
                 * @instance
                 */
                StringValue.prototype.value = "";
    
                /**
                 * Creates a new StringValue instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {google.protobuf.IStringValue=} [properties] Properties to set
                 * @returns {google.protobuf.StringValue} StringValue instance
                 */
                StringValue.create = function create(properties) {
                    return new StringValue(properties);
                };
    
                /**
                 * Encodes the specified StringValue message. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {google.protobuf.IStringValue} message StringValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                StringValue.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 2 =*/10).string(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified StringValue message, length delimited. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {google.protobuf.IStringValue} message StringValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                StringValue.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a StringValue message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.StringValue} StringValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                StringValue.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.StringValue();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.string();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a StringValue message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.StringValue} StringValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                StringValue.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a StringValue message.
                 * @function verify
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                StringValue.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (!$util.isString(message.value))
                            return "value: string expected";
                    return null;
                };
    
                /**
                 * Creates a StringValue message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.StringValue} StringValue
                 */
                StringValue.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.StringValue)
                        return object;
                    var message = new $root.google.protobuf.StringValue();
                    if (object.value != null)
                        message.value = String(object.value);
                    return message;
                };
    
                /**
                 * Creates a plain object from a StringValue message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.StringValue
                 * @static
                 * @param {google.protobuf.StringValue} message StringValue
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                StringValue.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        object.value = "";
                    if (message.value != null && message.hasOwnProperty("value"))
                        object.value = message.value;
                    return object;
                };
    
                /**
                 * Converts this StringValue to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.StringValue
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                StringValue.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return StringValue;
            })();
    
            protobuf.BytesValue = (function() {
    
                /**
                 * Properties of a BytesValue.
                 * @memberof google.protobuf
                 * @interface IBytesValue
                 * @property {Uint8Array|null} [value] BytesValue value
                 */
    
                /**
                 * Constructs a new BytesValue.
                 * @memberof google.protobuf
                 * @classdesc Represents a BytesValue.
                 * @implements IBytesValue
                 * @constructor
                 * @param {google.protobuf.IBytesValue=} [properties] Properties to set
                 */
                function BytesValue(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * BytesValue value.
                 * @member {Uint8Array} value
                 * @memberof google.protobuf.BytesValue
                 * @instance
                 */
                BytesValue.prototype.value = $util.newBuffer([]);
    
                /**
                 * Creates a new BytesValue instance using the specified properties.
                 * @function create
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {google.protobuf.IBytesValue=} [properties] Properties to set
                 * @returns {google.protobuf.BytesValue} BytesValue instance
                 */
                BytesValue.create = function create(properties) {
                    return new BytesValue(properties);
                };
    
                /**
                 * Encodes the specified BytesValue message. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {google.protobuf.IBytesValue} message BytesValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                BytesValue.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified BytesValue message, length delimited. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {google.protobuf.IBytesValue} message BytesValue message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                BytesValue.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a BytesValue message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.BytesValue} BytesValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                BytesValue.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.BytesValue();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.value = reader.bytes();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a BytesValue message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.BytesValue} BytesValue
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                BytesValue.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                /**
                 * Verifies a BytesValue message.
                 * @function verify
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                BytesValue.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.value != null && message.hasOwnProperty("value"))
                        if (!(message.value && typeof message.value.length === "number" || $util.isString(message.value)))
                            return "value: buffer expected";
                    return null;
                };
    
                /**
                 * Creates a BytesValue message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {google.protobuf.BytesValue} BytesValue
                 */
                BytesValue.fromObject = function fromObject(object) {
                    if (object instanceof $root.google.protobuf.BytesValue)
                        return object;
                    var message = new $root.google.protobuf.BytesValue();
                    if (object.value != null)
                        if (typeof object.value === "string")
                            $util.base64.decode(object.value, message.value = $util.newBuffer($util.base64.length(object.value)), 0);
                        else if (object.value.length)
                            message.value = object.value;
                    return message;
                };
    
                /**
                 * Creates a plain object from a BytesValue message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof google.protobuf.BytesValue
                 * @static
                 * @param {google.protobuf.BytesValue} message BytesValue
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                BytesValue.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults)
                        if (options.bytes === String)
                            object.value = "";
                        else {
                            object.value = [];
                            if (options.bytes !== Array)
                                object.value = $util.newBuffer(object.value);
                        }
                    if (message.value != null && message.hasOwnProperty("value"))
                        object.value = options.bytes === String ? $util.base64.encode(message.value, 0, message.value.length) : options.bytes === Array ? Array.prototype.slice.call(message.value) : message.value;
                    return object;
                };
    
                /**
                 * Converts this BytesValue to JSON.
                 * @function toJSON
                 * @memberof google.protobuf.BytesValue
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                BytesValue.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };
    
                return BytesValue;
            })();
    
            return protobuf;
        })();
    
        return google;
    })();

    return $root;
});
