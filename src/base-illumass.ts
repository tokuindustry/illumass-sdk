// Copyright 2020 TOKU Systems, Inc.

import { AuthController } from './controller/auth';
import { SignalDataController } from './controller/signal-data';
import { Kuzzle } from 'kuzzle-sdk';

export class BaseIllumass {
  readonly auth: AuthController;
  readonly signalData: SignalDataController;

  private _tenantIdFilter = '';

  get tenantIdFilter(): string {
    return this._tenantIdFilter;
  }

  set tenantIdFilter(tenantId: string) {
    this._tenantIdFilter = tenantId.trim();
  }

  constructor(readonly kuzzle: Kuzzle) {
    this.auth = new AuthController(this.kuzzle, this);
    this.signalData = new SignalDataController(this.kuzzle);
  }

  async connect(): Promise<void> {
    await this.kuzzle.connect();
  }

  disconnect(): void {
    this.kuzzle.disconnect();
  }
}
