// Copyright 2025 TOKU Systems Inc.
// This file is automatically generated
import { BaseIllumass } from './base-illumass';
import { Kuzzle } from 'kuzzle-sdk';
import { CountryController } from './controller/entity/country-controller';
import { DeviceController } from './controller/entity/device-controller';
import { DeviceModelController } from './controller/entity/device-model-controller';
import { DeviceTypeController } from './controller/entity/device-type-controller';
import { MeasurementTypeController } from './controller/entity/measurement-type-controller';
import { MeasuringUnitController } from './controller/entity/measuring-unit-controller';
import { SignalController } from './controller/entity/signal-controller';
import { TenantController } from './controller/entity/tenant-controller';
import { UserController } from './controller/entity/user-controller';
import { AssetTypeController } from './controller/entity/asset-type-controller';
import { AssetController } from './controller/entity/asset-controller';
import { HardpointController } from './controller/entity/hardpoint-controller';
import { SoftpointTypeController } from './controller/entity/softpoint-type-controller';
import { SoftpointController } from './controller/entity/softpoint-controller';
import { DeviceProfileController } from './controller/entity/device-profile-controller';
import { DeviceProfileTypeController } from './controller/entity/device-profile-type-controller';
import { SensorController } from './controller/entity/sensor-controller';
import { SensorModelController } from './controller/entity/sensor-model-controller';
import { SensorClassController } from './controller/entity/sensor-class-controller';

export class Illumass extends BaseIllumass {
  readonly country: CountryController;
  readonly device: DeviceController;
  readonly deviceModel: DeviceModelController;
  readonly deviceType: DeviceTypeController;
  readonly measurementType: MeasurementTypeController;
  readonly measuringUnit: MeasuringUnitController;
  readonly signal: SignalController;
  readonly tenant: TenantController;
  readonly user: UserController;
  readonly assetType: AssetTypeController;
  readonly asset: AssetController;
  readonly hardpoint: HardpointController;
  readonly softpointType: SoftpointTypeController;
  readonly softpoint: SoftpointController;
  readonly deviceProfile: DeviceProfileController;
  readonly deviceProfileType: DeviceProfileTypeController;
  readonly sensor: SensorController;
  readonly sensorModel: SensorModelController;
  readonly sensorClass: SensorClassController;
  constructor(kuzzle: Kuzzle) {
    super(kuzzle);
    this.country = new CountryController(this);
    this.device = new DeviceController(this);
    this.deviceModel = new DeviceModelController(this);
    this.deviceType = new DeviceTypeController(this);
    this.measurementType = new MeasurementTypeController(this);
    this.measuringUnit = new MeasuringUnitController(this);
    this.signal = new SignalController(this);
    this.tenant = new TenantController(this);
    this.user = new UserController(this);
    this.assetType = new AssetTypeController(this);
    this.asset = new AssetController(this);
    this.hardpoint = new HardpointController(this);
    this.softpointType = new SoftpointTypeController(this);
    this.softpoint = new SoftpointController(this);
    this.deviceProfile = new DeviceProfileController(this);
    this.deviceProfileType = new DeviceProfileTypeController(this);
    this.sensor = new SensorController(this);
    this.sensorModel = new SensorModelController(this);
    this.sensorClass = new SensorClassController(this);
  }
}
