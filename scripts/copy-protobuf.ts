import ncp from 'ncp';
import * as path from 'path';
import mkdirp from 'mkdirp';

const projectDir = path.join(__dirname, '..');
const protobufDir = path.join(projectDir, 'protobuf', 'equestria');
const sourceProtobufDir = path.join(
  projectDir,
  'node_modules',
  '@equestria',
  'microservices',
  'protobuf',
  'equestria'
);

async function execute(): Promise<void> {
  await mkdirp(protobufDir);
  for (const pbdir of ['common', 'entity']) {
    const srcDir = path.join(sourceProtobufDir, pbdir);
    const dstDir = path.join(protobufDir, pbdir);
    await new Promise<void>((resolve, reject) => {
      ncp(srcDir, dstDir, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}

execute()
  .then(() => {
    console.log('Done.');
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
