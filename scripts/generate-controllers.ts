// Copyright 2020 TOKU Systems Inc.

import { entityTypeInfoList } from '@equestria/rumble-common-objects';
import * as path from 'path';
import * as fs from 'fs';
import * as _ from 'lodash';

const controllerDir = path.join('src', 'controller');
const entityDir = path.join(controllerDir, 'entity');

fs.mkdirSync(entityDir, { recursive: true });

entityTypeInfoList.forEach((info) => {
  const baseName = _.upperFirst(info.singular);
  const controllerName = baseName + 'Controller';
  const tsFile = info.hyphenatedSingular + '-controller.ts';

  const lines: string[] = [];
  lines.push(`// Copyright ${new Date().getFullYear()} TOKU Systems Inc.`);

  if (['Device', 'Hardpoint', 'Signal'].indexOf(baseName) >= 0) {
    lines.push('import {');
    lines.push('  BaseEntityController,');
    lines.push('  ReadWithIncludeLastOptions,');
    lines.push('  SearchResult,');
    lines.push('  SearchWithIncludeLastOptions,');
    lines.push("} from '../base-entity-controller';");
    lines.push('import {');
    lines.push(`  ${baseName},`);
    lines.push('  entityTypeInfoByEntityType,');
    lines.push('  EntityType,');
    lines.push('  EntityRef,');
    lines.push("} from '../../common';");
  } else {
    lines.push(
      "import { BaseEntityController } from '../base-entity-controller';"
    );
    lines.push(
      `import { ${baseName}, entityTypeInfoByEntityType, EntityType } from '../../common';`
    );
  }

  lines.push("import { Illumass } from '../../illumass';");
  lines.push('');
  lines.push(
    `export class ${controllerName} extends BaseEntityController<${baseName}> {`
  );
  lines.push('  constructor(readonly illumass: Illumass) {');
  const entityTypeName = info.hyphenatedPlural.toUpperCase().replace(/-/g, '_');
  lines.push(
    `    super(illumass, entityTypeInfoByEntityType(EntityType.${entityTypeName}));`
  );
  lines.push('  }');

  if (['Device', 'Hardpoint', 'Signal'].indexOf(baseName) >= 0) {
    lines.push('');
    lines.push('  async read(');
    lines.push('    ref: string | EntityRef,');
    lines.push('    options?: ReadWithIncludeLastOptions');
    lines.push(`  ): Promise<${baseName}> {`);

    lines.push('    const optsWithIncludeLast = {');
    lines.push('      ...options,');
    lines.push('      includeLast: options?.includeLast ?? false,');
    lines.push('    };');
    lines.push('    return await super.read(');
    lines.push('      ref,');
    lines.push('      optsWithIncludeLast as ReadWithIncludeLastOptions');
    lines.push('    );');
    lines.push('  }');

    lines.push('');

    lines.push('  async search(');
    lines.push('    query: object,');
    lines.push('    options?: SearchWithIncludeLastOptions');
    lines.push(`  ): Promise<SearchResult<${baseName}>> {`);
    lines.push('    const optsWithIncludeLast = {');
    lines.push('      ...options,');
    lines.push('      includeLast: options?.includeLast ?? false,');
    lines.push('    };');
    lines.push('    return await super.search(');
    lines.push('      query,');
    lines.push('      optsWithIncludeLast as ReadWithIncludeLastOptions');
    lines.push('    );');
    lines.push('  }');
  }
  lines.push('}');
  lines.push('');

  fs.writeFileSync(path.join(entityDir, tsFile), lines.join('\n'));
});
