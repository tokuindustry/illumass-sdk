# Developer notes

## Installing dependencies and building

When building, login to `npm.illumass.com`.

```sh
> npm --registry https://registry.npmjs.org login
> npm --registry https://registry.npmjs.org ci
```

## Publishing

To publish to default npm registry

1. Generate a [granular access token](https://www.npmjs.com/settings/toku-leo/tokens/). It should have write permission to @illumass/illumass-sdk.

1. Add the token to `~/.npmrc`. The file should look as follows

    ```ini
    //registry.npmjs.org/:_authToken=<granular access token>
    ```

1. Publish

    ```sh
    > npm publish
    ```
