# Illumass SDK

This project is the Typescript SDK to Illumass v4.

## Installation

```sh
npm install @illumass/illumass-sdk
```

## Documenation

See [documentation](https://illumass-sdk.readthedocs.io/en/latest/).
